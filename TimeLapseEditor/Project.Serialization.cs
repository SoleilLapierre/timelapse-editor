﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using ManagedCowTools.Math;
using System.Drawing.Drawing2D;
using System.Linq;

namespace TimeLapseEditor
{
    public partial class Project
    {
        #region ----- Serialized properties -----

        // Project data version - used to detect when automatic upgrades are needed.
        public int Version { get; set; }


        public string Name 
        {
            get { return mName; }
            set
            {
                mName = value;
                IsSaved = false;
            }
        }


        // List of all user-defined video sequences that frames can belong to.
        public List<Sequence> Sequences 
        {
            get { return mSequences; }
            set
            {
                mSequences = value;
                IsSaved = false;
                mSequenceNames = null;
            }
        }


        // List of all visual guidelines defined in the project settings.
        public List<Guideline> Guidelines
        {
            get { return mGuidelines; }
            set
            {
                mGuidelines = value;
                IsSaved = false;
            }
        }


        // Path to search for compass images.
        public string CompassImagePath { get; set; }


        // Final output image width in pixels.
        public int OutputWidth 
        { 
            get { return mOutputWidth; }
            set
            {
                mOutputWidth = value;
                IsSaved = false;
            }
        }


        // Final output image height in pixels.
        public int OutputHeight 
        { 
            get { return mOutputHeight; }
            set
            {
                mOutputHeight = value;
                IsSaved = false;
            }
        }


        // Bookmark for last image displayed.   TODO - this should be a program setting, not a project property.
        public int LastSelectedImage 
        { 
            get { return mLastSelectedImage; }
            set
            {
                mLastSelectedImage = value;
                IsSaved = false;
            }
        }


        #region -- Timelines --

        // Smooth timelines for various transformations.
        public FloatParameter ScaleTimeline { get; set; }

        public FloatParameter RotationTimeline { get; set; }

        public Vector2Parameter TranslationTimeline { get; set; }

        public FloatParameter HeadingOffsetTimeline { get; set; }

        public FloatParameter LatitudeOffsetTimeline { get; set; }

        public FloatParameter LongitudeOffsetTimeline { get; set; }

        public FloatParameter MapZoomTimeline { get; set; }

        public FloatParameter HueTimeline { get; set; }

        public FloatParameter SaturationTimeline { get; set; }

        public FloatParameter BrightnessTimeline { get; set; }

        public FloatParameter ContrastTimeline { get; set; }

        public FloatParameter GammaTimeline { get; set; }

        #endregion


        public Vector2 CompassImagePosition { get; set; }

        public float CompassImageScale { get; set; }

        public Vector2 MapImagePosition { get; set; }

        public Vector2 MapImageDimensions { get; set; }

        public float MapImageScale { get; set; }

        public string MapImagePath { get; set; }

        public string OsmMapsPath { get; set; }

        public Vector2 HudTextPosition { get; set; }

        public float MapImageAlpha { get; set; }

        public string MapRulesName { get; set; }

        public Vector2 AutoRegisterCenter { get; set; }

        public int AutoRegisterSearchRadius { get; set; }

        public int AutoregisterMaxDelta { get; set; }


        // Special serialization for fonts - serializes as a unique identifier for the font, rather than the class.
        // Relies on the same font being present on the system.
        public string HudTextFontSerialized
        {
            get
            {
                try
                {
                    if (HudTextFont != null)
                    {
                        TypeConverter converter = TypeDescriptor.GetConverter(typeof(System.Drawing.Font));
                        return converter.ConvertToString(HudTextFont);
                    }
                }
                catch { }

                return null;
            }
            set
            {
                HudTextFont = null;

                try
                {
                    TypeConverter converter = TypeDescriptor.GetConverter(typeof(System.Drawing.Font));
                    HudTextFont = (System.Drawing.Font)converter.ConvertFromString(value);
                }
                catch { }
            }
        }


        public int HudTextColorSerialized
        {
            get
            {
                return HudTextColor.ToArgb();
            }
            set
            {
                HudTextColor = System.Drawing.Color.FromArgb(value);
            }
        }

        public int SerializableTextOutlineColor { get; set; }

        public float TextOutlineThickness { get; set; }

        public float TextOutlineAlpha { get; set; }


        // List of all text overlays used in the project. Although overlays are frame-specific,
        // they are usually used on multiple frames so they are saved at the project level
        // to avoid duplication, and the resultant problems with updating their properties.
        public List<TextOverlay> TextOverlays { get; set; }

        #endregion
        #region ----- Serialization helpers -----

        public static bool IsCurrentProjectSaved()
        {
            return (Instance != null) ? Instance.IsSaved : true;
        }


        public void Save()
        {
            if (String.IsNullOrEmpty(ProjectRoot))
            {
                FolderBrowserDialog fd = new FolderBrowserDialog();
                fd.ShowNewFolderButton = true;
                if (fd.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                if (!Directory.Exists(fd.SelectedPath))
                {
                    return;
                }

                ProjectRoot = fd.SelectedPath;
            }

            string path = Path.Combine(ProjectRoot, Name + ".xml");
            TextWriter writer = null;
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(Project));
                writer = new StreamWriter(path);
                ser.Serialize(writer, this);

                if (!Directory.Exists(IndexPath))
                {
                    Directory.CreateDirectory(IndexPath);
                }

                FrameStore.SaveIndex();
                IsSaved = true;
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }
        }


        public static void SaveCurrentProject()
        {
            if (Instance != null)
            {
                Instance.Save();
            }
        }


        public static bool LoadCurrentProject(string aPath)
        {
            if (File.Exists(aPath))
            {
                Project p = null;
                TextReader reader = null;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(Project));
                    reader = new StreamReader(aPath);
                    p = (Project)ser.Deserialize(reader);
                    p.ProjectRoot = Path.GetDirectoryName(aPath);
                    p.FrameStore.LoadIndex(p.IndexPath);
                    p.IsSaved = true;
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }

                if (p != null)
                {
                    string dir = Path.GetDirectoryName(aPath);
                    if (!p.ProjectRoot.Equals(dir))
                    {
                        p.ProjectRoot = dir;
                    }

                    Instance = p;

                    Instance.Upgrade();
                    p.UpdateAccumulatedProperties(0);

                    return true;
                }
            }

            return false;
        }

        #endregion
        #region ----- Automatic version upgrades -----

        private void Upgrade()
        {
            bool changed = false;

            if (Instance.Version < 1)
            {
                Instance.MakeBlurRegionsRelative();
                changed = true;
            }

            if (Instance.Version < 2)
            {
                Instance.MakeJiggleTranslationRelative();
                changed = true;
            }

            if (Instance.Version < 3)
            {
                Instance.MakeBlurRegionsPreTransform();
                changed = true;
            }

            if (changed)
            {
                Instance.Version = 3;
            }
        }


        private void MakeBlurRegionsRelative()
        {
            int numFrames = GetNumFrames();
            for (int index = 0; index < numFrames; ++index)
            {
                Frame f = GetFrame(index);
                int count = f.BlurRegions.Count;
                for (int i = 0; i < count; ++i)
                {
                    Vector2 offset = f.JiggleCorrection + TranslationTimeline[(double)index];
                    int xOffset = (int)Math.Round(offset.X);
                    int yOffset = -(int)Math.Round(offset.Y);
                    BlurRegion br = f.BlurRegions[i];
                    br.Bounds = new Rectangle(br.Bounds.Left - xOffset, br.Bounds.Top - yOffset, br.Bounds.Width, br.Bounds.Height);
                }
            }
        }


        private void MakeJiggleTranslationRelative()
        {
            int numFrames = GetNumFrames();
            for (int index = 1; index < numFrames; ++index)
            {
                Frame f1 = GetFrame(index);
                Frame f2 = GetFrame(index - 1);
                f1.AccumulatedJiggleCorrection = f1.JiggleCorrection;
                f1.JiggleCorrection -= f2.AccumulatedJiggleCorrection;
            }
        }


        private void MakeBlurRegionsPreTransform()
        {
            UpdateAccumulatedProperties(0);

            using (BatchDialog dlg = new BatchDialog(this, "Upgrading...", 0, GetNumFrames() - 1))
            {
                dlg.Autostart = true;
                dlg.SetOptionName("Project Upgrade");
                dlg.ProcessFrameCallback += new BatchDialog.WorkCallback(BatchMakeBlurRegionsPreTransform);
                dlg.ShowDialog();
                dlg.ProcessFrameCallback -= new BatchDialog.WorkCallback(BatchMakeBlurRegionsPreTransform);
            }
        }


        private bool BatchMakeBlurRegionsPreTransform(int aIndex, bool aDummy)
        {
            Frame f = GetFrame(aIndex);
            int count = f.BlurRegions.Count;
            if (count > 0)
            {
                var oldRegions = f.BlurRegions;
                f.BlurRegions = new List<BlurRegion>();

                foreach (BlurRegion br in oldRegions)
                {
                    if ((br.Bounds.Width > 0) && (br.Bounds.Height > 0))
                    {
                        f.AddBlurRegionScreenSpace(br.Bounds, br.Shape);
                    }
                }
            }

            return true;
        }

        #endregion
    }
}
