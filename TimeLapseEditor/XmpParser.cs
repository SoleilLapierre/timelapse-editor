﻿// Copyright (c) 2012 by Soleil Lapierre.

using System.IO;
using System.Xml;

namespace TimeLapseEditor
{
    public class XmpParser
    {
        public class Record
        {
            public Record()
            {
                Altitude = "0";
                AltitudeRef = "0";
                Latitude = "0,0N";
                Longitude = "0,0W";
            }

            public string Altitude { get; set; }
            public string AltitudeRef { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
        }


        public static Record Parse(string aFilePath)
        {
            Record result = null;

            using (FileStream fs = new FileStream(aFilePath, FileMode.Open))
            {
                result = Parse(fs);
            }

            return result;
        }


        public static Record Parse(Stream aStream)
        {
            Record result = new Record();

            XmlDocument doc = new XmlDocument();
            doc.Load(aStream);
            XmlNode node1 = doc["x:xmpmeta"];
            if (node1 != null)
            {
                node1 = node1["rdf:RDF"];
            }
            if (node1 != null)
            {
                node1 = node1["rdf:Description"];
            }

            XmlNode node2 = node1["exif:GPSAltitude"];
            if (node2 != null)
            {
                result.Altitude = node2.InnerText;
            }

            node2 = node1["exif:GPSAltitudeRef"];
            if (node2 != null)
            {
                result.AltitudeRef = node2.InnerText;
            }

            node2 = node1["exif:GPSLatitude"];
            if (node2 != null)
            {
                result.Latitude = node2.InnerText;
            }

            node2 = node1["exif:GPSLongitude"];
            if (node2 != null)
            {
                result.Longitude = node2.InnerText;
            }

            return result;
        }
    }
}
