﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Xml.Serialization;
using ManagedCowTools.Math;

namespace TimeLapseEditor
{
    public partial class Project
    {
        #region ----- Types -----

        public class Sequence
        {
            public string Name { get; set; }
            public uint ID { get; set; }
            public bool ShowOverlays { get; set; }

            public override string ToString()
            {
                return Name + (ShowOverlays ? " +ovl" : "");
            }
        }


        public class Guideline
        {
            public Guideline()
            {
                X1 = Y1 = 0.0f;
                X2 = Y2 = 1.0f;
                Color = System.Drawing.Color.White;
            }

            public float X1 { get; set; }
            public float Y1 { get; set; }
            public float X2 { get; set; }
            public float Y2 { get; set; }
            public int SerializedColor { get; set; }

            [XmlIgnore]
            public System.Drawing.Color Color
            {
                get { return System.Drawing.Color.FromArgb(SerializedColor); }
                set { SerializedColor = value.ToArgb(); }
            }

            public override string ToString()
            {
                return String.Format("({0:0.00}.{1:0.00})-({2:0.00}-{3:0.00})", X1, Y1, X2, Y2);
            }
        }

        #endregion
        #region ----- Member data -----

        // The program currently only supports having one project instance loaded at a time.
        public static Project Instance { get; set; }

        private string mName;
        private List<Sequence> mSequences;
        private List<Guideline> mGuidelines;
        private Dictionary<uint, string> mSequenceNames;
        private Dictionary<string, Bitmap> mCompassImages = null;
        private int mOutputWidth = 1920;
        private int mOutputHeight = 1080;
        private int mLastSelectedImage = 0;

        #endregion
        #region ----- Construction -----

        static Project()
        {
            Instance = new Project();
            Instance.IsSaved = true;
        }


        public Project()
        {
            mName = "New Project";
            ProjectRoot = "";
            Sequences = new List<Sequence>();
            mGuidelines = new List<Guideline>();
            FrameStore = new FrameStore();

            ScaleTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            ScaleTimeline.Insert(0, 1.0f);

            RotationTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            RotationTimeline.Insert(0, 0.0f);

            TranslationTimeline = new Vector2Parameter(ParameterTypes.InterpolationMode.Sinus);
            TranslationTimeline.Insert(0, Vector2.Zero);

            HeadingOffsetTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            HeadingOffsetTimeline.Insert(0, 0.0f);

            LatitudeOffsetTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            LatitudeOffsetTimeline.Insert(0, 0.0f);

            LongitudeOffsetTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            LongitudeOffsetTimeline.Insert(0, 0.0f);

            MapZoomTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            MapZoomTimeline.Insert(0, 15.0f);

            HueTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            HueTimeline.Insert(0, 0.0f);

            SaturationTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            SaturationTimeline.Insert(0, 0.0f);

            BrightnessTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            BrightnessTimeline.Insert(0, 0.0f);

            ContrastTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            ContrastTimeline.Insert(0, 1.0f);

            GammaTimeline = new FloatParameter(ParameterTypes.InterpolationMode.Sinus);
            GammaTimeline.Insert(0, 1.0f);

            CompassImagePosition = new ManagedCowTools.Math.Vector2(0.0f, 0.0f);
            CompassImageScale = 1.0f;
            CompassImagePath = null;

            MapImagePosition = new ManagedCowTools.Math.Vector2(0.0f, 0.0f);
            MapImageDimensions = new Vector2(384.0f, 288.0f);
            MapImageScale = 1.0f;
            MapImagePath = "";
            MapRulesName = "alias=googlemaps";
            OsmMapsPath = "";
            HudTextPosition = Vector2.Zero;
            HudTextColor = System.Drawing.Color.White;
            HudTextFont = new System.Drawing.Font(System.Drawing.FontFamily.GenericSerif, 16.0f);
            IsSaved = false;
            MapImageAlpha = 1.0f;
            AutoRegisterCenter = new Vector2(960.0f, 540.0f);
            AutoRegisterSearchRadius = 60;
            AutoregisterMaxDelta = 20;

            TextOutlineColor = Color.FromArgb(127, 0, 0, 0);
            TextOutlineThickness = 2.5f;
            TextOutlineAlpha = 0.5f;

            TextOverlays = new List<TextOverlay>();
        }


        public Project(Project aOther)
        : this()
        {
            Version = aOther.Version;
            mName = aOther.mName;
            ProjectRoot = aOther.ProjectRoot;
            Sequences = new List<Sequence>(aOther.Sequences); // Shallow copy is OK because both projects won't be edited at the same time.
            mGuidelines = new List<Guideline>(aOther.Guidelines);

            FrameStore.CopyFrom(aOther.FrameStore);
            int numFrames = GetNumFrames();
            for (int i = 0; i < numFrames; ++i)
            {
                GetFrame(i).Project = this;
            }

            OutputWidth = aOther.OutputWidth;
            OutputHeight = aOther.OutputHeight;
            LastSelectedImage = aOther.LastSelectedImage;
            ScaleTimeline = new FloatParameter(aOther.ScaleTimeline);
            RotationTimeline = new FloatParameter(aOther.RotationTimeline);
            TranslationTimeline = new Vector2Parameter(aOther.TranslationTimeline);
            HeadingOffsetTimeline = new FloatParameter(aOther.HeadingOffsetTimeline);
            LatitudeOffsetTimeline = new FloatParameter(aOther.LatitudeOffsetTimeline);
            LongitudeOffsetTimeline = new FloatParameter(aOther.LongitudeOffsetTimeline);
            MapZoomTimeline = new FloatParameter(aOther.MapZoomTimeline);
            HueTimeline = new FloatParameter(aOther.HueTimeline);
            SaturationTimeline = new FloatParameter(aOther.SaturationTimeline);
            BrightnessTimeline = new FloatParameter(aOther.BrightnessTimeline);
            ContrastTimeline = new FloatParameter(aOther.ContrastTimeline);
            GammaTimeline = new FloatParameter(aOther.GammaTimeline);

            HudTextPosition = aOther.HudTextPosition;
            HudTextColor = aOther.HudTextColor;
            HudTextFont = aOther.HudTextFont;
            MapImageAlpha = aOther.MapImageAlpha;
            AutoRegisterCenter = aOther.AutoRegisterCenter;
            AutoRegisterSearchRadius = aOther.AutoRegisterSearchRadius;
            AutoregisterMaxDelta = aOther.AutoregisterMaxDelta;

            CompassImagePosition = aOther.CompassImagePosition;
            CompassImageScale = aOther.CompassImageScale;
            CompassImagePath = aOther.CompassImagePath;

            MapImagePosition = aOther.MapImagePosition;
            MapImageDimensions = aOther.MapImageDimensions;
            MapImageScale = aOther.MapImageScale;

            MapImagePath = aOther.MapImagePath;
            MapRulesName = aOther.MapRulesName;
            OsmMapsPath = aOther.OsmMapsPath;

            TextOutlineColor = aOther.TextOutlineColor;
            TextOutlineThickness = aOther.TextOutlineThickness;
            TextOutlineAlpha = aOther.TextOutlineAlpha;

            TextOverlays = new List<TextOverlay>();
            foreach (TextOverlay ovl in aOther.TextOverlays)
            {
                TextOverlays.Add(new TextOverlay(ovl));
            }

            IsSaved = aOther.IsSaved;
        }

        #endregion
        #region ----- Nonserialized properties -----

        [XmlIgnore]
        public string ProjectRoot { get; set; }


        [XmlIgnore]
        public string IndexPath
        {
            get
            {
                string result = ProjectRoot;
                if (result[result.Length - 1] != System.IO.Path.DirectorySeparatorChar)
                {
                    result += System.IO.Path.DirectorySeparatorChar;
                }

                result += "toc" + System.IO.Path.DirectorySeparatorChar;
                return result;
            }
        }


        [XmlIgnore]
        public System.Drawing.Font HudTextFont { get; set; }


        [XmlIgnore]
        public System.Drawing.Color HudTextColor { get; set; }


        [XmlIgnore]
        public System.Drawing.Color TextOutlineColor
        {
            get
            {
                int r = (SerializableTextOutlineColor >> 16) & 255;
                int g = (SerializableTextOutlineColor >> 8) & 255;
                int b = SerializableTextOutlineColor & 255;
                return System.Drawing.Color.FromArgb((int)(255.0f * TextOutlineAlpha), r, g, b);
            }
            set
            {
                int c = value.ToArgb();
                SerializableTextOutlineColor = c & 0x00FFFFFF;
                TextOutlineAlpha = (float)((c >> 24) & 255) / 255.0f;
            }
        }


        [XmlIgnore]
        public FrameStore FrameStore { get; private set; }


        [XmlIgnore]
        public bool IsSaved { get; set; }

        #endregion
        #region ----- Public methods -----

        public string GetSequenceName(uint aHash)
        {
            if (mSequenceNames == null)
            {
                mSequenceNames = new Dictionary<uint, string>();
                foreach (Sequence s in mSequences)
                {
                    mSequenceNames.Add(s.ID, s.Name);
                }
            }

            if (mSequenceNames.ContainsKey(aHash))
            {
                return mSequenceNames[aHash];
            }

            return null;
        }


        public void SetCompassImagePositionX(float aX)
        {
            CompassImagePosition = new Vector2(aX, CompassImagePosition.Y);
            IsSaved = false;
        }


        public void SetCompassImagePositionY(float aY)
        {
            CompassImagePosition = new Vector2(CompassImagePosition.X, aY);
            IsSaved = false;
        }


        public void SetMapImagePositionX(float aX)
        {
            MapImagePosition = new Vector2(aX, MapImagePosition.Y);
            IsSaved = false;
        }


        public void SetMapImagePositionY(float aY)
        {
            MapImagePosition = new Vector2(MapImagePosition.X, aY);
            IsSaved = false;
        }


        public void SetMapImageWidth(float aWidth)
        {
            MapImageDimensions = new Vector2(aWidth, MapImageDimensions.Y);
            IsSaved = false;
        }


        public void SetMapImageHeight(float aHeight)
        {
            MapImageDimensions = new Vector2(MapImageDimensions.X, aHeight);
            IsSaved = false;
        }


        public void SetHudTextPositionX(float aX)
        {
            HudTextPosition = new Vector2(aX, HudTextPosition.Y);
            IsSaved = false;
        }


        public void SetHudTextPositionY(float aY)
        {
            HudTextPosition = new Vector2(HudTextPosition.X, aY);
            IsSaved = false;
        }


        public void SetAutoRegisterCenter(float aX, float aY)
        {
            AutoRegisterCenter = new Vector2(aX, aY);
            IsSaved = false;
        }


        public void SetAutoRegisterCenterX(float aX)
        {
            AutoRegisterCenter = new Vector2(aX, AutoRegisterCenter.Y);
            IsSaved = false;
        }


        public void SetAutoRegisterCenterY(float aY)
        {
            AutoRegisterCenter = new Vector2(AutoRegisterCenter.X, aY);
            IsSaved = false;
        }

        public TextOverlay GetTextOverlay(int aId)
        {
            foreach (TextOverlay ovl in TextOverlays)
            {
                if (ovl.ID == aId)
                {
                    return ovl;
                }
            }

            return null;
        }


        public int GetNumFrames()
        {
            return FrameStore.Count;
        }


        public Frame GetFrame(int aIndex)
        {
            Frame f = FrameStore[aIndex];
            f.Project = this;
            return f;
        }


        public Bitmap GetCompassImage(string aName)
        {
            if (mCompassImages == null)
            {
                mCompassImages = new Dictionary<string,Bitmap>();

                if (!String.IsNullOrEmpty(CompassImagePath))
                {
                    foreach (string file in Directory.GetFiles(CompassImagePath))
                    {
                        if (file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png"))
                        {
                            Bitmap im = Bitmap.FromFile(file) as Bitmap;
                            mCompassImages[Path.GetFileNameWithoutExtension(file)] = im;
                        }
                    }
                }
            }

            if (mCompassImages.ContainsKey(aName))
            {
                return mCompassImages[aName];
            }

            return null;
        }


        public ImageHelper GetImageHelper(int aIndex)
        {
            Frame f = GetFrame(aIndex);
            ImageHelper ih = new ImageHelper();
            ih.OutputSize = new Size(OutputWidth, OutputHeight);
            ih.Scale = ScaleTimeline[(double)aIndex];
            ih.Rotation = f.AngleCorrection + RotationTimeline[(double)aIndex];
            ih.Translation = f.AccumulatedJiggleCorrection + TranslationTimeline[(double)aIndex];

            return ih;
        }


        public void UpdateAccumulatedProperties(int aLastValidIndex)
        {
            if (aLastValidIndex > 0)
            {
                --aLastValidIndex;
            }

            Vector2 currentJiggle = GetFrame(aLastValidIndex).AccumulatedJiggleCorrection;
            int numFrames = GetNumFrames();
            for (int index = aLastValidIndex + 1; index < numFrames; ++index)
            {
                Frame f = GetFrame(index);
                currentJiggle += f.JiggleCorrection;
                f.AccumulatedJiggleCorrection = currentJiggle;
            }
        }

        #endregion
    }
}
