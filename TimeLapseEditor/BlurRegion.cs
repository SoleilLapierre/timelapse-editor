﻿// Copyright (c) 2012 by Soleil Lapierre.

using ManagedCowTools.Math;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace TimeLapseEditor
{
    public class BlurRegion
    {
        public enum ShapeType
        {
            Rectangle,
            Ellipse,
            Autoregister
        };

        public List<Vector2> Polygon { get; set; }

        // Deprecated - these properties are now only used by upgrades.
        public System.Drawing.Rectangle Bounds { get; set; }
        public ShapeType Shape { get; set; }

        public BlurRegion()
        {
            Shape = ShapeType.Rectangle;
            Bounds = new System.Drawing.Rectangle(-2, -2, 0, 0);
            Polygon = new List<Vector2>();
        }


        public BlurRegion(BlurRegion aOther)
        {
            Shape = aOther.Shape;
            Bounds = new System.Drawing.Rectangle(aOther.Bounds.Left, aOther.Bounds.Top, aOther.Bounds.Width, aOther.Bounds.Height);
            Polygon = new List<Vector2>(aOther.Polygon);
        }


        public Rectangle GetBounds()
        {
            if (Polygon.Count < 1)
            {
                return Rectangle.Empty;
            }

            float minX, minY, maxX, maxY;
            minX = maxX = Polygon[0].X;
            minY = maxY = Polygon[0].Y;

            foreach (Vector2 p in Polygon)
            {
                minX = Math.Min(minX, p.X);
                minY = Math.Min(minY, p.Y);
                maxX = Math.Max(maxX, p.X);
                maxY = Math.Max(maxY, p.Y);
            }

            return new Rectangle((int)Math.Floor(minX), (int)Math.Floor(minY), (int)Math.Ceiling(maxX - minX), (int)Math.Ceiling(maxY - minY));
        }


        public Region ToRegion()
        {
            PointF[] path = Polygon.Select(p => new PointF(p.X, p.Y)).ToArray();
            byte[] types = new byte[path.Length];
            for (int i = 0; i < types.Length; ++i)
            {
                types[i] = (byte)PathPointType.Line;
            }

            GraphicsPath gPath = new GraphicsPath(path, types);
            return new Region(gPath);
        }
    }
}
