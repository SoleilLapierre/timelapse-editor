﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TimeLapseEditor
{
    public class FrameStore
    {
        private const int TABLE_SIZE = 1000;

        private string mDatabaseDir = "";
        private List<Frame> mFrames = new List<Frame>();

        public void LoadIndex(string aPath)
        {
            mFrames = new List<Frame>();
            mDatabaseDir = aPath;            

            SortedList<int, string> orderedFiles = new SortedList<int, string>();
            foreach (string s in Directory.EnumerateFiles(mDatabaseDir, "toc?????.xml"))
            {
                string numStr = Path.GetFileName(s).TrimStart('t', 'o', 'c').TrimEnd('.', 'x', 'm', 'l');
                int index = 0;
                if (int.TryParse(numStr, out index))
                {
                    orderedFiles.Add(index, s);
                }
            }

            XmlSerializer ser = new XmlSerializer(typeof(List<Frame>));
            foreach (int index in orderedFiles.Keys)
            {
                string path = orderedFiles[index];
                using (FileStream fs = File.OpenRead(path))
                {
                    List<Frame> segment = ser.Deserialize(fs) as List<Frame>;
                    if (segment != null)
                    {
                        mFrames.AddRange(segment);
                    }
                }
            }

            for (int index = 0; index < mFrames.Count; ++index)
            {
                mFrames[index].Index = index;
            }
        }


        public void SaveIndex()
        {
            int i = 0;
            int count = Count;
            XmlSerializer ser = new XmlSerializer(typeof(List<Frame>));

            foreach (string s in Directory.EnumerateFiles(mDatabaseDir, "toc?????.xml"))
            {
                File.Delete(s);
            }

            int tocCount = 1;
            while (i < count)
            {
                List<Frame> segment = mFrames.GetRange(i, Math.Min(TABLE_SIZE, count - i));
                string path = Path.Combine(mDatabaseDir, string.Format("toc{0:00000}.xml", tocCount));

                using (FileStream fs = File.OpenWrite(path))
                {
                    ser.Serialize(fs, segment);                    
                }

                i += segment.Count;
                tocCount++;
            }
        }


        public void InsertFrames(IEnumerable<Frame> aFrameSet, int aIndex)
        {
            aIndex = Math.Min(Math.Max(0, aIndex), mFrames.Count);
            mFrames.InsertRange(aIndex, aFrameSet);
        }


        public void Remove(IEnumerable<Frame> aFrameSet)
        {
            foreach (Frame f in aFrameSet)
            {
                mFrames.Remove(f);
            }
        }


        public int Count
        {
            get { return mFrames.Count; }
        }


        public Frame this[int aIndex]
        {
            get
            {
                Frame f = mFrames[aIndex];
                f.Index = aIndex;
                return f;
            }
            set
            {
                value.Index = aIndex;
                mFrames[aIndex] = value;
            }
        }


        public void CopyFrom(FrameStore aOther)
        {
            mDatabaseDir = aOther.mDatabaseDir;
            mFrames.Clear();
            foreach (Frame f in aOther.mFrames)
            {
                mFrames.Add(new Frame(f));
            }
        }
    }
}
