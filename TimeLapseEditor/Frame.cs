﻿// Copyright (c) 2015 by Soleil Lapierre.

using ManagedCowTools.Math;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace TimeLapseEditor
{
    public class Frame
    {
        #region ----- Types -----

        public enum CompassUpdateMode
        {
            Freeze,
            Initialize,
            Update
        };

        #endregion
        #region ----- Construction -----

        public Frame()
        {
            SourcePath = "C:\\";
            ImportSession = "Default";
            Time = DateTime.MinValue;
            Latitude = 0.0;
            Longitude = 0.0;
            Altitude = 0.0f;
            AltitudeSmoothed = null;
            CoordinatesValid = false;
            Heading = null;
            ShowOverlays = true;
            Sequences = new List<uint>();
            TextOverlayIDs = new List<int>();
            PersistentPropertiesEdited = false;
            BlurRegions = new List<BlurRegion>();
            MapsToRender = new List<string>();
            MapImageFilename = null;
            FrameMultiple = 1;
            JiggleCorrection = Vector2.Zero;
            AccumulatedJiggleCorrection = Vector2.Zero;
            AngleCorrection = 0.0f;
            CompassMode = CompassUpdateMode.Update;
            Index = -1;
            Project = null;
        }


        public Frame(string aPath)
        : this()
        {
            SourcePath = aPath;

            if (!File.Exists(aPath))
            {
                throw new IOException("File " + aPath + " does not exist.");
            }

            // Use the file creation time as a first approximation timestamp.
            Time = File.GetCreationTimeUtc(aPath);
            // As a better approximation, try to get the creation time from the EXIF data.
            Image img = Image.FromFile(aPath);
            if (img != null)
            {
                System.Drawing.Imaging.PropertyItem propItem = img.GetPropertyItem(0x132);
                if (propItem != null)
                {
                    string text = (new System.Text.ASCIIEncoding()).GetString(propItem.Value, 0, propItem.Len - 1);
                    Time = DateTime.ParseExact(text, "yyyy:MM:d H:m:s", System.Globalization.CultureInfo.InvariantCulture);
                }
            }
            img.Dispose();
            img = null;
            GC.Collect();

            Sequences = new List<uint>();

            string dir = Path.GetDirectoryName(aPath);
            string fileName = Path.GetFileNameWithoutExtension(aPath) + ".xmp";
            string xmpPath = Path.Combine(dir, fileName);
            if (File.Exists(xmpPath))
            {
                XmpParser.Record record = XmpParser.Parse(xmpPath);

                if (record != null)
                {
                    if (!string.IsNullOrEmpty(record.Altitude))
                    {
                        string alt = record.Altitude;
                        if (alt.Contains("/"))
                        {
                            alt = alt.Split('/')[0];
                        }

                        float altitude = 0.0f;
                        if (float.TryParse(alt, out altitude))
                        {
                            Altitude = altitude;
                        }
                    }

                    bool latSet = false;
                    bool lonSet = false;

                    if (!string.IsNullOrEmpty(record.Latitude))
                    {
                        string[] parts = record.Latitude.Split(',');
                        double degrees = 0.0;
                        if (double.TryParse(parts[0], out degrees))
                        {
                            char direction = parts[1][parts[1].Length - 1];
                            string mins = parts[1].Substring(0, parts[1].Length - 1);
                            double minutes = 0.0;
                            if (double.TryParse(mins, out minutes))
                            {
                                double latitude = degrees + (minutes / 60.0);
                                if ((direction == 'S') || (direction == 's'))
                                {
                                    latitude = -latitude;
                                }

                                Latitude = latitude;
                                latSet = true;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(record.Longitude))
                    {
                        string[] parts = record.Longitude.Split(',');
                        double degrees = 0.0;
                        if (double.TryParse(parts[0], out degrees))
                        {
                            char direction = parts[1][parts[1].Length - 1];
                            string mins = parts[1].Substring(0, parts[1].Length - 1);
                            double minutes = 0.0;
                            if (double.TryParse(mins, out minutes))
                            {
                                double longitude = degrees + (minutes / 60.0);
                                if ((direction == 'E') || (direction == 'e'))
                                {
                                    longitude = 360.0 - longitude;
                                }

                                Longitude = longitude;
                                lonSet = true;
                            }
                        }
                    }

                    CoordinatesValid = latSet && lonSet;
                }
            }
        }


        public Frame(Frame aOther)
        {
            SourcePath = aOther.SourcePath;
            ImportSession = aOther.ImportSession;
            Time = aOther.Time;
            Latitude = aOther.Latitude;
            Longitude = aOther.Longitude;
            Altitude = aOther.Altitude;
            AltitudeSmoothed = aOther.AltitudeSmoothed;
            CoordinatesValid = aOther.CoordinatesValid;
            Heading = aOther.Heading;
            ShowOverlays = aOther.ShowOverlays;
            Sequences = new List<uint>(aOther.Sequences);
            PersistentPropertiesEdited = aOther.PersistentPropertiesEdited;
            MapsToRender = new List<string>(aOther.MapsToRender);
            MapImageFilename = aOther.MapImageFilename;
            FrameMultiple = aOther.FrameMultiple;
            JiggleCorrection = aOther.JiggleCorrection;
            AccumulatedJiggleCorrection = aOther.AccumulatedJiggleCorrection;
            AngleCorrection = aOther.AngleCorrection;
            CompassMode = aOther.CompassMode;
            Index = aOther.Index;
            Project = aOther.Project;

            TextOverlayIDs = new List<int>();
            TextOverlayIDs.AddRange(aOther.TextOverlayIDs);

            BlurRegions = new List<BlurRegion>();
            foreach (BlurRegion br in aOther.BlurRegions)
            {
                BlurRegions.Add(new BlurRegion(br));
            }
        }

        #endregion
        #region ----- Public methods -----

        // Called when moving to a new frame, to carry over persistent properties if they have not yet been set.
        public void CarryOverPropertiesFrom(Frame aFrame, bool aGeneral, bool aMaps, bool aLabels)
        {
            aGeneral |= !PersistentPropertiesEdited;
            aMaps |= !PersistentPropertiesEdited;
            aLabels |= !PersistentPropertiesEdited;

            if (aMaps)
            {
                MapsToRender = new List<string>(aFrame.MapsToRender);
                PersistentPropertiesEdited = true;
            }

            if (aLabels)
            {
                TextOverlayIDs = new List<int>();
                TextOverlayIDs.AddRange(aFrame.TextOverlayIDs);
                PersistentPropertiesEdited = true;
            }

            if (aGeneral)
            {
                Sequences = new List<uint>(aFrame.Sequences);
                ShowOverlays = aFrame.ShowOverlays;
                CompassMode = aFrame.CompassMode;
                FrameMultiple = aFrame.FrameMultiple;
                PersistentPropertiesEdited = true;
            }
        }


        public BlurRegion FindBlurRegionScreenSpace(PointF aLocation)
        {
            Matrix m = new Matrix();
            using (Bitmap img = Bitmap.FromFile(SourcePath) as Bitmap)
            {
                ImageHelper ih = Project.GetImageHelper(Index);
                ih.Translation = Vector2.Zero;
                m = ih.GetInverseTransform(img.Width, img.Height);
            }


            PointF[] temp = new PointF[] { aLocation };
            m.TransformPoints(temp);

            foreach (BlurRegion br in BlurRegions)
            {
                if (br.ToRegion().IsVisible(temp[0]))
                {
                    return br;
                }
            }

            return null;
        }


        public void AddBlurRegionScreenSpace(Rectangle aBounds, BlurRegion.ShapeType aType)
        {
            Matrix m = new Matrix();
            using (Bitmap img = Bitmap.FromFile(SourcePath) as Bitmap)
            {
                ImageHelper ih = Project.GetImageHelper(Index);
                ih.Translation = Vector2.Zero;
                m = ih.GetInverseTransform(img.Width, img.Height);
            }

            List<PointF> polygon = new List<PointF>();

            if (aType == BlurRegion.ShapeType.Rectangle)
            {
                polygon.Add(new PointF(aBounds.Left, aBounds.Top));
                polygon.Add(new PointF(aBounds.Right, aBounds.Top));
                polygon.Add(new PointF(aBounds.Right, aBounds.Bottom));
                polygon.Add(new PointF(aBounds.Left, aBounds.Bottom));
            }
            else if (aType == BlurRegion.ShapeType.Ellipse)
            {
                PointF[] circle = new PointF[16];
                for (int j = 0; j < 16; ++j)
                {
                    double angle = (2.0 * j * Math.PI) / 16.0;
                    circle[j].X = 0.5f * (1.0f + (float)Math.Cos(angle));
                    circle[j].Y = 0.5f * (1.0f - (float)Math.Sin(angle));
                }

                Matrix cm = new Matrix(RectangleF.FromLTRB(0.0f, 1.0f, 1.0f, 0.0f),
                        new PointF[]
                        {
                                    new PointF(aBounds.Left,  aBounds.Top),
                                    new PointF(aBounds.Right, aBounds.Top),
                                    new PointF(aBounds.Left,  aBounds.Bottom)
                        });

                cm.TransformPoints(circle);
                foreach (PointF p in circle)
                {
                    polygon.Add(p);
                }
            }

            PointF[] temp = polygon.ToArray();
            m.TransformPoints(temp);
            polygon = temp.ToList();

            BlurRegion br = new BlurRegion();
            br.Polygon = polygon.Select(p => new Vector2(p)).ToList();
            BlurRegions.Add(br);
        }
    
        #endregion
        #region ----- Nonserialized properties -----

        [XmlIgnore]
        public Vector2 AccumulatedJiggleCorrection { get; set; }

        [XmlIgnore]
        public int Index { get; set; }

        [XmlIgnore]
        public Project Project { get; set; }

        #endregion
        #region ----- Serialized properties -----

        public string SourcePath { get; set; }

        public string ImportSession { get; set; }

        public DateTime Time { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public float Altitude { get; set; }

        public float? AltitudeSmoothed { get; set; }

        public bool CoordinatesValid { get; set; }

        public float? Heading { get; set; }

        public bool ShowOverlays { get; set; }

        // Video tracks this frame belongs to.
        public List<uint> Sequences { get; set; } 

        public List<int> TextOverlayIDs { get; set; }

        public List<BlurRegion> BlurRegions { get; set; }

        public bool PersistentPropertiesEdited { get; set; }

        public List<string> MapsToRender { get; set; }

        public string MapImageFilename { get; set; }

        public int FrameMultiple { get; set; }

        public Vector2 JiggleCorrection { get; set; }

        public float AngleCorrection { get; set; }

        public CompassUpdateMode CompassMode { get; set; }

        #endregion
    }
}
