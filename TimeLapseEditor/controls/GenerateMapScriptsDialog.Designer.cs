﻿namespace TimeLapseEditor
{
    partial class GenerateMapScriptsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.mBatchSizeBox = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.mRangeAllButton = new System.Windows.Forms.RadioButton();
            this.mRangeSpecifyButton = new System.Windows.Forms.RadioButton();
            this.mRangeMinBox = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.mRangeMaxBox = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.mWidthBox = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.mHeightBox = new System.Windows.Forms.NumericUpDown();
            this.mGenerateButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.mBaseNameBox = new System.Windows.Forms.TextBox();
            this.mProgressBar = new System.Windows.Forms.ProgressBar();
            this.mOnlyMissingButton = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.mBatchSizeBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mRangeMinBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mRangeMaxBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mWidthBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mHeightBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Batch Size:";
            // 
            // mBatchSizeBox
            // 
            this.mBatchSizeBox.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mBatchSizeBox.Location = new System.Drawing.Point(65, 7);
            this.mBatchSizeBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mBatchSizeBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mBatchSizeBox.Name = "mBatchSizeBox";
            this.mBatchSizeBox.Size = new System.Drawing.Size(70, 20);
            this.mBatchSizeBox.TabIndex = 2;
            this.mBatchSizeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mBatchSizeBox.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Range:";
            // 
            // mRangeAllButton
            // 
            this.mRangeAllButton.AutoSize = true;
            this.mRangeAllButton.Checked = true;
            this.mRangeAllButton.Location = new System.Drawing.Point(50, 34);
            this.mRangeAllButton.Name = "mRangeAllButton";
            this.mRangeAllButton.Size = new System.Drawing.Size(36, 17);
            this.mRangeAllButton.TabIndex = 4;
            this.mRangeAllButton.TabStop = true;
            this.mRangeAllButton.Text = "All";
            this.mRangeAllButton.UseVisualStyleBackColor = true;
            // 
            // mRangeSpecifyButton
            // 
            this.mRangeSpecifyButton.AutoSize = true;
            this.mRangeSpecifyButton.Location = new System.Drawing.Point(50, 57);
            this.mRangeSpecifyButton.Name = "mRangeSpecifyButton";
            this.mRangeSpecifyButton.Size = new System.Drawing.Size(60, 17);
            this.mRangeSpecifyButton.TabIndex = 5;
            this.mRangeSpecifyButton.Text = "Range:";
            this.mRangeSpecifyButton.UseVisualStyleBackColor = true;
            this.mRangeSpecifyButton.CheckedChanged += new System.EventHandler(this.mRangeSpecifyButton_CheckedChanged);
            // 
            // mRangeMinBox
            // 
            this.mRangeMinBox.Enabled = false;
            this.mRangeMinBox.Location = new System.Drawing.Point(116, 57);
            this.mRangeMinBox.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.mRangeMinBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mRangeMinBox.Name = "mRangeMinBox";
            this.mRangeMinBox.Size = new System.Drawing.Size(69, 20);
            this.mRangeMinBox.TabIndex = 6;
            this.mRangeMinBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mRangeMinBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mRangeMinBox.ValueChanged += new System.EventHandler(this.mRangeMinBox_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(193, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "to";
            // 
            // mRangeMaxBox
            // 
            this.mRangeMaxBox.Enabled = false;
            this.mRangeMaxBox.Location = new System.Drawing.Point(215, 57);
            this.mRangeMaxBox.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.mRangeMaxBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mRangeMaxBox.Name = "mRangeMaxBox";
            this.mRangeMaxBox.Size = new System.Drawing.Size(75, 20);
            this.mRangeMaxBox.TabIndex = 8;
            this.mRangeMaxBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mRangeMaxBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.mRangeMaxBox.ValueChanged += new System.EventHandler(this.mRangeMaxBox_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Output image dimensions:";
            // 
            // mWidthBox
            // 
            this.mWidthBox.Location = new System.Drawing.Point(136, 88);
            this.mWidthBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mWidthBox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mWidthBox.Name = "mWidthBox";
            this.mWidthBox.Size = new System.Drawing.Size(73, 20);
            this.mWidthBox.TabIndex = 12;
            this.mWidthBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mWidthBox.Value = new decimal(new int[] {
            640,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(215, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "x";
            // 
            // mHeightBox
            // 
            this.mHeightBox.Location = new System.Drawing.Point(233, 88);
            this.mHeightBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mHeightBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mHeightBox.Name = "mHeightBox";
            this.mHeightBox.Size = new System.Drawing.Size(75, 20);
            this.mHeightBox.TabIndex = 14;
            this.mHeightBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mHeightBox.Value = new decimal(new int[] {
            480,
            0,
            0,
            0});
            // 
            // mGenerateButton
            // 
            this.mGenerateButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mGenerateButton.Location = new System.Drawing.Point(155, 150);
            this.mGenerateButton.Name = "mGenerateButton";
            this.mGenerateButton.Size = new System.Drawing.Size(195, 23);
            this.mGenerateButton.TabIndex = 15;
            this.mGenerateButton.Text = "Generate!";
            this.mGenerateButton.UseVisualStyleBackColor = true;
            this.mGenerateButton.Click += new System.EventHandler(this.mGenerateButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(277, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Base Script Filename:";
            // 
            // mBaseNameBox
            // 
            this.mBaseNameBox.Location = new System.Drawing.Point(392, 6);
            this.mBaseNameBox.Name = "mBaseNameBox";
            this.mBaseNameBox.Size = new System.Drawing.Size(118, 20);
            this.mBaseNameBox.TabIndex = 17;
            this.mBaseNameBox.Text = "script_";
            // 
            // mProgressBar
            // 
            this.mProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mProgressBar.Location = new System.Drawing.Point(155, 121);
            this.mProgressBar.Name = "mProgressBar";
            this.mProgressBar.Size = new System.Drawing.Size(195, 23);
            this.mProgressBar.Step = 1;
            this.mProgressBar.TabIndex = 18;
            // 
            // mOnlyMissingButton
            // 
            this.mOnlyMissingButton.AutoSize = true;
            this.mOnlyMissingButton.Location = new System.Drawing.Point(334, 46);
            this.mOnlyMissingButton.Name = "mOnlyMissingButton";
            this.mOnlyMissingButton.Size = new System.Drawing.Size(85, 17);
            this.mOnlyMissingButton.TabIndex = 19;
            this.mOnlyMissingButton.Text = "Only Missing";
            this.mOnlyMissingButton.UseVisualStyleBackColor = true;
            // 
            // GenerateMapScriptsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 179);
            this.Controls.Add(this.mOnlyMissingButton);
            this.Controls.Add(this.mProgressBar);
            this.Controls.Add(this.mBaseNameBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.mGenerateButton);
            this.Controls.Add(this.mHeightBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.mWidthBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.mRangeMaxBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.mRangeMinBox);
            this.Controls.Add(this.mRangeSpecifyButton);
            this.Controls.Add(this.mRangeAllButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mBatchSizeBox);
            this.Controls.Add(this.label1);
            this.Name = "GenerateMapScriptsDialog";
            this.Text = "GenerateMapScriptsDialog";
            ((System.ComponentModel.ISupportInitialize)(this.mBatchSizeBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mRangeMinBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mRangeMaxBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mWidthBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mHeightBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown mBatchSizeBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton mRangeAllButton;
        private System.Windows.Forms.RadioButton mRangeSpecifyButton;
        private System.Windows.Forms.NumericUpDown mRangeMinBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown mRangeMaxBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown mWidthBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown mHeightBox;
        private System.Windows.Forms.Button mGenerateButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox mBaseNameBox;
        private System.Windows.Forms.ProgressBar mProgressBar;
        private System.Windows.Forms.CheckBox mOnlyMissingButton;
    }
}