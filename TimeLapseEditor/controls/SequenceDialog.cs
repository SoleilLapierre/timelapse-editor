﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Windows.Forms;

namespace TimeLapseEditor
{
    public partial class SequenceDialog : Form
    {
        public SequenceDialog()
        {
            InitializeComponent();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }


        public SequenceDialog(string aTitle)
        : this()
        {
            Text = aTitle;
        }


        public SequenceDialog(string aTitle, string aText)
        : this(aTitle)
        {
            Value = aText;
        }   


        public string Value
        {
            get { return mTextBox.Text; }
            set { mTextBox.Text = value; }
        }


        public bool ShowOverlays
        {
            get { return mShowOverlaysCheckBox.Checked; }
            set { mShowOverlaysCheckBox.Checked = value; }
        }


        private void mOkButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }


        private void mCancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
