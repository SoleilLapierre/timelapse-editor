﻿namespace TimeLapseEditor
{
    partial class BatchDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mActionLabel = new System.Windows.Forms.Label();
            this.mFirstFrameBox = new System.Windows.Forms.NumericUpDown();
            this.mLastFrameBox = new System.Windows.Forms.NumericUpDown();
            this.mGoButton = new System.Windows.Forms.Button();
            this.mCancelButton = new System.Windows.Forms.Button();
            this.mProgressBar = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.mIndexLabel = new System.Windows.Forms.Label();
            this.mOptionCheckbox = new System.Windows.Forms.CheckBox();
            this.mStartButton = new System.Windows.Forms.Button();
            this.mMinus100Button = new System.Windows.Forms.Button();
            this.mPlus100Button = new System.Windows.Forms.Button();
            this.mEndButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mFirstFrameBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLastFrameBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mActionLabel
            // 
            this.mActionLabel.AutoSize = true;
            this.mActionLabel.Location = new System.Drawing.Point(13, 13);
            this.mActionLabel.Name = "mActionLabel";
            this.mActionLabel.Size = new System.Drawing.Size(35, 13);
            this.mActionLabel.TabIndex = 0;
            this.mActionLabel.Text = "label1";
            // 
            // mFirstFrameBox
            // 
            this.mFirstFrameBox.Location = new System.Drawing.Point(73, 61);
            this.mFirstFrameBox.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.mFirstFrameBox.Name = "mFirstFrameBox";
            this.mFirstFrameBox.Size = new System.Drawing.Size(92, 20);
            this.mFirstFrameBox.TabIndex = 1;
            this.mFirstFrameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mLastFrameBox
            // 
            this.mLastFrameBox.Location = new System.Drawing.Point(73, 86);
            this.mLastFrameBox.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.mLastFrameBox.Name = "mLastFrameBox";
            this.mLastFrameBox.Size = new System.Drawing.Size(92, 20);
            this.mLastFrameBox.TabIndex = 2;
            this.mLastFrameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mGoButton
            // 
            this.mGoButton.Location = new System.Drawing.Point(90, 141);
            this.mGoButton.Name = "mGoButton";
            this.mGoButton.Size = new System.Drawing.Size(75, 23);
            this.mGoButton.TabIndex = 3;
            this.mGoButton.Text = "GO";
            this.mGoButton.UseVisualStyleBackColor = true;
            this.mGoButton.Click += new System.EventHandler(this.mGoButton_Click);
            // 
            // mCancelButton
            // 
            this.mCancelButton.Location = new System.Drawing.Point(171, 141);
            this.mCancelButton.Name = "mCancelButton";
            this.mCancelButton.Size = new System.Drawing.Size(75, 23);
            this.mCancelButton.TabIndex = 4;
            this.mCancelButton.Text = "Cancel";
            this.mCancelButton.UseVisualStyleBackColor = true;
            this.mCancelButton.Click += new System.EventHandler(this.mCancelButton_Click);
            // 
            // mProgressBar
            // 
            this.mProgressBar.Location = new System.Drawing.Point(15, 112);
            this.mProgressBar.Name = "mProgressBar";
            this.mProgressBar.Size = new System.Drawing.Size(231, 23);
            this.mProgressBar.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "First Frame:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Last Frame:";
            // 
            // mIndexLabel
            // 
            this.mIndexLabel.AutoSize = true;
            this.mIndexLabel.Location = new System.Drawing.Point(100, 117);
            this.mIndexLabel.Name = "mIndexLabel";
            this.mIndexLabel.Size = new System.Drawing.Size(13, 13);
            this.mIndexLabel.TabIndex = 8;
            this.mIndexLabel.Text = "0";
            this.mIndexLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mOptionCheckbox
            // 
            this.mOptionCheckbox.AutoSize = true;
            this.mOptionCheckbox.Checked = true;
            this.mOptionCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mOptionCheckbox.Location = new System.Drawing.Point(73, 38);
            this.mOptionCheckbox.Name = "mOptionCheckbox";
            this.mOptionCheckbox.Size = new System.Drawing.Size(57, 17);
            this.mOptionCheckbox.TabIndex = 9;
            this.mOptionCheckbox.Text = "Option";
            this.mOptionCheckbox.UseVisualStyleBackColor = true;
            // 
            // mStartButton
            // 
            this.mStartButton.Location = new System.Drawing.Point(172, 58);
            this.mStartButton.Name = "mStartButton";
            this.mStartButton.Size = new System.Drawing.Size(38, 23);
            this.mStartButton.TabIndex = 10;
            this.mStartButton.Text = "|<--";
            this.mStartButton.UseVisualStyleBackColor = true;
            this.mStartButton.Click += new System.EventHandler(this.mStartButton_Click);
            // 
            // mMinus100Button
            // 
            this.mMinus100Button.Location = new System.Drawing.Point(216, 58);
            this.mMinus100Button.Name = "mMinus100Button";
            this.mMinus100Button.Size = new System.Drawing.Size(37, 23);
            this.mMinus100Button.TabIndex = 11;
            this.mMinus100Button.Text = "-100";
            this.mMinus100Button.UseVisualStyleBackColor = true;
            this.mMinus100Button.Click += new System.EventHandler(this.mMinus100Button_Click);
            // 
            // mPlus100Button
            // 
            this.mPlus100Button.Location = new System.Drawing.Point(171, 83);
            this.mPlus100Button.Name = "mPlus100Button";
            this.mPlus100Button.Size = new System.Drawing.Size(39, 23);
            this.mPlus100Button.TabIndex = 12;
            this.mPlus100Button.Text = "+100";
            this.mPlus100Button.UseVisualStyleBackColor = true;
            this.mPlus100Button.Click += new System.EventHandler(this.mPlus100Button_Click);
            // 
            // mEndButton
            // 
            this.mEndButton.Location = new System.Drawing.Point(216, 83);
            this.mEndButton.Name = "mEndButton";
            this.mEndButton.Size = new System.Drawing.Size(37, 23);
            this.mEndButton.TabIndex = 13;
            this.mEndButton.Text = "-->|";
            this.mEndButton.UseVisualStyleBackColor = true;
            this.mEndButton.Click += new System.EventHandler(this.mEndButton_Click);
            // 
            // BatchDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 174);
            this.Controls.Add(this.mEndButton);
            this.Controls.Add(this.mPlus100Button);
            this.Controls.Add(this.mMinus100Button);
            this.Controls.Add(this.mStartButton);
            this.Controls.Add(this.mOptionCheckbox);
            this.Controls.Add(this.mIndexLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mProgressBar);
            this.Controls.Add(this.mCancelButton);
            this.Controls.Add(this.mGoButton);
            this.Controls.Add(this.mLastFrameBox);
            this.Controls.Add(this.mFirstFrameBox);
            this.Controls.Add(this.mActionLabel);
            this.Name = "BatchDialog";
            this.Text = "BatchDialog";
            ((System.ComponentModel.ISupportInitialize)(this.mFirstFrameBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLastFrameBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label mActionLabel;
        private System.Windows.Forms.NumericUpDown mFirstFrameBox;
        private System.Windows.Forms.NumericUpDown mLastFrameBox;
        private System.Windows.Forms.Button mGoButton;
        private System.Windows.Forms.Button mCancelButton;
        private System.Windows.Forms.ProgressBar mProgressBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label mIndexLabel;
        private System.Windows.Forms.CheckBox mOptionCheckbox;
        private System.Windows.Forms.Button mStartButton;
        private System.Windows.Forms.Button mMinus100Button;
        private System.Windows.Forms.Button mPlus100Button;
        private System.Windows.Forms.Button mEndButton;
    }
}