﻿namespace TimeLapseEditor
{
    partial class FileListDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mSessionListBox = new System.Windows.Forms.ListBox();
            this.mInsertSessionButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mAddSessionButton = new System.Windows.Forms.Button();
            this.mSessionRenameButton = new System.Windows.Forms.Button();
            this.mDeleteSessionButton = new System.Windows.Forms.Button();
            this.mFileGroupBox = new System.Windows.Forms.GroupBox();
            this.mPasteButton = new System.Windows.Forms.Button();
            this.mCopyButton = new System.Windows.Forms.Button();
            this.mCutButton = new System.Windows.Forms.Button();
            this.mAddFilesButton = new System.Windows.Forms.Button();
            this.mCloseButton = new System.Windows.Forms.Button();
            this.mProgressBar = new System.Windows.Forms.ProgressBar();
            this.mFindMapsButton = new System.Windows.Forms.Button();
            this.mPreviewBox = new System.Windows.Forms.PictureBox();
            this.mSmoothAltitudeButton = new System.Windows.Forms.Button();
            this.mUpdateCompassButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.mFileGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mPreviewBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mListView
            // 
            this.mListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader4,
            this.columnHeader6,
            this.columnHeader5,
            this.columnHeader7,
            this.columnHeader3,
            this.columnHeader2,
            this.columnHeader8,
            this.columnHeader9});
            this.mListView.GridLines = true;
            this.mListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.mListView.HideSelection = false;
            this.mListView.Location = new System.Drawing.Point(6, 19);
            this.mListView.Name = "mListView";
            this.mListView.ShowGroups = false;
            this.mListView.Size = new System.Drawing.Size(712, 475);
            this.mListView.TabIndex = 1;
            this.mListView.UseCompatibleStateImageBehavior = false;
            this.mListView.View = System.Windows.Forms.View.Details;
            this.mListView.SelectedIndexChanged += new System.EventHandler(this.mListView_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Filename";
            this.columnHeader1.Width = 160;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Timestamp";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.DisplayIndex = 5;
            this.columnHeader6.Text = "Longitude";
            // 
            // columnHeader5
            // 
            this.columnHeader5.DisplayIndex = 4;
            this.columnHeader5.Text = "Latitude";
            // 
            // columnHeader7
            // 
            this.columnHeader7.DisplayIndex = 6;
            this.columnHeader7.Text = "Altitude";
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 2;
            this.columnHeader3.Text = "Tracks";
            this.columnHeader3.Width = 300;
            // 
            // columnHeader2
            // 
            this.columnHeader2.DisplayIndex = 3;
            this.columnHeader2.Text = "Path";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Map Image";
            this.columnHeader8.Width = 300;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Frame Index";
            // 
            // mSessionListBox
            // 
            this.mSessionListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mSessionListBox.FormattingEnabled = true;
            this.mSessionListBox.Location = new System.Drawing.Point(6, 19);
            this.mSessionListBox.Name = "mSessionListBox";
            this.mSessionListBox.Size = new System.Drawing.Size(211, 303);
            this.mSessionListBox.TabIndex = 2;
            this.mSessionListBox.SelectedIndexChanged += new System.EventHandler(this.mSessionListBox_SelectedIndexChanged);
            // 
            // mInsertSessionButton
            // 
            this.mInsertSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mInsertSessionButton.Location = new System.Drawing.Point(6, 367);
            this.mInsertSessionButton.Name = "mInsertSessionButton";
            this.mInsertSessionButton.Size = new System.Drawing.Size(75, 23);
            this.mInsertSessionButton.TabIndex = 5;
            this.mInsertSessionButton.Text = "Insert";
            this.mInsertSessionButton.UseVisualStyleBackColor = true;
            this.mInsertSessionButton.Click += new System.EventHandler(this.mInsertSessionButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.mAddSessionButton);
            this.groupBox1.Controls.Add(this.mSessionRenameButton);
            this.groupBox1.Controls.Add(this.mDeleteSessionButton);
            this.groupBox1.Controls.Add(this.mSessionListBox);
            this.groupBox1.Controls.Add(this.mInsertSessionButton);
            this.groupBox1.Location = new System.Drawing.Point(0, 145);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(217, 396);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sessions";
            // 
            // mAddSessionButton
            // 
            this.mAddSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mAddSessionButton.Location = new System.Drawing.Point(6, 338);
            this.mAddSessionButton.Name = "mAddSessionButton";
            this.mAddSessionButton.Size = new System.Drawing.Size(75, 23);
            this.mAddSessionButton.TabIndex = 8;
            this.mAddSessionButton.Text = "Add";
            this.mAddSessionButton.UseVisualStyleBackColor = true;
            this.mAddSessionButton.Click += new System.EventHandler(this.mAddSessionButton_Click);
            // 
            // mSessionRenameButton
            // 
            this.mSessionRenameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mSessionRenameButton.Enabled = false;
            this.mSessionRenameButton.Location = new System.Drawing.Point(136, 338);
            this.mSessionRenameButton.Name = "mSessionRenameButton";
            this.mSessionRenameButton.Size = new System.Drawing.Size(75, 23);
            this.mSessionRenameButton.TabIndex = 7;
            this.mSessionRenameButton.Text = "Rename";
            this.mSessionRenameButton.UseVisualStyleBackColor = true;
            this.mSessionRenameButton.Click += new System.EventHandler(this.mSessionRenameButton_Click);
            // 
            // mDeleteSessionButton
            // 
            this.mDeleteSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mDeleteSessionButton.Enabled = false;
            this.mDeleteSessionButton.Location = new System.Drawing.Point(136, 367);
            this.mDeleteSessionButton.Name = "mDeleteSessionButton";
            this.mDeleteSessionButton.Size = new System.Drawing.Size(75, 23);
            this.mDeleteSessionButton.TabIndex = 6;
            this.mDeleteSessionButton.Text = "Delete";
            this.mDeleteSessionButton.UseVisualStyleBackColor = true;
            this.mDeleteSessionButton.Click += new System.EventHandler(this.mDeleteSessionButton_Click);
            // 
            // mFileGroupBox
            // 
            this.mFileGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mFileGroupBox.Controls.Add(this.mPasteButton);
            this.mFileGroupBox.Controls.Add(this.mCopyButton);
            this.mFileGroupBox.Controls.Add(this.mCutButton);
            this.mFileGroupBox.Controls.Add(this.mAddFilesButton);
            this.mFileGroupBox.Controls.Add(this.mListView);
            this.mFileGroupBox.Location = new System.Drawing.Point(217, 12);
            this.mFileGroupBox.Name = "mFileGroupBox";
            this.mFileGroupBox.Size = new System.Drawing.Size(730, 529);
            this.mFileGroupBox.TabIndex = 7;
            this.mFileGroupBox.TabStop = false;
            this.mFileGroupBox.Text = "Files in Selected Session";
            // 
            // mPasteButton
            // 
            this.mPasteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mPasteButton.Enabled = false;
            this.mPasteButton.Location = new System.Drawing.Point(643, 500);
            this.mPasteButton.Name = "mPasteButton";
            this.mPasteButton.Size = new System.Drawing.Size(75, 23);
            this.mPasteButton.TabIndex = 6;
            this.mPasteButton.Text = "Paste";
            this.mPasteButton.UseVisualStyleBackColor = true;
            this.mPasteButton.Click += new System.EventHandler(this.mPasteButton_Click);
            // 
            // mCopyButton
            // 
            this.mCopyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mCopyButton.Enabled = false;
            this.mCopyButton.Location = new System.Drawing.Point(562, 500);
            this.mCopyButton.Name = "mCopyButton";
            this.mCopyButton.Size = new System.Drawing.Size(75, 23);
            this.mCopyButton.TabIndex = 5;
            this.mCopyButton.Text = "Copy";
            this.mCopyButton.UseVisualStyleBackColor = true;
            this.mCopyButton.Click += new System.EventHandler(this.mCopyButton_Click);
            // 
            // mCutButton
            // 
            this.mCutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mCutButton.Enabled = false;
            this.mCutButton.Location = new System.Drawing.Point(481, 500);
            this.mCutButton.Name = "mCutButton";
            this.mCutButton.Size = new System.Drawing.Size(75, 23);
            this.mCutButton.TabIndex = 4;
            this.mCutButton.Text = "Cut";
            this.mCutButton.UseVisualStyleBackColor = true;
            this.mCutButton.Click += new System.EventHandler(this.mCutButton_Click);
            // 
            // mAddFilesButton
            // 
            this.mAddFilesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mAddFilesButton.Location = new System.Drawing.Point(148, 500);
            this.mAddFilesButton.Name = "mAddFilesButton";
            this.mAddFilesButton.Size = new System.Drawing.Size(125, 23);
            this.mAddFilesButton.TabIndex = 3;
            this.mAddFilesButton.Text = "Insert Files...";
            this.mAddFilesButton.UseVisualStyleBackColor = true;
            this.mAddFilesButton.Click += new System.EventHandler(this.mAddFilesButton_Click);
            // 
            // mCloseButton
            // 
            this.mCloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mCloseButton.Location = new System.Drawing.Point(860, 547);
            this.mCloseButton.Name = "mCloseButton";
            this.mCloseButton.Size = new System.Drawing.Size(75, 23);
            this.mCloseButton.TabIndex = 2;
            this.mCloseButton.Text = "Close";
            this.mCloseButton.UseVisualStyleBackColor = true;
            this.mCloseButton.Click += new System.EventHandler(this.mCloseButton_Click);
            // 
            // mProgressBar
            // 
            this.mProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mProgressBar.Location = new System.Drawing.Point(6, 547);
            this.mProgressBar.Name = "mProgressBar";
            this.mProgressBar.Size = new System.Drawing.Size(382, 23);
            this.mProgressBar.Step = 1;
            this.mProgressBar.TabIndex = 7;
            // 
            // mFindMapsButton
            // 
            this.mFindMapsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mFindMapsButton.Location = new System.Drawing.Point(394, 547);
            this.mFindMapsButton.Name = "mFindMapsButton";
            this.mFindMapsButton.Size = new System.Drawing.Size(210, 23);
            this.mFindMapsButton.TabIndex = 8;
            this.mFindMapsButton.Text = "Find Maps for All Images";
            this.mFindMapsButton.UseVisualStyleBackColor = true;
            this.mFindMapsButton.Click += new System.EventHandler(this.mFindMapsButton_Click);
            // 
            // mPreviewBox
            // 
            this.mPreviewBox.Location = new System.Drawing.Point(6, 12);
            this.mPreviewBox.Name = "mPreviewBox";
            this.mPreviewBox.Size = new System.Drawing.Size(205, 127);
            this.mPreviewBox.TabIndex = 7;
            this.mPreviewBox.TabStop = false;
            // 
            // mSmoothAltitudeButton
            // 
            this.mSmoothAltitudeButton.Location = new System.Drawing.Point(610, 547);
            this.mSmoothAltitudeButton.Name = "mSmoothAltitudeButton";
            this.mSmoothAltitudeButton.Size = new System.Drawing.Size(126, 23);
            this.mSmoothAltitudeButton.TabIndex = 9;
            this.mSmoothAltitudeButton.Text = "Smooth Altitudes";
            this.mSmoothAltitudeButton.UseVisualStyleBackColor = true;
            this.mSmoothAltitudeButton.Click += new System.EventHandler(this.mSmoothAltitudeButton_Click);
            // 
            // mUpdateCompassButton
            // 
            this.mUpdateCompassButton.Location = new System.Drawing.Point(742, 547);
            this.mUpdateCompassButton.Name = "mUpdateCompassButton";
            this.mUpdateCompassButton.Size = new System.Drawing.Size(112, 23);
            this.mUpdateCompassButton.TabIndex = 10;
            this.mUpdateCompassButton.Text = "Update Compass";
            this.mUpdateCompassButton.UseVisualStyleBackColor = true;
            this.mUpdateCompassButton.Click += new System.EventHandler(this.mUpdateCompassButton_Click);
            // 
            // FileListDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 580);
            this.Controls.Add(this.mUpdateCompassButton);
            this.Controls.Add(this.mSmoothAltitudeButton);
            this.Controls.Add(this.mPreviewBox);
            this.Controls.Add(this.mProgressBar);
            this.Controls.Add(this.mFindMapsButton);
            this.Controls.Add(this.mFileGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.mCloseButton);
            this.Name = "FileListDialog";
            this.Text = "FileListDialog";
            this.groupBox1.ResumeLayout(false);
            this.mFileGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mPreviewBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView mListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ListBox mSessionListBox;
        private System.Windows.Forms.Button mInsertSessionButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button mDeleteSessionButton;
        private System.Windows.Forms.GroupBox mFileGroupBox;
        private System.Windows.Forms.Button mPasteButton;
        private System.Windows.Forms.Button mCopyButton;
        private System.Windows.Forms.Button mCutButton;
        private System.Windows.Forms.Button mAddFilesButton;
        private System.Windows.Forms.Button mCloseButton;
        private System.Windows.Forms.Button mSessionRenameButton;
        private System.Windows.Forms.ProgressBar mProgressBar;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button mFindMapsButton;
        private System.Windows.Forms.PictureBox mPreviewBox;
        private System.Windows.Forms.Button mAddSessionButton;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.Button mSmoothAltitudeButton;
        private System.Windows.Forms.Button mUpdateCompassButton;
    }
}