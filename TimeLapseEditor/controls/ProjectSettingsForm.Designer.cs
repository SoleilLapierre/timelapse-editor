﻿namespace TimeLapseEditor
{
    partial class ProjectSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mCancelButton = new System.Windows.Forms.Button();
            this.mOkButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mProjectNameBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mImageHeightBox = new System.Windows.Forms.TextBox();
            this.mImageWidthBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.mSequenceRemoveButton = new System.Windows.Forms.Button();
            this.mSequenceMoveDownButton = new System.Windows.Forms.Button();
            this.mSequenceMoveUpButton = new System.Windows.Forms.Button();
            this.mSequenceRenameButton = new System.Windows.Forms.Button();
            this.mSequenceAddButton = new System.Windows.Forms.Button();
            this.mSequenceListBox = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mGuidelineColorButton = new System.Windows.Forms.Button();
            this.mGuidelineX2Box = new System.Windows.Forms.NumericUpDown();
            this.mGuidelineX1Box = new System.Windows.Forms.NumericUpDown();
            this.mGuidelineY2Box = new System.Windows.Forms.NumericUpDown();
            this.mGuidelineY1Box = new System.Windows.Forms.NumericUpDown();
            this.mGuidelineRemoveButton = new System.Windows.Forms.Button();
            this.mGuidelineAddButton = new System.Windows.Forms.Button();
            this.mGuidelineListBox = new System.Windows.Forms.ListBox();
            this.mTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.mCompassImagePathBox = new ManagedCowTools.WinForms.PathWidget();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.mCompassScaleBox = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mCompassYBox = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.mCompassXBox = new System.Windows.Forms.NumericUpDown();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.mMapRulesBox = new ManagedCowTools.WinForms.PathWidget();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.mMapImageHeightBox = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.mMapImageWidthBox = new System.Windows.Forms.NumericUpDown();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.mMapAlphaBox = new System.Windows.Forms.NumericUpDown();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.mOsmPathBox = new ManagedCowTools.WinForms.PathWidget();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.mMapPathBox = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.mMapScaleBox = new System.Windows.Forms.NumericUpDown();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.mMapPositionYBox = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.mMapPositionXBox = new System.Windows.Forms.NumericUpDown();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.mOutlineAlphaBox = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.mOutlineThicknessButton = new System.Windows.Forms.NumericUpDown();
            this.mOutlineColorButton = new System.Windows.Forms.Button();
            this.mHudTextColorButton = new System.Windows.Forms.Button();
            this.mHudTextFontButton = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.mHudTextYBox = new System.Windows.Forms.NumericUpDown();
            this.mHudTextXBox = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.mAutoRegisterSearchRadiusBox = new System.Windows.Forms.NumericUpDown();
            this.mAutoRegisterCenterYBox = new System.Windows.Forms.NumericUpDown();
            this.mAutoRegisterCenterXBox = new System.Windows.Forms.NumericUpDown();
            this.mAutoregisterMaxDeltaBox = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineX2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineX1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineY2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineY1Box)).BeginInit();
            this.mTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCompassScaleBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCompassYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCompassXBox)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mMapImageHeightBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mMapImageWidthBox)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mMapAlphaBox)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mMapScaleBox)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mMapPositionYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mMapPositionXBox)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOutlineAlphaBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mOutlineThicknessButton)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mHudTextYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mHudTextXBox)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoRegisterSearchRadiusBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoRegisterCenterYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoRegisterCenterXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoregisterMaxDeltaBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mCancelButton
            // 
            this.mCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mCancelButton.Location = new System.Drawing.Point(597, 427);
            this.mCancelButton.Name = "mCancelButton";
            this.mCancelButton.Size = new System.Drawing.Size(75, 23);
            this.mCancelButton.TabIndex = 0;
            this.mCancelButton.Text = "Cancel";
            this.mCancelButton.UseVisualStyleBackColor = true;
            this.mCancelButton.Click += new System.EventHandler(this.mCancelButton_Click);
            // 
            // mOkButton
            // 
            this.mOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mOkButton.Location = new System.Drawing.Point(516, 427);
            this.mOkButton.Name = "mOkButton";
            this.mOkButton.Size = new System.Drawing.Size(75, 23);
            this.mOkButton.TabIndex = 1;
            this.mOkButton.Text = "OK";
            this.mOkButton.UseVisualStyleBackColor = true;
            this.mOkButton.Click += new System.EventHandler(this.mOkButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.mProjectNameBox);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(640, 49);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Project Name";
            // 
            // mProjectNameBox
            // 
            this.mProjectNameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mProjectNameBox.Location = new System.Drawing.Point(7, 20);
            this.mProjectNameBox.Name = "mProjectNameBox";
            this.mProjectNameBox.Size = new System.Drawing.Size(627, 20);
            this.mProjectNameBox.TabIndex = 0;
            this.mProjectNameBox.Leave += new System.EventHandler(this.mProjectNameBox_Leave);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.mImageHeightBox);
            this.groupBox3.Controls.Add(this.mImageWidthBox);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(306, 49);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Output Image Size";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "x";
            // 
            // mImageHeightBox
            // 
            this.mImageHeightBox.Location = new System.Drawing.Point(131, 20);
            this.mImageHeightBox.Name = "mImageHeightBox";
            this.mImageHeightBox.Size = new System.Drawing.Size(100, 20);
            this.mImageHeightBox.TabIndex = 1;
            this.mImageHeightBox.Validating += new System.ComponentModel.CancelEventHandler(this.mImageHeightBox_Validating);
            this.mImageHeightBox.Validated += new System.EventHandler(this.mImageHeightBox_Validated);
            // 
            // mImageWidthBox
            // 
            this.mImageWidthBox.Location = new System.Drawing.Point(7, 20);
            this.mImageWidthBox.Name = "mImageWidthBox";
            this.mImageWidthBox.Size = new System.Drawing.Size(100, 20);
            this.mImageWidthBox.TabIndex = 0;
            this.mImageWidthBox.Validating += new System.ComponentModel.CancelEventHandler(this.mImageWidthBox_Validating);
            this.mImageWidthBox.Validated += new System.EventHandler(this.mImageWidthBox_Validated);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.mSequenceRemoveButton);
            this.groupBox4.Controls.Add(this.mSequenceMoveDownButton);
            this.groupBox4.Controls.Add(this.mSequenceMoveUpButton);
            this.groupBox4.Controls.Add(this.mSequenceRenameButton);
            this.groupBox4.Controls.Add(this.mSequenceAddButton);
            this.groupBox4.Controls.Add(this.mSequenceListBox);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(640, 371);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sequences";
            // 
            // mSequenceRemoveButton
            // 
            this.mSequenceRemoveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mSequenceRemoveButton.Enabled = false;
            this.mSequenceRemoveButton.Location = new System.Drawing.Point(556, 140);
            this.mSequenceRemoveButton.Name = "mSequenceRemoveButton";
            this.mSequenceRemoveButton.Size = new System.Drawing.Size(75, 23);
            this.mSequenceRemoveButton.TabIndex = 5;
            this.mSequenceRemoveButton.Text = "Remove";
            this.mSequenceRemoveButton.UseVisualStyleBackColor = true;
            this.mSequenceRemoveButton.Click += new System.EventHandler(this.mSequenceRemoveButton_Click);
            // 
            // mSequenceMoveDownButton
            // 
            this.mSequenceMoveDownButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mSequenceMoveDownButton.Enabled = false;
            this.mSequenceMoveDownButton.Location = new System.Drawing.Point(556, 110);
            this.mSequenceMoveDownButton.Name = "mSequenceMoveDownButton";
            this.mSequenceMoveDownButton.Size = new System.Drawing.Size(75, 23);
            this.mSequenceMoveDownButton.TabIndex = 4;
            this.mSequenceMoveDownButton.Text = "Move Down";
            this.mSequenceMoveDownButton.UseVisualStyleBackColor = true;
            this.mSequenceMoveDownButton.Click += new System.EventHandler(this.mSequenceMoveDownButton_Click);
            // 
            // mSequenceMoveUpButton
            // 
            this.mSequenceMoveUpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mSequenceMoveUpButton.Enabled = false;
            this.mSequenceMoveUpButton.Location = new System.Drawing.Point(556, 80);
            this.mSequenceMoveUpButton.Name = "mSequenceMoveUpButton";
            this.mSequenceMoveUpButton.Size = new System.Drawing.Size(75, 23);
            this.mSequenceMoveUpButton.TabIndex = 3;
            this.mSequenceMoveUpButton.Text = "Move Up";
            this.mSequenceMoveUpButton.UseVisualStyleBackColor = true;
            this.mSequenceMoveUpButton.Click += new System.EventHandler(this.mSequenceMoveUpButton_Click);
            // 
            // mSequenceRenameButton
            // 
            this.mSequenceRenameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mSequenceRenameButton.Enabled = false;
            this.mSequenceRenameButton.Location = new System.Drawing.Point(556, 50);
            this.mSequenceRenameButton.Name = "mSequenceRenameButton";
            this.mSequenceRenameButton.Size = new System.Drawing.Size(75, 23);
            this.mSequenceRenameButton.TabIndex = 2;
            this.mSequenceRenameButton.Text = "Rename";
            this.mSequenceRenameButton.UseVisualStyleBackColor = true;
            this.mSequenceRenameButton.Click += new System.EventHandler(this.mSequenceRenameButton_Click);
            // 
            // mSequenceAddButton
            // 
            this.mSequenceAddButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mSequenceAddButton.Location = new System.Drawing.Point(556, 20);
            this.mSequenceAddButton.Name = "mSequenceAddButton";
            this.mSequenceAddButton.Size = new System.Drawing.Size(75, 23);
            this.mSequenceAddButton.TabIndex = 1;
            this.mSequenceAddButton.Text = "Add";
            this.mSequenceAddButton.UseVisualStyleBackColor = true;
            this.mSequenceAddButton.Click += new System.EventHandler(this.mSequenceAddButton_Click);
            // 
            // mSequenceListBox
            // 
            this.mSequenceListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mSequenceListBox.FormattingEnabled = true;
            this.mSequenceListBox.Location = new System.Drawing.Point(7, 20);
            this.mSequenceListBox.Name = "mSequenceListBox";
            this.mSequenceListBox.Size = new System.Drawing.Size(542, 316);
            this.mSequenceListBox.TabIndex = 0;
            this.mSequenceListBox.SelectedIndexChanged += new System.EventHandler(this.mSequenceListBox_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.mGuidelineColorButton);
            this.groupBox5.Controls.Add(this.mGuidelineX2Box);
            this.groupBox5.Controls.Add(this.mGuidelineX1Box);
            this.groupBox5.Controls.Add(this.mGuidelineY2Box);
            this.groupBox5.Controls.Add(this.mGuidelineY1Box);
            this.groupBox5.Controls.Add(this.mGuidelineRemoveButton);
            this.groupBox5.Controls.Add(this.mGuidelineAddButton);
            this.groupBox5.Controls.Add(this.mGuidelineListBox);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(646, 377);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Guidelines";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(491, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Y1:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(491, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "X1:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(491, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "X2:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(491, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Y2:";
            // 
            // mGuidelineColorButton
            // 
            this.mGuidelineColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mGuidelineColorButton.Enabled = false;
            this.mGuidelineColorButton.Location = new System.Drawing.Point(520, 123);
            this.mGuidelineColorButton.Name = "mGuidelineColorButton";
            this.mGuidelineColorButton.Size = new System.Drawing.Size(120, 23);
            this.mGuidelineColorButton.TabIndex = 7;
            this.mGuidelineColorButton.Text = "Color";
            this.mGuidelineColorButton.UseVisualStyleBackColor = true;
            this.mGuidelineColorButton.Click += new System.EventHandler(this.mGuidelineColorButton_Click);
            // 
            // mGuidelineX2Box
            // 
            this.mGuidelineX2Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mGuidelineX2Box.DecimalPlaces = 2;
            this.mGuidelineX2Box.Enabled = false;
            this.mGuidelineX2Box.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mGuidelineX2Box.Location = new System.Drawing.Point(520, 71);
            this.mGuidelineX2Box.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mGuidelineX2Box.Name = "mGuidelineX2Box";
            this.mGuidelineX2Box.Size = new System.Drawing.Size(120, 20);
            this.mGuidelineX2Box.TabIndex = 6;
            this.mGuidelineX2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mGuidelineX2Box.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mGuidelineX2Box.ValueChanged += new System.EventHandler(this.mGuidelineNumBox_ValueChanged);
            // 
            // mGuidelineX1Box
            // 
            this.mGuidelineX1Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mGuidelineX1Box.DecimalPlaces = 2;
            this.mGuidelineX1Box.Enabled = false;
            this.mGuidelineX1Box.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mGuidelineX1Box.Location = new System.Drawing.Point(520, 19);
            this.mGuidelineX1Box.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mGuidelineX1Box.Name = "mGuidelineX1Box";
            this.mGuidelineX1Box.Size = new System.Drawing.Size(120, 20);
            this.mGuidelineX1Box.TabIndex = 5;
            this.mGuidelineX1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mGuidelineX1Box.ValueChanged += new System.EventHandler(this.mGuidelineNumBox_ValueChanged);
            // 
            // mGuidelineY2Box
            // 
            this.mGuidelineY2Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mGuidelineY2Box.DecimalPlaces = 2;
            this.mGuidelineY2Box.Enabled = false;
            this.mGuidelineY2Box.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mGuidelineY2Box.Location = new System.Drawing.Point(520, 97);
            this.mGuidelineY2Box.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mGuidelineY2Box.Name = "mGuidelineY2Box";
            this.mGuidelineY2Box.Size = new System.Drawing.Size(120, 20);
            this.mGuidelineY2Box.TabIndex = 4;
            this.mGuidelineY2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mGuidelineY2Box.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mGuidelineY2Box.ValueChanged += new System.EventHandler(this.mGuidelineNumBox_ValueChanged);
            // 
            // mGuidelineY1Box
            // 
            this.mGuidelineY1Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mGuidelineY1Box.DecimalPlaces = 2;
            this.mGuidelineY1Box.Enabled = false;
            this.mGuidelineY1Box.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mGuidelineY1Box.Location = new System.Drawing.Point(520, 45);
            this.mGuidelineY1Box.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mGuidelineY1Box.Name = "mGuidelineY1Box";
            this.mGuidelineY1Box.Size = new System.Drawing.Size(120, 20);
            this.mGuidelineY1Box.TabIndex = 3;
            this.mGuidelineY1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mGuidelineY1Box.ValueChanged += new System.EventHandler(this.mGuidelineNumBox_ValueChanged);
            // 
            // mGuidelineRemoveButton
            // 
            this.mGuidelineRemoveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mGuidelineRemoveButton.Enabled = false;
            this.mGuidelineRemoveButton.Location = new System.Drawing.Point(79, 344);
            this.mGuidelineRemoveButton.Name = "mGuidelineRemoveButton";
            this.mGuidelineRemoveButton.Size = new System.Drawing.Size(67, 23);
            this.mGuidelineRemoveButton.TabIndex = 2;
            this.mGuidelineRemoveButton.Text = "Remove";
            this.mGuidelineRemoveButton.UseVisualStyleBackColor = true;
            this.mGuidelineRemoveButton.Click += new System.EventHandler(this.mGuidelineRemoveButton_Click);
            // 
            // mGuidelineAddButton
            // 
            this.mGuidelineAddButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mGuidelineAddButton.Location = new System.Drawing.Point(7, 344);
            this.mGuidelineAddButton.Name = "mGuidelineAddButton";
            this.mGuidelineAddButton.Size = new System.Drawing.Size(66, 23);
            this.mGuidelineAddButton.TabIndex = 1;
            this.mGuidelineAddButton.Text = "Add";
            this.mGuidelineAddButton.UseVisualStyleBackColor = true;
            this.mGuidelineAddButton.Click += new System.EventHandler(this.mGuidelineAddButton_Click);
            // 
            // mGuidelineListBox
            // 
            this.mGuidelineListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mGuidelineListBox.FormattingEnabled = true;
            this.mGuidelineListBox.Location = new System.Drawing.Point(7, 19);
            this.mGuidelineListBox.Name = "mGuidelineListBox";
            this.mGuidelineListBox.Size = new System.Drawing.Size(477, 303);
            this.mGuidelineListBox.TabIndex = 0;
            this.mGuidelineListBox.SelectedIndexChanged += new System.EventHandler(this.mGuidelineListBox_SelectedIndexChanged);
            // 
            // mTabControl
            // 
            this.mTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mTabControl.Controls.Add(this.tabPage1);
            this.mTabControl.Controls.Add(this.tabPage2);
            this.mTabControl.Controls.Add(this.tabPage3);
            this.mTabControl.Controls.Add(this.tabPage4);
            this.mTabControl.Controls.Add(this.tabPage5);
            this.mTabControl.Controls.Add(this.tabPage6);
            this.mTabControl.Controls.Add(this.tabPage7);
            this.mTabControl.Controls.Add(this.tabPage8);
            this.mTabControl.Location = new System.Drawing.Point(12, 12);
            this.mTabControl.Name = "mTabControl";
            this.mTabControl.SelectedIndex = 0;
            this.mTabControl.Size = new System.Drawing.Size(660, 409);
            this.mTabControl.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(652, 383);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Name";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(652, 383);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sequences";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(652, 383);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Guidelines";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(652, 383);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Output";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox14);
            this.tabPage5.Controls.Add(this.groupBox6);
            this.tabPage5.Controls.Add(this.groupBox1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(652, 383);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Compass";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox14.Controls.Add(this.mCompassImagePathBox);
            this.groupBox14.Location = new System.Drawing.Point(4, 115);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(645, 48);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Compass Image Path";
            // 
            // mCompassImagePathBox
            // 
            this.mCompassImagePathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mCompassImagePathBox.BrowseMode = ManagedCowTools.WinForms.PathWidget.BrowseModeType.Directory;
            this.mCompassImagePathBox.FileFilter = "All files (*.*)|*.*";
            this.mCompassImagePathBox.Location = new System.Drawing.Point(6, 16);
            this.mCompassImagePathBox.Name = "mCompassImagePathBox";
            this.mCompassImagePathBox.PathMustExist = true;
            this.mCompassImagePathBox.SelectedPath = "";
            this.mCompassImagePathBox.Size = new System.Drawing.Size(627, 26);
            this.mCompassImagePathBox.TabIndex = 6;
            this.mCompassImagePathBox.PathChanged += new ManagedCowTools.WinForms.PathWidget.PathChangedEvent(this.mCompassImagePathBox_PathChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.mCompassScaleBox);
            this.groupBox6.Location = new System.Drawing.Point(4, 58);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(137, 51);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Scale";
            // 
            // mCompassScaleBox
            // 
            this.mCompassScaleBox.DecimalPlaces = 3;
            this.mCompassScaleBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mCompassScaleBox.Location = new System.Drawing.Point(7, 20);
            this.mCompassScaleBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.mCompassScaleBox.Name = "mCompassScaleBox";
            this.mCompassScaleBox.Size = new System.Drawing.Size(123, 20);
            this.mCompassScaleBox.TabIndex = 0;
            this.mCompassScaleBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mCompassScaleBox.ValueChanged += new System.EventHandler(this.mCompassScaleBox_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mCompassYBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.mCompassXBox);
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 47);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Position";
            // 
            // mCompassYBox
            // 
            this.mCompassYBox.Location = new System.Drawing.Point(143, 18);
            this.mCompassYBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mCompassYBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.mCompassYBox.Name = "mCompassYBox";
            this.mCompassYBox.Size = new System.Drawing.Size(85, 20);
            this.mCompassYBox.TabIndex = 4;
            this.mCompassYBox.ValueChanged += new System.EventHandler(this.mCompassYBox_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "X:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(120, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Y:";
            // 
            // mCompassXBox
            // 
            this.mCompassXBox.Location = new System.Drawing.Point(30, 18);
            this.mCompassXBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mCompassXBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.mCompassXBox.Name = "mCompassXBox";
            this.mCompassXBox.Size = new System.Drawing.Size(81, 20);
            this.mCompassXBox.TabIndex = 3;
            this.mCompassXBox.ValueChanged += new System.EventHandler(this.mCompassXBox_ValueChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox15);
            this.tabPage6.Controls.Add(this.groupBox13);
            this.tabPage6.Controls.Add(this.groupBox12);
            this.tabPage6.Controls.Add(this.groupBox10);
            this.tabPage6.Controls.Add(this.groupBox9);
            this.tabPage6.Controls.Add(this.groupBox7);
            this.tabPage6.Controls.Add(this.groupBox8);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(652, 383);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Maps";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.mMapRulesBox);
            this.groupBox15.Location = new System.Drawing.Point(6, 268);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(633, 53);
            this.groupBox15.TabIndex = 8;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Map Rendering Rules";
            // 
            // mMapRulesBox
            // 
            this.mMapRulesBox.BrowseMode = ManagedCowTools.WinForms.PathWidget.BrowseModeType.File;
            this.mMapRulesBox.FileFilter = "All files (*.*)|*.*";
            this.mMapRulesBox.Location = new System.Drawing.Point(10, 19);
            this.mMapRulesBox.Name = "mMapRulesBox";
            this.mMapRulesBox.PathMustExist = false;
            this.mMapRulesBox.SelectedPath = "";
            this.mMapRulesBox.Size = new System.Drawing.Size(617, 26);
            this.mMapRulesBox.TabIndex = 0;
            this.mMapRulesBox.PathChanged += new ManagedCowTools.WinForms.PathWidget.PathChangedEvent(this.mMapRulesBox_PathChanged);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.mMapImageHeightBox);
            this.groupBox13.Controls.Add(this.label15);
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.mMapImageWidthBox);
            this.groupBox13.Location = new System.Drawing.Point(6, 50);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(240, 47);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Dimensions";
            // 
            // mMapImageHeightBox
            // 
            this.mMapImageHeightBox.Location = new System.Drawing.Point(143, 18);
            this.mMapImageHeightBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mMapImageHeightBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.mMapImageHeightBox.Name = "mMapImageHeightBox";
            this.mMapImageHeightBox.Size = new System.Drawing.Size(85, 20);
            this.mMapImageHeightBox.TabIndex = 4;
            this.mMapImageHeightBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mMapImageHeightBox.ValueChanged += new System.EventHandler(this.mMapImageHeightBox_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "X:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(120, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Y:";
            // 
            // mMapImageWidthBox
            // 
            this.mMapImageWidthBox.Location = new System.Drawing.Point(30, 18);
            this.mMapImageWidthBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mMapImageWidthBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.mMapImageWidthBox.Name = "mMapImageWidthBox";
            this.mMapImageWidthBox.Size = new System.Drawing.Size(81, 20);
            this.mMapImageWidthBox.TabIndex = 3;
            this.mMapImageWidthBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mMapImageWidthBox.ValueChanged += new System.EventHandler(this.mMapImageWidthBox_ValueChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.mMapAlphaBox);
            this.groupBox12.Location = new System.Drawing.Point(149, 100);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(137, 51);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Alpha";
            // 
            // mMapAlphaBox
            // 
            this.mMapAlphaBox.DecimalPlaces = 2;
            this.mMapAlphaBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mMapAlphaBox.Location = new System.Drawing.Point(6, 19);
            this.mMapAlphaBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mMapAlphaBox.Name = "mMapAlphaBox";
            this.mMapAlphaBox.Size = new System.Drawing.Size(120, 20);
            this.mMapAlphaBox.TabIndex = 0;
            this.mMapAlphaBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mMapAlphaBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mMapAlphaBox.ValueChanged += new System.EventHandler(this.mMapAlphaBox_ValueChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.mOsmPathBox);
            this.groupBox10.Location = new System.Drawing.Point(6, 211);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(639, 51);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "OSM Maps Location";
            // 
            // mOsmPathBox
            // 
            this.mOsmPathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mOsmPathBox.BrowseMode = ManagedCowTools.WinForms.PathWidget.BrowseModeType.Directory;
            this.mOsmPathBox.FileFilter = "All files (*.*)|*.*";
            this.mOsmPathBox.Location = new System.Drawing.Point(6, 19);
            this.mOsmPathBox.Name = "mOsmPathBox";
            this.mOsmPathBox.PathMustExist = true;
            this.mOsmPathBox.SelectedPath = "";
            this.mOsmPathBox.Size = new System.Drawing.Size(627, 26);
            this.mOsmPathBox.TabIndex = 5;
            this.mOsmPathBox.PathChanged += new ManagedCowTools.WinForms.PathWidget.PathChangedEvent(this.mOsmPathBox_PathChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.mMapPathBox);
            this.groupBox9.Location = new System.Drawing.Point(6, 157);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(640, 48);
            this.groupBox9.TabIndex = 4;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Map Image Search Path";
            // 
            // mMapPathBox
            // 
            this.mMapPathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mMapPathBox.Location = new System.Drawing.Point(7, 20);
            this.mMapPathBox.Name = "mMapPathBox";
            this.mMapPathBox.Size = new System.Drawing.Size(627, 20);
            this.mMapPathBox.TabIndex = 0;
            this.mMapPathBox.TextChanged += new System.EventHandler(this.mMapPathBox_TextChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.mMapScaleBox);
            this.groupBox7.Location = new System.Drawing.Point(6, 100);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(137, 51);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Scale";
            // 
            // mMapScaleBox
            // 
            this.mMapScaleBox.DecimalPlaces = 3;
            this.mMapScaleBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mMapScaleBox.Location = new System.Drawing.Point(7, 20);
            this.mMapScaleBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.mMapScaleBox.Name = "mMapScaleBox";
            this.mMapScaleBox.Size = new System.Drawing.Size(123, 20);
            this.mMapScaleBox.TabIndex = 0;
            this.mMapScaleBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mMapScaleBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mMapScaleBox.ValueChanged += new System.EventHandler(this.mMapScaleBox_ValueChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.mMapPositionYBox);
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Controls.Add(this.mMapPositionXBox);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(240, 47);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Position";
            // 
            // mMapPositionYBox
            // 
            this.mMapPositionYBox.Location = new System.Drawing.Point(143, 18);
            this.mMapPositionYBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mMapPositionYBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.mMapPositionYBox.Name = "mMapPositionYBox";
            this.mMapPositionYBox.Size = new System.Drawing.Size(85, 20);
            this.mMapPositionYBox.TabIndex = 4;
            this.mMapPositionYBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mMapPositionYBox.ValueChanged += new System.EventHandler(this.mMapPositionYBox_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "X:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(120, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Y:";
            // 
            // mMapPositionXBox
            // 
            this.mMapPositionXBox.Location = new System.Drawing.Point(30, 18);
            this.mMapPositionXBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mMapPositionXBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.mMapPositionXBox.Name = "mMapPositionXBox";
            this.mMapPositionXBox.Size = new System.Drawing.Size(81, 20);
            this.mMapPositionXBox.TabIndex = 3;
            this.mMapPositionXBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mMapPositionXBox.ValueChanged += new System.EventHandler(this.mMapPositionXBox_ValueChanged);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.mOutlineAlphaBox);
            this.tabPage7.Controls.Add(this.label18);
            this.tabPage7.Controls.Add(this.label17);
            this.tabPage7.Controls.Add(this.mOutlineThicknessButton);
            this.tabPage7.Controls.Add(this.mOutlineColorButton);
            this.tabPage7.Controls.Add(this.mHudTextColorButton);
            this.tabPage7.Controls.Add(this.mHudTextFontButton);
            this.tabPage7.Controls.Add(this.groupBox11);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(652, 383);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "HUD Text";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // mOutlineAlphaBox
            // 
            this.mOutlineAlphaBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mOutlineAlphaBox.Location = new System.Drawing.Point(147, 145);
            this.mOutlineAlphaBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mOutlineAlphaBox.Name = "mOutlineAlphaBox";
            this.mOutlineAlphaBox.Size = new System.Drawing.Size(86, 20);
            this.mOutlineAlphaBox.TabIndex = 7;
            this.mOutlineAlphaBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mOutlineAlphaBox.ValueChanged += new System.EventHandler(this.mOutlineAlphaBox_ValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(68, 147);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Outline Alpha:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(52, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "Outline Tickness:";
            // 
            // mOutlineThicknessButton
            // 
            this.mOutlineThicknessButton.DecimalPlaces = 1;
            this.mOutlineThicknessButton.Location = new System.Drawing.Point(147, 119);
            this.mOutlineThicknessButton.Name = "mOutlineThicknessButton";
            this.mOutlineThicknessButton.Size = new System.Drawing.Size(86, 20);
            this.mOutlineThicknessButton.TabIndex = 4;
            this.mOutlineThicknessButton.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mOutlineThicknessButton.ValueChanged += new System.EventHandler(this.mOutlineThicknessButton_ValueChanged);
            // 
            // mOutlineColorButton
            // 
            this.mOutlineColorButton.Location = new System.Drawing.Point(147, 89);
            this.mOutlineColorButton.Name = "mOutlineColorButton";
            this.mOutlineColorButton.Size = new System.Drawing.Size(86, 23);
            this.mOutlineColorButton.TabIndex = 3;
            this.mOutlineColorButton.Text = "Outline Color...";
            this.mOutlineColorButton.UseVisualStyleBackColor = true;
            this.mOutlineColorButton.Click += new System.EventHandler(this.mOutlineColorButton_Click);
            // 
            // mHudTextColorButton
            // 
            this.mHudTextColorButton.Location = new System.Drawing.Point(147, 60);
            this.mHudTextColorButton.Name = "mHudTextColorButton";
            this.mHudTextColorButton.Size = new System.Drawing.Size(86, 23);
            this.mHudTextColorButton.TabIndex = 2;
            this.mHudTextColorButton.Text = "Color...";
            this.mHudTextColorButton.UseVisualStyleBackColor = true;
            this.mHudTextColorButton.Click += new System.EventHandler(this.mHudTextColorButton_Click);
            // 
            // mHudTextFontButton
            // 
            this.mHudTextFontButton.Location = new System.Drawing.Point(37, 60);
            this.mHudTextFontButton.Name = "mHudTextFontButton";
            this.mHudTextFontButton.Size = new System.Drawing.Size(81, 23);
            this.mHudTextFontButton.TabIndex = 1;
            this.mHudTextFontButton.Text = "Font...";
            this.mHudTextFontButton.UseVisualStyleBackColor = true;
            this.mHudTextFontButton.Click += new System.EventHandler(this.mHudTextFontButton_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.mHudTextYBox);
            this.groupBox11.Controls.Add(this.mHudTextXBox);
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Location = new System.Drawing.Point(7, 7);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(241, 47);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Position";
            // 
            // mHudTextYBox
            // 
            this.mHudTextYBox.Location = new System.Drawing.Point(140, 18);
            this.mHudTextYBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mHudTextYBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.mHudTextYBox.Name = "mHudTextYBox";
            this.mHudTextYBox.Size = new System.Drawing.Size(86, 20);
            this.mHudTextYBox.TabIndex = 3;
            this.mHudTextYBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mHudTextYBox.ValueChanged += new System.EventHandler(this.mHudTextYBox_ValueChanged);
            // 
            // mHudTextXBox
            // 
            this.mHudTextXBox.Location = new System.Drawing.Point(30, 18);
            this.mHudTextXBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mHudTextXBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.mHudTextXBox.Name = "mHudTextXBox";
            this.mHudTextXBox.Size = new System.Drawing.Size(81, 20);
            this.mHudTextXBox.TabIndex = 2;
            this.mHudTextXBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mHudTextXBox.ValueChanged += new System.EventHandler(this.mHudTextXBox_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(117, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Y:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "X:";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.label19);
            this.tabPage8.Controls.Add(this.mAutoregisterMaxDeltaBox);
            this.tabPage8.Controls.Add(this.label14);
            this.tabPage8.Controls.Add(this.label13);
            this.tabPage8.Controls.Add(this.label12);
            this.tabPage8.Controls.Add(this.mAutoRegisterSearchRadiusBox);
            this.tabPage8.Controls.Add(this.mAutoRegisterCenterYBox);
            this.tabPage8.Controls.Add(this.mAutoRegisterCenterXBox);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(652, 383);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Auto-Registration";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(44, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Search Radius:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(194, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Y:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Registration center X:";
            // 
            // mAutoRegisterSearchRadiusBox
            // 
            this.mAutoRegisterSearchRadiusBox.Location = new System.Drawing.Point(121, 45);
            this.mAutoRegisterSearchRadiusBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.mAutoRegisterSearchRadiusBox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mAutoRegisterSearchRadiusBox.Name = "mAutoRegisterSearchRadiusBox";
            this.mAutoRegisterSearchRadiusBox.Size = new System.Drawing.Size(67, 20);
            this.mAutoRegisterSearchRadiusBox.TabIndex = 2;
            this.mAutoRegisterSearchRadiusBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mAutoRegisterSearchRadiusBox.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.mAutoRegisterSearchRadiusBox.ValueChanged += new System.EventHandler(this.mAutoRegisterSearchRadiusBox_ValueChanged);
            // 
            // mAutoRegisterCenterYBox
            // 
            this.mAutoRegisterCenterYBox.DecimalPlaces = 2;
            this.mAutoRegisterCenterYBox.Location = new System.Drawing.Point(217, 7);
            this.mAutoRegisterCenterYBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mAutoRegisterCenterYBox.Name = "mAutoRegisterCenterYBox";
            this.mAutoRegisterCenterYBox.Size = new System.Drawing.Size(63, 20);
            this.mAutoRegisterCenterYBox.TabIndex = 1;
            this.mAutoRegisterCenterYBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mAutoRegisterCenterYBox.Value = new decimal(new int[] {
            540,
            0,
            0,
            0});
            this.mAutoRegisterCenterYBox.ValueChanged += new System.EventHandler(this.mAutoRegisterCenterYBox_ValueChanged);
            // 
            // mAutoRegisterCenterXBox
            // 
            this.mAutoRegisterCenterXBox.DecimalPlaces = 2;
            this.mAutoRegisterCenterXBox.Location = new System.Drawing.Point(121, 7);
            this.mAutoRegisterCenterXBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mAutoRegisterCenterXBox.Name = "mAutoRegisterCenterXBox";
            this.mAutoRegisterCenterXBox.Size = new System.Drawing.Size(67, 20);
            this.mAutoRegisterCenterXBox.TabIndex = 0;
            this.mAutoRegisterCenterXBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mAutoRegisterCenterXBox.Value = new decimal(new int[] {
            960,
            0,
            0,
            0});
            this.mAutoRegisterCenterXBox.ValueChanged += new System.EventHandler(this.mAutoRegisterCenterXBox_ValueChanged);
            // 
            // mAutoregisterMaxDeltaBox
            // 
            this.mAutoregisterMaxDeltaBox.Location = new System.Drawing.Point(121, 77);
            this.mAutoregisterMaxDeltaBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.mAutoregisterMaxDeltaBox.Name = "mAutoregisterMaxDeltaBox";
            this.mAutoregisterMaxDeltaBox.Size = new System.Drawing.Size(67, 20);
            this.mAutoregisterMaxDeltaBox.TabIndex = 6;
            this.mAutoregisterMaxDeltaBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mAutoregisterMaxDeltaBox.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.mAutoregisterMaxDeltaBox.ValueChanged += new System.EventHandler(this.mAutoregisterMaxDeltaBox_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(57, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "Max Delta:";
            // 
            // ProjectSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 462);
            this.Controls.Add(this.mTabControl);
            this.Controls.Add(this.mOkButton);
            this.Controls.Add(this.mCancelButton);
            this.Name = "ProjectSettingsForm";
            this.Text = "Project Settings";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineX2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineX1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineY2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGuidelineY1Box)).EndInit();
            this.mTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mCompassScaleBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCompassYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCompassXBox)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mMapImageHeightBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mMapImageWidthBox)).EndInit();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mMapAlphaBox)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mMapScaleBox)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mMapPositionYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mMapPositionXBox)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOutlineAlphaBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mOutlineThicknessButton)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mHudTextYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mHudTextXBox)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoRegisterSearchRadiusBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoRegisterCenterYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoRegisterCenterXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mAutoregisterMaxDeltaBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button mCancelButton;
        private System.Windows.Forms.Button mOkButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox mProjectNameBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox mImageHeightBox;
        private System.Windows.Forms.TextBox mImageWidthBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button mSequenceRemoveButton;
        private System.Windows.Forms.Button mSequenceMoveDownButton;
        private System.Windows.Forms.Button mSequenceMoveUpButton;
        private System.Windows.Forms.Button mSequenceRenameButton;
        private System.Windows.Forms.Button mSequenceAddButton;
        private System.Windows.Forms.ListBox mSequenceListBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button mGuidelineColorButton;
        private System.Windows.Forms.NumericUpDown mGuidelineX2Box;
        private System.Windows.Forms.NumericUpDown mGuidelineX1Box;
        private System.Windows.Forms.NumericUpDown mGuidelineY2Box;
        private System.Windows.Forms.NumericUpDown mGuidelineY1Box;
        private System.Windows.Forms.Button mGuidelineRemoveButton;
        private System.Windows.Forms.Button mGuidelineAddButton;
        private System.Windows.Forms.ListBox mGuidelineListBox;
        private System.Windows.Forms.TabControl mTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.NumericUpDown mCompassYBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.NumericUpDown mCompassScaleBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown mCompassXBox;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown mMapScaleBox;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.NumericUpDown mMapPositionYBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown mMapPositionXBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox mMapPathBox;
        private System.Windows.Forms.GroupBox groupBox10;
        private ManagedCowTools.WinForms.PathWidget mOsmPathBox;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.NumericUpDown mHudTextYBox;
        private System.Windows.Forms.NumericUpDown mHudTextXBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button mHudTextColorButton;
        private System.Windows.Forms.Button mHudTextFontButton;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.NumericUpDown mMapAlphaBox;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown mAutoRegisterSearchRadiusBox;
        private System.Windows.Forms.NumericUpDown mAutoRegisterCenterYBox;
        private System.Windows.Forms.NumericUpDown mAutoRegisterCenterXBox;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.NumericUpDown mMapImageHeightBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown mMapImageWidthBox;
        private System.Windows.Forms.GroupBox groupBox14;
        private ManagedCowTools.WinForms.PathWidget mCompassImagePathBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private ManagedCowTools.WinForms.PathWidget mMapRulesBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown mOutlineThicknessButton;
        private System.Windows.Forms.Button mOutlineColorButton;
        private System.Windows.Forms.NumericUpDown mOutlineAlphaBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown mAutoregisterMaxDeltaBox;
    }
}