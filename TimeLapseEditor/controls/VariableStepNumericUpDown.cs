﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeLapseEditor.Controls
{
    public class VariableStepNumericUpDown : NumericUpDown
    {
        public override void UpButton()
        {
            decimal inc = Increment;
            Increment = ScaleIncrement(inc);
            base.UpButton();
            Increment = inc;
        }


        public override void DownButton()
        {
            decimal inc = Increment;
            Increment = ScaleIncrement(inc);
            base.DownButton();
            Increment = inc;
        }

        private decimal ScaleIncrement(decimal aValue)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
            {
                return 10 * aValue;
            }

            if ((Control.ModifierKeys & Keys.Alt) == Keys.Alt)
            {
                return 100 * aValue;
            }

            return aValue;
        }
    }
}
