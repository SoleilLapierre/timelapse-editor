﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TimeLapseEditor
{
    internal partial class ProjectSettingsForm : Form
    {
        private Project mProject;
        private bool mEnableEvents = true;

        public ProjectSettingsForm(Project aProject)
        {
            InitializeComponent();
            
            mProject = aProject;

            SuspendLayout();
            mEnableEvents = false;

            mProjectNameBox.Text = mProject.Name;

            mImageWidthBox.SuspendLayout();
            mImageWidthBox.Text = mProject.OutputWidth.ToString();
            mImageWidthBox.ResumeLayout();

            mImageHeightBox.SuspendLayout();
            mImageHeightBox.Text = mProject.OutputHeight.ToString();
            mImageHeightBox.ResumeLayout();

            mSequenceListBox.Items.Clear();
            foreach (Project.Sequence s in mProject.Sequences)
            {
                mSequenceListBox.Items.Add(s);
            }

            mGuidelineListBox.Items.Clear();
            foreach (Project.Guideline g in mProject.Guidelines)
            {
                mGuidelineListBox.Items.Add(g);
            }

            mCompassXBox.Value = (decimal)mProject.CompassImagePosition.X;
            mCompassYBox.Value = (decimal)mProject.CompassImagePosition.Y;
            mCompassScaleBox.Value = (decimal)mProject.CompassImageScale;
            mCompassImagePathBox.SelectedPath = mProject.CompassImagePath;

            mMapPositionXBox.Value = (decimal)mProject.MapImagePosition.X;
            mMapPositionYBox.Value = (decimal)mProject.MapImagePosition.Y;
            mMapImageWidthBox.Value = (decimal)mProject.MapImageDimensions.X;
            mMapImageHeightBox.Value = (decimal)mProject.MapImageDimensions.Y;
            mMapScaleBox.Value = (decimal)mProject.MapImageScale;

            mMapPathBox.Text = mProject.MapImagePath;
            mOsmPathBox.SelectedPath = mProject.OsmMapsPath;

            mHudTextXBox.Value = (decimal)mProject.HudTextPosition.X;
            mHudTextYBox.Value = (decimal)mProject.HudTextPosition.Y;
            mOutlineThicknessButton.Value = (decimal)mProject.TextOutlineThickness;
            mOutlineAlphaBox.Value = (decimal)mProject.TextOutlineAlpha;

            mMapAlphaBox.Value = (decimal)mProject.MapImageAlpha;
            mMapRulesBox.SelectedPath = mProject.MapRulesName;

            mAutoRegisterCenterXBox.Value = (decimal)mProject.AutoRegisterCenter.X;
            mAutoRegisterCenterYBox.Value = (decimal)mProject.AutoRegisterCenter.Y;
            mAutoRegisterSearchRadiusBox.Value = (decimal)mProject.AutoRegisterSearchRadius;
            mAutoregisterMaxDeltaBox.Value = (decimal)mProject.AutoregisterMaxDelta;

            mEnableEvents = true;
            ResumeLayout();
        }


        public static DialogResult Show(Project aProject)
        {
            ProjectSettingsForm form = new ProjectSettingsForm(aProject);
            form.ShowDialog();
            return form.DialogResult;
        }


        private void mOkButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents) 
            { 
                return; 
            }

            DialogResult = DialogResult.OK;
            Close();
        }


        private void mCancelButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            DialogResult = DialogResult.Cancel;
            Close();
        }


        private void mProjectNameBox_Leave(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            string name = mProjectNameBox.Text;
            int length = name.Length;
            if (length > 0)
            {
                HashSet<char> illegal = new HashSet<char>(System.IO.Path.GetInvalidFileNameChars());
                StringBuilder sb = new StringBuilder(name);
                for (int i = 0; i < length; ++i)
                {
                    if (illegal.Contains(sb[i]))
                    {
                        sb[i] = '_';
                    }
                }

                name = sb.ToString();
            }
            else
            {
                name = "New Project";
            }

            mProject.Name = name;
            mProjectNameBox.Text = name;
        }


        private void mImageWidthBox_Validating(object sender, CancelEventArgs e)
        {
            int width = 0;
            if (!int.TryParse(mImageWidthBox.Text, out width))
            {
                e.Cancel = true;
            }
        }


        private void mImageWidthBox_Validated(object sender, EventArgs e)
        {
            int width = 0;
            if (int.TryParse(mImageWidthBox.Text, out width))
            {
                mProject.OutputWidth = width;
            }
        }


        private void mImageHeightBox_Validating(object sender, CancelEventArgs e)
        {
            int height = 0;
            if (!int.TryParse(mImageHeightBox.Text, out height))
            {
                e.Cancel = true;
            }
        }


        private void mImageHeightBox_Validated(object sender, EventArgs e)
        {
            int height = 0;
            if (int.TryParse(mImageHeightBox.Text, out height))
            {
                mProject.OutputHeight = height;
            }
        }


        private void mSequenceListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            bool selected = ((mSequenceListBox.SelectedItem != null) && (mSequenceListBox.SelectedItem is Project.Sequence));
            mSequenceMoveUpButton.Enabled = selected && (mSequenceListBox.SelectedIndex > 0);
            mSequenceMoveDownButton.Enabled = selected && (mSequenceListBox.SelectedIndex < (mSequenceListBox.Items.Count - 1));
            mSequenceRemoveButton.Enabled = selected;
            mSequenceRenameButton.Enabled = selected;
        }


        private void mSequenceAddButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            List<Project.Sequence> list = mProject.Sequences;
            if (list == null)
            {
                list = new List<Project.Sequence>();
            }

            Project.Sequence s = new Project.Sequence();

            SequenceDialog dlg = new SequenceDialog("Enter Sequence Name", "New Sequence");
            if ((dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) || String.IsNullOrEmpty(dlg.Value))
            {
                return;
            }

            s.Name = dlg.Value;
            s.ID = (uint)s.Name.GetHashCode();
            s.ShowOverlays = dlg.ShowOverlays;
            while (mProject.GetSequenceName(s.ID) != null)
            {
                s.ID = (uint)(new Random()).Next();
            }

            list.Add(s);
            mProject.Sequences = list;
            mEnableEvents = false;
            mSequenceListBox.SelectedIndex = mSequenceListBox.Items.Add(s);
            mEnableEvents = true;
        }


        private void mSequenceRenameButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            Project.Sequence s = mSequenceListBox.SelectedItem as Project.Sequence;
            if (s != null)
            {
                SequenceDialog dlg = new SequenceDialog("Enter New Sequence Name", s.Name);
                dlg.ShowOverlays = s.ShowOverlays;
                if ((dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) && !String.IsNullOrEmpty(dlg.Value))
                {
                    s.Name = dlg.Value;
                    s.ShowOverlays = dlg.ShowOverlays;
                    mProject.Sequences = mProject.Sequences; // Force name table update.
                    int index = mSequenceListBox.SelectedIndex;
                    mEnableEvents = false;
                    mSequenceListBox.Items.RemoveAt(index);
                    mSequenceListBox.Items.Insert(index, s);
                    mEnableEvents = true;
                }
            }
        }


        private void mSequenceRemoveButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            Project.Sequence s = mSequenceListBox.SelectedItem as Project.Sequence;
            if (s != null)
            {
                mProject.Sequences.Remove(s);
                mEnableEvents = false;
                mSequenceListBox.Items.Remove(s);
                mEnableEvents = true;
                mProject.Sequences = mProject.Sequences; // Force name table update.
            }
        }


        private void mSequenceMoveUpButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            int index = mSequenceListBox.SelectedIndex;
            if (index < 1)
            {
                return;
            }

            Project.Sequence s = mSequenceListBox.SelectedItem as Project.Sequence;
            if (s != null)
            {
                List<Project.Sequence> list = mProject.Sequences;
                if (list == null)
                {
                    list = new List<Project.Sequence>();
                }

                mEnableEvents = false;
                mSequenceListBox.Items.Remove(s);
                list.Remove(s);
                list.Insert(index - 1, s);
                mSequenceListBox.Items.Insert(index - 1, s);
                mProject.Sequences = list;
                mEnableEvents = true;
            }
        }


        private void mSequenceMoveDownButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            int index = mSequenceListBox.SelectedIndex;
            if (index >= (mSequenceListBox.Items.Count - 1))
            {
                return;
            }

            Project.Sequence s = mSequenceListBox.SelectedItem as Project.Sequence;
            if (s != null)
            {
                List<Project.Sequence> list = mProject.Sequences;
                if (list == null)
                {
                    list = new List<Project.Sequence>();
                }

                mEnableEvents = false;
                mSequenceListBox.Items.Remove(s);
                list.Remove(s);
                list.Insert(index + 1, s);
                mSequenceListBox.Items.Insert(index + 1, s);
                mProject.Sequences = list;
                mEnableEvents = true;
            }
        }


        private void mGuidelineListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            SuspendLayout();
            mEnableEvents = false;

            Project.Guideline g = mGuidelineListBox.SelectedItem as Project.Guideline;
            if (g != null)
            {
                mGuidelineRemoveButton.Enabled = true;
                mGuidelineX1Box.Enabled = true;
                mGuidelineY1Box.Enabled = true;
                mGuidelineX2Box.Enabled = true;
                mGuidelineY2Box.Enabled = true;
                mGuidelineX1Box.Value = (decimal)g.X1;
                mGuidelineY1Box.Value = (decimal)g.Y1;
                mGuidelineX2Box.Value = (decimal)g.X2;
                mGuidelineY2Box.Value = (decimal)g.Y2;
                mGuidelineColorButton.Enabled = true;
                UpdateColorButton(g.Color);
            }
            else
            {
                mGuidelineRemoveButton.Enabled = false;
                mGuidelineX1Box.Enabled = false;
                mGuidelineY1Box.Enabled = false;
                mGuidelineX2Box.Enabled = false;
                mGuidelineY2Box.Enabled = false;
                mGuidelineColorButton.Enabled = false;
            }

            mEnableEvents = true;
            ResumeLayout();
        }


        private void mGuidelineAddButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            Project.Guideline g = new Project.Guideline();
            mProject.Guidelines.Add(g);
            mProject.IsSaved = false;
            mEnableEvents = false;
            mGuidelineListBox.SelectedIndex = mGuidelineListBox.Items.Add(g);
            mEnableEvents = true;
        }


        private void mGuidelineRemoveButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            Project.Guideline g = mGuidelineListBox.SelectedItem as Project.Guideline;
            if (g != null)
            {
                mProject.Guidelines.Remove(g);
                mProject.IsSaved = false;
                mEnableEvents = false;
                mGuidelineListBox.Items.Remove(g);
                mEnableEvents = true;
            }
        }


        private void mGuidelineNumBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            Project.Guideline g = mGuidelineListBox.SelectedItem as Project.Guideline;
            if (g != null)
            {
                g.X1 = (float)mGuidelineX1Box.Value;
                g.Y1 = (float)mGuidelineY1Box.Value;
                g.X2 = (float)mGuidelineX2Box.Value;
                g.Y2 = (float)mGuidelineY2Box.Value;
                mProject.IsSaved = false;
                mGuidelineListBox.Refresh();
            }
        }


        private void mGuidelineColorButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            Project.Guideline g = mGuidelineListBox.SelectedItem as Project.Guideline;
            if (g != null)
            {
                ColorDialog cd = new ColorDialog();
                cd.Color = g.Color;
                if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    g.Color = cd.Color;
                    mProject.IsSaved = false;
                    UpdateColorButton(g.Color);
                }
            }
        }


        private void UpdateColorButton(Color aColor)
        {
            Size bs = mGuidelineColorButton.ClientSize;
            Bitmap b = new Bitmap(bs.Width, bs.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Graphics gr = Graphics.FromImage(b);
            System.Drawing.Brush brush = new SolidBrush(aColor);
            gr.FillRectangle(brush, 0, 0, bs.Width, bs.Height);
            mGuidelineColorButton.Image = b;
            brush.Dispose();
        }


        private void mCompassXBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetCompassImagePositionX((float)mCompassXBox.Value);
            }
        }


        private void mCompassYBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetCompassImagePositionY((float)mCompassYBox.Value);
            }
        }


        private void mCompassScaleBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.CompassImageScale = (float)mCompassScaleBox.Value;
                mProject.IsSaved = false;
            }
        }


        private void mMapPositionXBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetMapImagePositionX((float)mMapPositionXBox.Value);
            }
        }


        private void mMapPositionYBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetMapImagePositionY((float)mMapPositionYBox.Value);
            }
        }


        private void mMapScaleBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.MapImageScale = (float)mMapScaleBox.Value;
                mProject.IsSaved = false;
            }
        }


        private void mMapPathBox_TextChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.MapImagePath = mMapPathBox.Text;
                mProject.IsSaved = false;
            }
        }


        private void mOsmPathBox_PathChanged(object aSender)
        {
            if (mEnableEvents)
            {
                mProject.OsmMapsPath = mOsmPathBox.SelectedPath;
                mProject.IsSaved = false;
            }
        }


        private void mCompassImagePathBox_PathChanged(object aSender)
        {
            if (mEnableEvents)
            {
                mProject.CompassImagePath = mCompassImagePathBox.SelectedPath;
                mProject.IsSaved = false;
            }
        }

        private void mHudTextXBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetHudTextPositionX((float)mHudTextXBox.Value);
            }
        }

        private void mHudTextYBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetHudTextPositionY((float)mHudTextYBox.Value);
            }
        }

        private void mHudTextFontButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            FontDialog fd = new FontDialog();
            fd.Font = mProject.HudTextFont;
            fd.FontMustExist = true;
            fd.ShowApply = false;
            fd.ShowColor = false;
            fd.ShowEffects = false;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                mProject.HudTextFont = fd.Font;
                mProject.IsSaved = false;
            }
        }

        private void mHudTextColorButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            ColorDialog dlg = new ColorDialog();
            dlg.AllowFullOpen = true;
            dlg.AnyColor = true;
            dlg.FullOpen = true;
            dlg.SolidColorOnly = false;
            dlg.Color = mProject.HudTextColor;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                mProject.HudTextColor= dlg.Color;
                mProject.IsSaved = false;
            }
        }

        private void mMapAlphaBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            mProject.MapImageAlpha = (float)mMapAlphaBox.Value;
            mProject.IsSaved = false;
        }


        private void mAutoRegisterCenterXBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            mProject.SetAutoRegisterCenterX((float)mAutoRegisterCenterXBox.Value);
        }


        private void mAutoRegisterCenterYBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            mProject.SetAutoRegisterCenterY((float)mAutoRegisterCenterYBox.Value);
        }


        private void mAutoRegisterSearchRadiusBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            mProject.AutoRegisterSearchRadius = (int)mAutoRegisterSearchRadiusBox.Value;
            mProject.IsSaved = false;
        }


        private void mMapImageWidthBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetMapImageWidth((float)mMapImageWidthBox.Value);
            }
        }


        private void mMapImageHeightBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.SetMapImageHeight((float)mMapImageHeightBox.Value);
            }
        }


        private void mMapRulesBox_PathChanged(object aSender)
        {
            if (mEnableEvents)
            {
                mProject.MapRulesName = mMapRulesBox.SelectedPath;
                mProject.IsSaved = false;
            }
        }


        private void mOutlineColorButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            ColorDialog dlg = new ColorDialog();
            dlg.AllowFullOpen = true;
            dlg.AnyColor = true;
            dlg.FullOpen = true;
            dlg.SolidColorOnly = false;
            dlg.Color = mProject.TextOutlineColor;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                mProject.TextOutlineColor = dlg.Color;
                mProject.IsSaved = false;
            }
        }


        private void mOutlineThicknessButton_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.TextOutlineThickness = (float)mOutlineThicknessButton.Value;
                mProject.IsSaved = false;
            }
        }

        private void mOutlineAlphaBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                mProject.TextOutlineAlpha = (float)mOutlineAlphaBox.Value;
                mProject.IsSaved = false;
            }
        }

        private void mAutoregisterMaxDeltaBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            mProject.AutoregisterMaxDelta = (int)mAutoregisterMaxDeltaBox.Value;
            mProject.IsSaved = false;
        }
    }
}
