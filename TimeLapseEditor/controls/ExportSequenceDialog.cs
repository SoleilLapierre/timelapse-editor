﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace TimeLapseEditor
{
    public partial class ExportSequenceDialog : Form
    {
        private class ThreadArgs
        {
            public int StartFrame { get; set; }
            public int EndFrame { get; set; }
            public HashSet<uint> Sequences { get; set; }
            public int FirstOutputIndex { get; set; }
            public int NumCopies { get; set; }
            public bool OverlayFrameNumber { get; set; }
        }

        private Project mProject;
        private bool mInProgress = false;
        private bool mEnableEvents = true;
        private BackgroundWorker mWorker;


        public ExportSequenceDialog(Project aProject)
        {
            mProject = aProject;
            InitializeComponent();

            mEnableEvents = false;
            mOutputPathBox.SelectedPath = Properties.Settings.Default.SequenceExportPath;
            mBaseFilenameBox.Text = Properties.Settings.Default.SequenceExportBaseName;
            int numFrames = mProject.GetNumFrames();
            mFrameRangeStartBox.Maximum = numFrames;
            mFrameRangeStartBox.Value = 0;
            mFrameRangeEndBox.Maximum = numFrames;
            mFrameRangeEndBox.Value = numFrames - 1;

            mSequenceListBox.Items.Clear();
            foreach (Project.Sequence s in mProject.Sequences)
            {
                mSequenceListBox.Items.Add(s);
            }

            mWorker = new BackgroundWorker();
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.DoWork += new DoWorkEventHandler(mWorker_DoWork);
            mWorker.ProgressChanged += new ProgressChangedEventHandler(mWorker_ProgressChanged);
            mWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(mWorker_RunWorkerCompleted);

            mEnableEvents = true;
        }


        private void mOutputPathBox_PathChanged(object aSender)
        {
            if (mEnableEvents)
            {
                Properties.Settings.Default.SequenceExportPath = mOutputPathBox.SelectedPath;
            }
        }


        private void mBaseFilenameBox_TextChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                Properties.Settings.Default.SequenceExportBaseName = mBaseFilenameBox.Text;
            }
        }


        private void mFrameRangeStartBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                if (mFrameRangeStartBox.Value > mFrameRangeEndBox.Value)
                {
                    mEnableEvents = false;
                    mFrameRangeEndBox.Value = mFrameRangeStartBox.Value;
                    mEnableEvents = true;
                }
            }
        }


        private void mFrameRangeEndBox_ValueChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                if (mFrameRangeStartBox.Value > mFrameRangeEndBox.Value)
                {
                    mEnableEvents = false;
                    mFrameRangeStartBox.Value = mFrameRangeEndBox.Value;
                    mEnableEvents = true;
                }
            }
        }


        private void mGoButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            if (mInProgress)
            {
                mWorker.CancelAsync();
            }
            else
            {
                mInProgress = true;
                mGoButton.Text = "STOP!";
                mGoButton.BackColor = Color.Salmon;
                int start = (int)mFrameRangeStartBox.Value;
                mProgressBar.Minimum = start;
                mProgressBar.Value = start;
                int end = (int)mFrameRangeEndBox.Value;
                mProgressBar.Maximum = end + 1;

                HashSet<uint> selectedSequences = new HashSet<uint>();
                foreach (Project.Sequence s in mSequenceListBox.SelectedItems)
                {
                    selectedSequences.Add(s.ID);
                }

                ThreadArgs ta = new ThreadArgs();
                ta.StartFrame = start;
                ta.EndFrame = end;
                ta.Sequences = selectedSequences;
                ta.FirstOutputIndex = (int)mFirstOutputIndexBox.Value;
                ta.NumCopies = (int)mNumCopiesBox.Value;
                ta.OverlayFrameNumber = mShowFrameButton.Checked;

                mWorker.RunWorkerAsync(ta);
            }
        }


        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }


        void mWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ThreadArgs ta = e.Argument as ThreadArgs;

            int start = ta.StartFrame;
            int end = ta.EndFrame;
            HashSet<uint> selectedSequences = ta.Sequences;

            ImageCodecInfo jgpEncoder = GetEncoder(ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;

            int range = Math.Max(ta.FirstOutputIndex, end - start);
            string formatStr = "{0}{1:000000}.jpg";

            int imageNo = ta.FirstOutputIndex;
            for (int i = start; (i <= end) && mInProgress; ++i)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                Frame f = mProject.GetFrame(i);

                bool render = false;
                foreach (uint seqId in f.Sequences)
                {
                    if (selectedSequences.Contains(seqId))
                    {
                        render = true;
                        break;
                    }
                }

                render &= (f.FrameMultiple > 0);

                string fileName = null;
                if (render)
                {
                    Image img = PreviewPanel.GenerateImage(mProject, i, f, mOverlayButton.Checked, true, ta.OverlayFrameNumber);
                    for (int j = 0; j < f.FrameMultiple * ta.NumCopies; ++j)
                    {
                        fileName = Path.Combine(mOutputPathBox.SelectedPath, String.Format(formatStr, mBaseFilenameBox.Text, imageNo));
                        img.Save(fileName, jgpEncoder, myEncoderParameters);
                        ++imageNo;
                    }
                }

                worker.ReportProgress(i, fileName);
                GC.Collect();
            }
        }


        void mWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            mProgressBar.Value = e.ProgressPercentage;
            if (e.UserState != null)
            {
                mProgressLabel.Text = String.Format("Wrote frame {0} to {1}", e.ProgressPercentage, (string)e.UserState);
            }
        }



        void mWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            mInProgress = false;
            mGoButton.Text = "Start";
            mGoButton.BackColor = Color.LightGreen;
            mProgressBar.Minimum = 0;
            mProgressBar.Value = 0;
            mProgressLabel.Text = "";
        }
    }
}
