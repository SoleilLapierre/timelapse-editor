﻿// Copyright (c) 2015 by Soleil Lapierre.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using ManagedCowTools.Imaging;
using ManagedCowTools.Math;

namespace TimeLapseEditor
{
    public partial class PreviewPanel : Form
    {
        #region ----- Data members -----

        private enum DragMode
        {
            None,
            DefineRegion,
            MoveRegion,
            ManualRegister
        };

        private ControlPanel mControlPanel;

        private ObjectMRUCache<string, Bitmap> mImageCache = new ObjectMRUCache<string, Bitmap>(10);
        private int mCurrentIndex = 0;
        private Frame mCurrentFrame = null;

        private Bitmap mUndecoratedImage = null;
        private Pen mSelectPen = new Pen(Color.Red, 2);

        private int mZoom = 7;
        private float[] mZoomTable = new float[] 
        {
            1.0f / 64.0f,
            1.0f / 32.0f,
            1.0f / 16.0f,
            1.0f / 8.0f,
            1.0f / 4.0f,
            0.5f,
            0.75f,
            1.0f,
            1.5f,
            2.0f,
            4.0f,
            8.0f,
            16.0f,
            32.0f,
            64.0f
        };

        // Mouse drag stuff
        private DragMode mDragMode = DragMode.None;
        private BlurRegion mSelectedBlurRegion = null;
        private int mDragStartX, mDragStartY;
        private int mDragOffsetX, mDragOffsetY;
        private int mLastMouseImageX = -1, mLastMouseImageY = -1;

        // Joystick input
        private JoystickMonitor mJoystick;
        private Timer mJoystickTimer;
        private bool mEnableJoystick = false;

        #endregion
        #region ----- Initialization -----

        public PreviewPanel(ControlPanel aControlPanel)
        {
            mControlPanel = aControlPanel;

            InitializeComponent();
            mRegionTypeBox.Items.Add(BlurRegion.ShapeType.Rectangle);
            mRegionTypeBox.Items.Add(BlurRegion.ShapeType.Ellipse);
            mRegionTypeBox.Items.Add(BlurRegion.ShapeType.Autoregister);
            mRegionTypeBox.SelectedIndex = 0;

            mZoom = Properties.Settings.Default.DefaultZoom;
            UpdateZoomControls();
            mGuidelinesCheckBox.Checked = Properties.Settings.Default.GuidelinesChecked;
            mColorCorrectionButton.Checked = Properties.Settings.Default.ColorCorrectionsChecked;
            mOverlayCheckbox.Checked = Properties.Settings.Default.ShowOverlaysChecked;

            mJoystick = new JoystickMonitor();
            mJoystickTimer = new Timer();
            mJoystickTimer.Tick += new EventHandler(mJoystickTimer_Tick);
            mJoystickTimer.Interval = 1000 / 15;
            mJoystickTimer.Start();

            MouseWheel += PreviewPanel_MouseWheel;
        }

        #endregion
        #region ----- Public properties and methods -----

        public bool RegistrationMode
        {
            get { return mRegistrationModeButton.Checked; }
            set { mRegistrationModeButton.Checked = value; }
        }


        public void DisplayImage(int aIndex, Frame f)
        {
            mEnableJoystick = false;

            mUndecoratedImage = null;
            mCurrentIndex = aIndex;
            mCurrentFrame = f;
            mSelectedBlurRegion = null;

            mUndecoratedImage = mImageCache.Get(aIndex.ToString());
            if (mUndecoratedImage == null)
            {
                mUndecoratedImage = GenerateImage(aIndex, f);
                GraphicsUnit unit = GraphicsUnit.Pixel;
                if (mUndecoratedImage != null)
                {
                    mImageCache.Insert(aIndex.ToString(), mUndecoratedImage.Clone(mUndecoratedImage.GetBounds(ref unit), mUndecoratedImage.PixelFormat));
                }
            }

            if (RegistrationMode && (aIndex > 0) && (mUndecoratedImage != null))
            {
                int prevIndex = aIndex - 1;
                while ((prevIndex > 0) && (Project.Instance.GetFrame(prevIndex).FrameMultiple < 1))
                {
                    --prevIndex;
                }

                Bitmap prev = mImageCache.Get(prevIndex.ToString());
                if (prev == null)
                {
                    prev = GenerateImage(prevIndex, Project.Instance.GetFrame(prevIndex));
                    mImageCache.Insert(prevIndex.ToString(), prev);
                }

                ImageAttributes ia = new ImageAttributes();
                ColorMatrix cm = new ColorMatrix();
                cm.Matrix33 = 0.4f;
                ia.SetColorMatrix(cm);

                Graphics g = Graphics.FromImage(mUndecoratedImage);
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(prev,
                            new Rectangle(0, 0, mUndecoratedImage.Width, mUndecoratedImage.Height),
                            0, 0, prev.Width, prev.Height,
                            GraphicsUnit.Pixel, ia);

            }

            UpdateDecorations();

            GC.Collect();

            mEnableJoystick = true;
        }


        // Transforms image to final size and orientation and draws stuff that will be output on it like compass and text and map.        
        public static Bitmap GenerateImage(Project aProject, int aIndex, Frame f, bool aShowOverlays, bool aApplyColorCorrections, bool aOverlayFrameNumber = false)
        {
            if (f != null)
            {
                Bitmap img = Bitmap.FromFile(f.SourcePath) as Bitmap;
                if (img != null)
                {
                    // Apply blur regions.
                    ApplyBlurRegions(img as Bitmap, f, aIndex);

                    ImageHelper ih = aProject.GetImageHelper(aIndex);
                    img = ih.Transform(img);

                    float gamma = aProject.GammaTimeline[(double)aIndex];
                    float hue = aProject.HueTimeline[(double)aIndex];
                    float sat = aProject.SaturationTimeline[(double)aIndex];
                    float val = aProject.BrightnessTimeline[(double)aIndex];
                    float contrast = aProject.ContrastTimeline[(double)aIndex];

                    if ((gamma != 1.0f) || (hue != 0.0f) || (sat != 0.0f) || (val != 0.0f) || (contrast != 1.0f))
                    {
                        if (aApplyColorCorrections)
                        {
                            FloatImageRGB floatImage = new FloatImageRGB(img);
                            float dpiX = img.HorizontalResolution;
                            float dpiY = img.VerticalResolution;

                            if ((hue != 0.0f) || (sat != 0.0f) || (val != 0.0f))
                            {
                                floatImage.HSVAdjust(hue, sat, val);
                            }

                            if (contrast != 1.0f)
                            {
                                floatImage.ScaleColors(0.5f, contrast);
                            }

                            if (gamma != 1.0f)
                            {
                                floatImage.Gamma(gamma);
                            }

                            img = floatImage.ToBitmap();
                            floatImage.Dispose();
                            img.SetResolution(dpiX, dpiY);
                        }
                    }

                    Graphics g = Graphics.FromImage(img);
                    float fontScale = g.DpiY / 96.0f;
                    bool fixFont = Math.Abs(fontScale - 1.0f) > 0.001f;

                    // Can't change the DPI properties of the Graphics once it's created, and apparently
                    // the value is wrong for some images.
                    Font hudFont = aProject.HudTextFont;
                    if (fixFont)
                    {
                        hudFont = new Font(hudFont.Name, hudFont.SizeInPoints * fontScale, hudFont.Style);
                    }
                    
                    if (f.ShowOverlays && aShowOverlays)
                    {
                        // Draw compass.
                        float angle = 0.0f;
                        if (f.Heading != null)
                        {
                            angle = (float)f.Heading + ((f.CompassMode == Frame.CompassUpdateMode.Initialize) ? 0.0f : aProject.HeadingOffsetTimeline[(double)aIndex]);
                            while (angle < 0.0f)
                            {
                                angle += 360.0f;
                            }
                            while (angle > 360.0f)
                            {
                                angle -= 360.0f;
                            }

                            DrawCompass(g, aProject.CompassImagePosition, aProject.CompassImageScale, (int)Math.Round((double)angle));
                        }

                        // Draw map.
                        if (!string.IsNullOrEmpty(f.MapImageFilename) && File.Exists(f.MapImageFilename))
                        {
                            float x = aProject.MapImagePosition.X;
                            float y = aProject.MapImagePosition.Y;
                            Image im = Image.FromFile(f.MapImageFilename);
                            float scale = aProject.MapImageScale;

                            ImageAttributes ia = new ImageAttributes();
                            ColorMatrix cm = new ColorMatrix();
                            cm.Matrix33 = aProject.MapImageAlpha;
                            ia.SetColorMatrix(cm);

                            System.Drawing.Drawing2D.CompositingMode origMode = g.CompositingMode;
                            System.Drawing.Drawing2D.CompositingQuality origQuality = g.CompositingQuality;
                            g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            g.DrawImage(im, 
                                        new Rectangle((int)Math.Round(x), (int)Math.Round(y), (int)Math.Round(scale * im.Width), (int)Math.Round(scale * im.Height)), 
                                        0, 0, im.Width, im.Height, 
                                        GraphicsUnit.Pixel, ia);
                            g.CompositingMode = origMode;
                            g.CompositingQuality = origQuality;
 
                            if (f.Heading != null)
                            {
                                DrawArrow(g, angle, x + 0.5f * scale * im.Width, y + 0.5f * scale * im.Height);
                            }
                        }

                        // Draw text overlays.
                        foreach (int ovlId in f.TextOverlayIDs)
                        {
                            TextOverlay ovl = Project.Instance.GetTextOverlay(ovlId);
                            if (ovl != null)
                            {
                                Font font = ovl.Font;
                                if (fixFont)
                                {
                                    font = new Font(font.Name, font.SizeInPoints * fontScale, font.Style);
                                }

                                DrawTextHelper(g, ovl.Text, font, ovl.Color,
                                    Project.Instance.TextOutlineColor,
                                    Project.Instance.TextOutlineThickness,
                                    font.SizeInPoints * fontScale,
                                    new PointF(ovl.Position.X, ovl.Position.Y));

                                if (fixFont)
                                {
                                    font.Dispose();
                                }
                            }
                        }

                        // Draw HUD text
                        string text;

                        if (f.CoordinatesValid)
                        {
                            float altitude = f.AltitudeSmoothed ?? f.Altitude;
                            if (altitude > 0.0f)
                            {
                                text = String.Format("                                {0}\n                                Altitude: {1:.0}m", f.Time.ToString("yyyy/MM/dd HH:mm:ss"), altitude);
                            }
                            else
                            {
                                text = "                                " + f.Time.ToString("yyyy/MM/dd HH:mm:ss");
                            }

                            DrawTextHelper(g, text, hudFont, aProject.HudTextColor,
                                Project.Instance.TextOutlineColor,
                                Project.Instance.TextOutlineThickness,
                                hudFont.SizeInPoints,
                                new PointF(aProject.HudTextPosition.X, aProject.HudTextPosition.Y));

                            text = String.Format("Longitude: {1:.000}\u00B0W\nLatitude: {0:.000}\u00B0N", f.Latitude, f.Longitude);
                            DrawTextHelper(g, text, hudFont, aProject.HudTextColor,
                                Project.Instance.TextOutlineColor,
                                Project.Instance.TextOutlineThickness,
                                hudFont.SizeInPoints,
                                new PointF(aProject.HudTextPosition.X, aProject.HudTextPosition.Y));
                        }
                        else
                        {
                            text = "                                " + f.Time.ToString("yyyy/MM/dd HH:mm:ss");
                            DrawTextHelper(g, text, hudFont, aProject.HudTextColor,
                                Project.Instance.TextOutlineColor,
                                Project.Instance.TextOutlineThickness,
                                hudFont.SizeInPoints,
                                new PointF(aProject.HudTextPosition.X, aProject.HudTextPosition.Y));
                        }
                    }

                    if (aOverlayFrameNumber)
                    {
                        string text = String.Format("{0} {1}", aIndex, f.SourcePath);
                        DrawTextHelper(g, text, hudFont, Color.White, Color.Black, 3.0f, 24.0f, new PointF(50.0f, 50.0f));
                    }

                    if (fixFont)
                    {
                        hudFont.Dispose();
                    }
                }

                return img;
            }

            return null;
        }


        public static Vector2 AutoRegister(int aSlidableIndex, int aFixedIndex)
        {
            return AutoRegister(aSlidableIndex, 
                                aFixedIndex, 
                                (int)Project.Instance.AutoRegisterCenter.X,
                                (int)Project.Instance.AutoRegisterCenter.Y,
                                Project.Instance.AutoRegisterSearchRadius);
        }


        public void InvalidateCachedImage(int aIndex)
        {
            mImageCache.Remove(aIndex.ToString());
        }


        public void InvalidateCachedImages()
        {
            mImageCache.Clear();
        }


        public void InvalidateCachedImages(int aFrom, int aTo)
        {
            for (int i = aFrom; i < aTo; ++i)
            {
                InvalidateCachedImage(i);
            }
        }

        #endregion
        #region ----- Private helpers -----

        private Bitmap GenerateImage(int aIndex, Frame f)
        {
            return GenerateImage(Project.Instance, aIndex, f, mOverlayCheckbox.Checked, mColorCorrectionButton.Checked);
        }


        private static void DrawTextHelper(Graphics g, string aText, Font aFont, Color aFillColor, Color aOutlineColor, float aOutlineSize, float aPointSize, PointF aLocation)
        {
            SolidBrush b = new SolidBrush(aFillColor);
            GraphicsPath path = new GraphicsPath();
            path.AddString(aText, aFont.FontFamily, (int)aFont.Style, aPointSize, aLocation, new StringFormat());
            Pen p = new Pen(aOutlineColor, 2.0f * aOutlineSize);
            p.LineJoin = LineJoin.Round;
            g.DrawPath(p, path);
            g.FillPath(b, path);
            path.Dispose();
            p.Dispose();
            b.Dispose();
        }


        // Draws display-only stuff like guidelines and blur region boundaries, which will never be output.
        // Works on a copy of the input image.
        private Bitmap DecorateImage(Bitmap aImage)
        {
            if (aImage == null)
            {
                return null;
            }

            // Make a copy and apply zoom.
            float scale = ZoomToScale(mZoom);
            ImageHelper ih = new ImageHelper();
            ih.OutputSize = new Size((int)(scale * Project.Instance.OutputWidth), (int)(scale * Project.Instance.OutputHeight));
            ih.Rotation = 0.0f;
            ih.Scale = scale;
            ih.Translation = Vector2.Zero;
            Bitmap img = ih.Transform(aImage);

            // Draw guidelines.
            if (mGuidelinesCheckBox.Checked)
            {
                DrawGuidelines(img, scale);
            }

            Graphics g = Graphics.FromImage(img);

            // Draw blur region boundary being manipulated by mouse.
            if (mSelectedBlurRegion != null)
            {
                Vector2 offset = mCurrentFrame.AccumulatedJiggleCorrection + Project.Instance.TranslationTimeline[(double)mCurrentIndex];
                int xOffset = (int)Math.Round(offset.X);
                int yOffset = -(int)Math.Round(offset.Y);
                g.DrawRectangle(mSelectPen, scale * (mSelectedBlurRegion.Bounds.Left + xOffset), scale * (mSelectedBlurRegion.Bounds.Top + yOffset), scale * mSelectedBlurRegion.Bounds.Width, scale * mSelectedBlurRegion.Bounds.Height);
            }

            // Draw autoregister region
            if (RegistrationMode)
            {
                int arX = (int)(Project.Instance.AutoRegisterCenter.X);
                int arY = (int)(Project.Instance.AutoRegisterCenter.Y);
                int r = Project.Instance.AutoRegisterSearchRadius;
                g.DrawRectangle(mSelectPen, scale * (arX - r), scale * (arY - r), scale * (2 * r + 1), scale * (2 * r + 1));
            }

            return img;
        }


        private void UpdateDecorations()
        {
            mPicture.SuspendLayout();
            mPicture.Image = DecorateImage(mUndecoratedImage);
            if (mPicture.Image != null)
            {
                mPicture.Width = mPicture.Image.Width;
                mPicture.Height = mPicture.Image.Height;
            }
            mPicture.ResumeLayout();
            Refresh();
        }


        private void UpdateImage()
        {
            InvalidateCachedImage(mCurrentIndex);
            DisplayImage(mCurrentIndex, mCurrentFrame);
        }


        private void UpdateZoomControls()
        {
            mZoom = Math.Min(Math.Max(0, mZoom), mZoomTable.Length - 1);
            mZoomLabel.Text = String.Format("Zoom: {0:0.00}x", ZoomToScale(mZoom));
            mMagnifyButton.Enabled = mZoom < (mZoomTable.Length - 1);
            mMinifyButton.Enabled = mZoom > 0;
            mDragMode = DragMode.None;
            mSelectedBlurRegion = null;
            Properties.Settings.Default.DefaultZoom = mZoom;
        }


        private float ZoomToScale(int aZoomLevel)
        {
            return mZoomTable[aZoomLevel];
        }


        private void DrawGuidelines(Image aImage, float aScale)
        {
            Graphics g = Graphics.FromImage(aImage);
            foreach (Project.Guideline line in Project.Instance.Guidelines)
            {
                float x1 = aImage.Width * line.X1;
                float y1 = aImage.Height * line.Y1;
                float x2 = aImage.Width * line.X2;
                float y2 = aImage.Height * line.Y2;
                Pen pen = new Pen(line.Color);
                g.DrawLine(pen, x1, y1, x2, y2);
                pen.Dispose();
            }
        }


        private static void ApplyBlurRegions(Bitmap aImage, Frame aFrame, int aFrameIndex)
        {
            foreach (BlurRegion br in aFrame.BlurRegions)
            {
                Region region = br.ToRegion();
                var bounds = br.GetBounds();

                double r = 0.0;
                double g = 0.0;
                double b = 0.0f;
                int numSamples = 0;

                // Find average color in the region.
                for (int y = bounds.Top; y <= bounds.Bottom; ++y)
                {
                    if ((y >= 0) && (y < aImage.Height))
                    {
                        for (int x = bounds.Left; x <= bounds.Right; ++x)
                        {
                            if (region.IsVisible(x, y) && (x >= 0) && (x  < aImage.Width))
                            {
                                Color c = aImage.GetPixel(x, y);
                                r += c.R;
                                g += c.G;
                                b += c.B;
                                numSamples++;
                            }
                        }
                    }
                }

                Color avg = Color.White;
                if (numSamples > 0)
                {
                    avg = Color.FromArgb((int)(r / numSamples), (int)(g / numSamples), (int)(b / numSamples));
                }

                // Fill the region with the average color.
                for (int y = bounds.Top; y <= bounds.Bottom; ++y)
                {
                    if ((y >= 0) && (y < aImage.Height))
                    {
                        for (int x = bounds.Left; x <= bounds.Right; ++x)
                        {
                            if (region.IsVisible(x, y) && (x >= 0) && (x < aImage.Width))
                            {
                                aImage.SetPixel(x, y, avg);
                            }
                        }
                    }
                }
            }
        }


        private static void DrawCompass(Graphics g, Vector2 aPosition, float aScale, int aAngle)
        {
            while (aAngle < 0)
            {
                aAngle += 360;
            }
            aAngle %= 360;

            string resourceName = String.Format("compass_{0:0000}", aAngle);
            Bitmap bmp = Project.Instance.GetCompassImage(resourceName);
            if (bmp != null)
            {
                g.DrawImage(bmp, aPosition.X, aPosition.Y, aScale * bmp.Width, aScale * bmp.Height);
            }
        }


        private static void DrawArrow(Graphics g, float aAngle, float x, float y)
        {
            Pen arrowPen = new Pen(Color.Blue, 2.0f);
            aAngle = (float)(Math.PI * (aAngle - 90.0) / 180.0);
            const float L = 7.0f;
            const float A1 = (float)(3.0 * Math.PI / 4.0);
            const float A2 = (float)(5.0 * Math.PI / 4.0);
            PointF p1 = new PointF((float)(x + L * Math.Cos(aAngle + A1)), (float)(y + L * Math.Sin(aAngle + A1)));
            PointF p2 = new PointF((float)(x + L * Math.Cos(aAngle)), (float)(y + L * Math.Sin(aAngle)));
            PointF p3 = new PointF((float)(x + L * Math.Cos(aAngle + A2)), (float)(y + L * Math.Sin(aAngle + A2)));
            g.DrawLine(arrowPen, p1, p2);
            g.DrawLine(arrowPen, p3, p2);
            arrowPen.Dispose();
        }


        private void MouseToImageCoordinates(int x, int y, out int ix, out int iy)
        {
            ix = x;
            iy = y;

            float scale = ZoomToScale(mZoom);
            ix = (int)Math.Round(x / scale);
            iy = (int)Math.Round(y / scale);
        }


        private BlurRegion FindBlurRegionUnderMouse(int x, int y)
        {

            Vector2 offset = mCurrentFrame.AccumulatedJiggleCorrection + Project.Instance.TranslationTimeline[(double)mCurrentIndex];
            int xOffset = (int)Math.Round(offset.X);
            int yOffset = -(int)Math.Round(offset.Y);

            return mCurrentFrame.FindBlurRegionScreenSpace(new PointF(x - xOffset, y - yOffset));
        }


        private static void DumpBitmap(byte[,] aBits, int aWidth, int aHeight, string aPath)
        {
            Bitmap b = new Bitmap(aWidth, aHeight);
            for (int y = 0; y < aHeight; ++y)
            {
                for (int x = 0; x < aWidth; ++x)
                {
                    int c = aBits[x, y];
                    b.SetPixel(x, y, Color.FromArgb(c, c, c));
                }
            }

            b.Save(aPath, ImageFormat.Jpeg);
            b.Dispose();
        }


        private static void GetLuma(Bitmap aImg, int aLeft, int aTop, int aWidth, int aHeight, ref byte[,] aOutput)
        {
            for (int y = 0; y < aHeight; ++y)
            {
                for (int x = 0; x < aWidth; ++x)
                {
                    Color c = aImg.GetPixel(aLeft + x, aTop + y);
                    float lum = c.GetBrightness();
                    aOutput[x, y] = (byte)(255.0f * lum);
                }
            }
        }


        private static Vector2 AutoRegister(int aSlidableIndex, int aFixedIndex, int aX, int aY, int aRadius)
        {
            Frame prevFrame = Project.Instance.GetFrame(aFixedIndex);
            Frame curFrame = Project.Instance.GetFrame(aSlidableIndex);

            Bitmap fix = GenerateImage(Project.Instance, aFixedIndex, prevFrame, false, false);
            Bitmap slide = GenerateImage(Project.Instance, aSlidableIndex, curFrame, false, false);

            if ((fix == null) || (slide == null))
            {
                return Vector2.Zero;
            }

            Point result = slide.Register(fix, new Point(aX, aY), aRadius, Project.Instance.AutoregisterMaxDelta);

            GC.Collect();

            return new Vector2(result.X, -result.Y);
        }

        #endregion
        #region ----- Mouse interaction -----

        private void mPicture_MouseDown(object sender, MouseEventArgs e)
        {
            if (mCurrentFrame == null)
            {
                return;
            }

            mEnableJoystick = false;
            mSelectedBlurRegion = null;

            int mx, my;
            MouseToImageCoordinates(e.X, e.Y, out mx, out my);

            Vector2 offset = mCurrentFrame.AccumulatedJiggleCorrection + Project.Instance.TranslationTimeline[(double)mCurrentIndex];
            int xOffset = (int)Math.Round(offset.X);
            int yOffset = -(int)Math.Round(offset.Y);

            if ((e.Button & System.Windows.Forms.MouseButtons.Left) == System.Windows.Forms.MouseButtons.Left)
            {
                mDragMode = DragMode.DefineRegion;
                mDragStartX = mx;
                mDragStartY = my;

                mSelectedBlurRegion = FindBlurRegionUnderMouse(mx, my);

                if (RegistrationMode)
                {
                    mDragMode = DragMode.ManualRegister;
                }
                else if (mSelectedBlurRegion != null)
                {
                    mDragMode = DragMode.MoveRegion;
                    mDragOffsetX = mx - mSelectedBlurRegion.Bounds.Left + xOffset;
                    mDragOffsetY = my - mSelectedBlurRegion.Bounds.Top + yOffset;
                }
                else
                {
                    mSelectedBlurRegion = new BlurRegion();
                    mSelectedBlurRegion.Shape = (BlurRegion.ShapeType)mRegionTypeBox.SelectedItem;
                    mSelectedBlurRegion.Bounds = new Rectangle(mDragStartX - xOffset, mDragStartY - yOffset, 0, 0);
                }
            }
        }


        private void mPicture_MouseMove(object sender, MouseEventArgs e)
        {
            if (mCurrentFrame == null)
            {
                return;
            }

            int mx, my;
            MouseToImageCoordinates(e.X, e.Y, out mx, out my);
            mLastMouseImageX = mx;
            mLastMouseImageY = my;

            if ((e.Button & System.Windows.Forms.MouseButtons.Left) == System.Windows.Forms.MouseButtons.Left)
            {
                Vector2 offset = mCurrentFrame.AccumulatedJiggleCorrection + Project.Instance.TranslationTimeline[(double)mCurrentIndex];
                int xOffset = (int)Math.Round(offset.X);
                int yOffset = -(int)Math.Round(offset.Y);

                switch (mDragMode)
                {
                    case DragMode.ManualRegister:
                        mCurrentFrame.JiggleCorrection += new Vector2(mx - mDragStartX, mDragStartY - my);
                        mDragStartX = mx;
                        mDragStartY = my;
                        Project.Instance.IsSaved = false;
                        Project.Instance.UpdateAccumulatedProperties(mCurrentIndex - 1);
                        InvalidateCachedImages(mCurrentIndex, Project.Instance.GetNumFrames() - 1);
                        mControlPanel.UpdateJiggleValues();
                        UpdateImage();
                        break;
                    case DragMode.DefineRegion:
                        if (mSelectedBlurRegion != null)
                        {
                            int left = Math.Min(mx, mDragStartX);
                            int right = Math.Max(mx, mDragStartX);
                            int top = Math.Min(my, mDragStartY);
                            int bottom = Math.Max(my, mDragStartY);

                            mSelectedBlurRegion.Bounds = new Rectangle(left - xOffset, top - yOffset, right - left, bottom - top);
                            UpdateDecorations();
                        }
                        break;
                    case DragMode.MoveRegion:
                        if (mSelectedBlurRegion != null)
                        {
                            mSelectedBlurRegion.Bounds = new Rectangle(mx - mDragOffsetX, my - mDragOffsetY, mSelectedBlurRegion.Bounds.Width, mSelectedBlurRegion.Bounds.Height);
                            UpdateDecorations();
                        }
                        break;
                    default:
                        break;
                }
            }
        }


        private void mPicture_MouseUp(object sender, MouseEventArgs e)
        {
            if (mCurrentFrame == null)
            {
                return;
            }

            if ((e.Button & System.Windows.Forms.MouseButtons.Left) == System.Windows.Forms.MouseButtons.Left)
            {
                if ((mSelectedBlurRegion != null) && (mCurrentFrame != null))
                {
                    if (mDragMode == DragMode.DefineRegion)
                    {
                        if (mSelectedBlurRegion.Shape == BlurRegion.ShapeType.Autoregister)
                        {
                            Vector2 offset = mCurrentFrame.AccumulatedJiggleCorrection + Project.Instance.TranslationTimeline[(double)mCurrentIndex];
                            int xOffset = (int)Math.Round(offset.X);
                            int yOffset = -(int)Math.Round(offset.Y);

                            Project.Instance.SetAutoRegisterCenter(
                                (int)(((mSelectedBlurRegion.Bounds.Left + mSelectedBlurRegion.Bounds.Right) / 2) + xOffset), 
                                (int)(((mSelectedBlurRegion.Bounds.Top + mSelectedBlurRegion.Bounds.Bottom) / 2) + yOffset));
                            Project.Instance.AutoRegisterSearchRadius = Math.Max(mSelectedBlurRegion.Bounds.Width, mSelectedBlurRegion.Bounds.Height) / 2;
                            mRegionTypeBox.SelectedIndex = 0;
                        }
                        else
                        {
                            mCurrentFrame.AddBlurRegionScreenSpace(mSelectedBlurRegion.Bounds, mSelectedBlurRegion.Shape);
                            mSelectedBlurRegion = null;
                            Project.Instance.IsSaved = false;
                        }
                    }

                    UpdateImage();
                }

                mDragMode = DragMode.None;
            }
            else if ((e.Button & System.Windows.Forms.MouseButtons.Right) == System.Windows.Forms.MouseButtons.Right)
            {
                int mx, my;
                MouseToImageCoordinates(e.X, e.Y, out mx, out my);
                mSelectedBlurRegion = FindBlurRegionUnderMouse(mx, my);
                if (mSelectedBlurRegion != null)
                {
                    UpdateDecorations();
                    mContextMenu.Show(mPicture, e.X, e.Y);
                }
            }

            mEnableJoystick = true;
        }

        #endregion
        #region ----- Joystick interaction -----

        private void mJoystickTimer_Tick(object sender, EventArgs e)
        {
            if (!mEnableJoystick || (mCurrentFrame == null))
            {
                return;
            }

            int numFrames = Project.Instance.GetNumFrames();
            mEnableJoystick = false;
            bool changed = false;
            int frameDelta = 0;
            if (mJoystick.AButton)
            {
                Vector2 delta = mJoystick.LeftStick;
                delta = delta * 5.0f;
                mCurrentFrame.JiggleCorrection += delta;
                Project.Instance.UpdateAccumulatedProperties(mCurrentIndex - 1);
                InvalidateCachedImages(mCurrentIndex, numFrames - 1);
                changed = true;
            }

            if (mJoystick.YButton)
            {
                mRegistrationModeButton.Checked = !mRegistrationModeButton.Checked;
                changed = true;
            }

            if (mJoystick.XButton && (mCurrentIndex > 0))
            {
                Frame cur = Project.Instance.GetFrame(mCurrentIndex);
                Vector2 delta = AutoRegister(mCurrentIndex, mCurrentIndex - 1);
                if (delta.Magnitude < 25.0f)
                {
                    cur.JiggleCorrection += delta;
                    Project.Instance.UpdateAccumulatedProperties(mCurrentIndex - 1);
                    InvalidateCachedImages(mCurrentIndex, numFrames - 1);
                    changed = true;
                }
            }

            if (mJoystick.TriggerSum != 0.0f)
            {
                mCurrentFrame.AngleCorrection += mJoystick.TriggerSum;
                changed = true;
            }

            if (mJoystick.LeftShoulderButton && (mCurrentIndex > 0))
            {
                frameDelta = -1;
                changed = true;
            }

            if (mJoystick.RightShoulderButton && (mCurrentIndex < (numFrames - 1)))
            {
                frameDelta = 1;
                changed = true;
            }

            if (changed)
            {
                Project.Instance.IsSaved = false;
                InvalidateCachedImage(mCurrentIndex);
                mControlPanel.ShowImage(mCurrentIndex + frameDelta);
            }

            mEnableJoystick = true;
        }

        #endregion
        #region ----- Keyboard interaction -----

        private void PreviewPanel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((mCurrentFrame == null) || (mUndecoratedImage == null))
            {
                return;
            }

            int mx = mLastMouseImageX;
            int my = mLastMouseImageY;

            if (e.KeyChar == 'r')
            { 
                if ((mx < 0) || (my < 0) || (mx >= mUndecoratedImage.Width) || (my >= mUndecoratedImage.Height))
                {
                    return;
                }

                Vector2 delta = AutoRegister(mCurrentIndex, mCurrentIndex - 1, mx, my, 20);
                if (delta.Magnitude < 25.0f)
                {
                    mCurrentFrame.JiggleCorrection += delta;
                    Project.Instance.UpdateAccumulatedProperties(mCurrentIndex - 1);
                    InvalidateCachedImages(mCurrentIndex, Project.Instance.GetNumFrames() - 1);
                    Project.Instance.IsSaved = false;
                    InvalidateCachedImage(mCurrentIndex);
                    mControlPanel.ShowImage(mCurrentIndex);
                }

                e.Handled = true;
            }
            else if (e.KeyChar == '+')
            {
                mControlPanel.ShowImage(mCurrentIndex + 1);
                e.Handled = true;
            }
            else if (e.KeyChar == '-')
            {
                mControlPanel.ShowImage(mCurrentIndex - 1);
                e.Handled = true;
            }
            else if (e.KeyChar == 'o')
            {
                mOverlayCheckbox.Checked = !mOverlayCheckbox.Checked;
                e.Handled = true;
            }
            else if (e.KeyChar == 'g')
            {
                mGuidelinesCheckBox.Checked = !mGuidelinesCheckBox.Checked;
                e.Handled = true;
            }
            else if (e.KeyChar == 'm')
            {
                mRegistrationModeButton.Checked = !mRegistrationModeButton.Checked;
                e.Handled = true;
            }
            else if (e.KeyChar == ' ')
            {
                mRefreshButton_Click(null, null);
                e.Handled = true;
            }
        }


        private void PreviewPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            int inc = e.Delta;

            if (inc != 0)
            {
                inc /= Math.Abs(inc);
                mZoom += inc;
                UpdateZoomControls();
                DisplayImage(mCurrentIndex, mCurrentFrame);
            }
        }

        #endregion
        #region ----- UI callbacks -----

        private void deleteBlurRegionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((mCurrentFrame != null)
             && (mSelectedBlurRegion != null))
            {
                mCurrentFrame.BlurRegions.Remove(mSelectedBlurRegion);
                Project.Instance.IsSaved = false;
                mDragMode = DragMode.None;
                mSelectedBlurRegion = null;
                UpdateImage();
            }
        }


        private void mCopyToTaggedButton_Click(object sender, EventArgs e)
        {
            string tagPath = Path.Combine(Project.Instance.ProjectRoot, "tagged");
            if (!Directory.Exists(tagPath))
            {
                Directory.CreateDirectory(tagPath);
            }

            if ((mCurrentFrame != null) && !string.IsNullOrEmpty(mCurrentFrame.SourcePath))
            {
                string destPath = Path.Combine(tagPath, Path.GetFileName(mCurrentFrame.SourcePath));
                if (File.Exists(destPath))
                {
                    System.Media.SystemSounds.Beep.Play();
                }
                else
                {
                    File.Copy(mCurrentFrame.SourcePath, destPath);
                }
            }
        }


        private void mRegistrationModeButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdateImage();
        }


        private void mGuidelinesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            DisplayImage(mCurrentIndex, mCurrentFrame);
            Properties.Settings.Default.GuidelinesChecked = mGuidelinesCheckBox.Checked;
        }


        private void mMagnifyButton_Click(object sender, EventArgs e)
        {
            mZoom++;
            UpdateZoomControls();
            DisplayImage(mCurrentIndex, mCurrentFrame);
        }


        private void mMinifyButton_Click(object sender, EventArgs e)
        {
            mZoom--;
            UpdateZoomControls();
            DisplayImage(mCurrentIndex, mCurrentFrame);
        }

        private void mRefreshButton_Click(object sender, EventArgs e)
        {
            InvalidateCachedImage(mCurrentIndex);
            DisplayImage(mCurrentIndex, mCurrentFrame);
        }


        private void mOverlayCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            InvalidateCachedImage(mCurrentIndex);
            DisplayImage(mCurrentIndex, mCurrentFrame);
            Properties.Settings.Default.ShowOverlaysChecked = mOverlayCheckbox.Checked;
        }


        private void mColorCorrectionButton_CheckedChanged(object sender, EventArgs e)
        {
            DisplayImage(mCurrentIndex, mCurrentFrame);
            Properties.Settings.Default.ColorCorrectionsChecked = mColorCorrectionButton.Checked;
        }

        #endregion
    }
}
