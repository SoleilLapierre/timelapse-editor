﻿namespace TimeLapseEditor
{
    partial class PreviewPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                mJoystick.Dispose();
                mJoystickTimer.Stop();
                mJoystickTimer = null;
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mToolStrip = new System.Windows.Forms.ToolStrip();
            this.mZoomLabel = new System.Windows.Forms.ToolStripLabel();
            this.mMagnifyButton = new System.Windows.Forms.ToolStripButton();
            this.mMinifyButton = new System.Windows.Forms.ToolStripButton();
            this.mPicture = new System.Windows.Forms.PictureBox();
            this.mGuidelinesCheckBox = new System.Windows.Forms.CheckBox();
            this.mRegionTypeBox = new System.Windows.Forms.ComboBox();
            this.mContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteBlurRegionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mCopyToTaggedButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mRegistrationModeButton = new System.Windows.Forms.CheckBox();
            this.mRefreshButton = new System.Windows.Forms.Button();
            this.mOverlayCheckbox = new System.Windows.Forms.CheckBox();
            this.mColorCorrectionButton = new System.Windows.Forms.CheckBox();
            this.mToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mPicture)).BeginInit();
            this.mContextMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mToolStrip
            // 
            this.mToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mZoomLabel,
            this.mMagnifyButton,
            this.mMinifyButton});
            this.mToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mToolStrip.Name = "mToolStrip";
            this.mToolStrip.Size = new System.Drawing.Size(1008, 25);
            this.mToolStrip.TabIndex = 0;
            this.mToolStrip.Text = "toolStrip1";
            // 
            // mZoomLabel
            // 
            this.mZoomLabel.AutoSize = false;
            this.mZoomLabel.Name = "mZoomLabel";
            this.mZoomLabel.Size = new System.Drawing.Size(72, 22);
            this.mZoomLabel.Text = "Zoom: 1x";
            // 
            // mMagnifyButton
            // 
            this.mMagnifyButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.mMagnifyButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mMagnifyButton.Name = "mMagnifyButton";
            this.mMagnifyButton.Size = new System.Drawing.Size(23, 22);
            this.mMagnifyButton.Text = "+";
            this.mMagnifyButton.Click += new System.EventHandler(this.mMagnifyButton_Click);
            // 
            // mMinifyButton
            // 
            this.mMinifyButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.mMinifyButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mMinifyButton.Name = "mMinifyButton";
            this.mMinifyButton.Size = new System.Drawing.Size(23, 22);
            this.mMinifyButton.Text = "-";
            this.mMinifyButton.Click += new System.EventHandler(this.mMinifyButton_Click);
            // 
            // mPicture
            // 
            this.mPicture.Location = new System.Drawing.Point(0, 0);
            this.mPicture.MaximumSize = new System.Drawing.Size(11000, 11000);
            this.mPicture.Name = "mPicture";
            this.mPicture.Size = new System.Drawing.Size(300, 300);
            this.mPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.mPicture.TabIndex = 1;
            this.mPicture.TabStop = false;
            this.mPicture.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mPicture_MouseDown);
            this.mPicture.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mPicture_MouseMove);
            this.mPicture.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mPicture_MouseUp);
            // 
            // mGuidelinesCheckBox
            // 
            this.mGuidelinesCheckBox.AutoSize = true;
            this.mGuidelinesCheckBox.Location = new System.Drawing.Point(131, 4);
            this.mGuidelinesCheckBox.Name = "mGuidelinesCheckBox";
            this.mGuidelinesCheckBox.Size = new System.Drawing.Size(75, 17);
            this.mGuidelinesCheckBox.TabIndex = 2;
            this.mGuidelinesCheckBox.Text = "Guidelines";
            this.mGuidelinesCheckBox.UseVisualStyleBackColor = true;
            this.mGuidelinesCheckBox.CheckedChanged += new System.EventHandler(this.mGuidelinesCheckBox_CheckedChanged);
            // 
            // mRegionTypeBox
            // 
            this.mRegionTypeBox.CausesValidation = false;
            this.mRegionTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mRegionTypeBox.Location = new System.Drawing.Point(307, 2);
            this.mRegionTypeBox.Name = "mRegionTypeBox";
            this.mRegionTypeBox.Size = new System.Drawing.Size(121, 21);
            this.mRegionTypeBox.TabIndex = 3;
            // 
            // mContextMenu
            // 
            this.mContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteBlurRegionToolStripMenuItem});
            this.mContextMenu.Name = "mContextMenu";
            this.mContextMenu.Size = new System.Drawing.Size(172, 26);
            // 
            // deleteBlurRegionToolStripMenuItem
            // 
            this.deleteBlurRegionToolStripMenuItem.Name = "deleteBlurRegionToolStripMenuItem";
            this.deleteBlurRegionToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.deleteBlurRegionToolStripMenuItem.Text = "Delete Blur Region";
            this.deleteBlurRegionToolStripMenuItem.Click += new System.EventHandler(this.deleteBlurRegionToolStripMenuItem_Click);
            // 
            // mCopyToTaggedButton
            // 
            this.mCopyToTaggedButton.Location = new System.Drawing.Point(434, 0);
            this.mCopyToTaggedButton.Name = "mCopyToTaggedButton";
            this.mCopyToTaggedButton.Size = new System.Drawing.Size(92, 23);
            this.mCopyToTaggedButton.TabIndex = 4;
            this.mCopyToTaggedButton.Text = "Copy to Tagged";
            this.mCopyToTaggedButton.UseVisualStyleBackColor = true;
            this.mCopyToTaggedButton.Click += new System.EventHandler(this.mCopyToTaggedButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(212, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Blur Region Type";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.mPicture);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 705);
            this.panel1.TabIndex = 6;
            // 
            // mRegistrationModeButton
            // 
            this.mRegistrationModeButton.AutoSize = true;
            this.mRegistrationModeButton.Location = new System.Drawing.Point(547, 4);
            this.mRegistrationModeButton.Name = "mRegistrationModeButton";
            this.mRegistrationModeButton.Size = new System.Drawing.Size(112, 17);
            this.mRegistrationModeButton.TabIndex = 7;
            this.mRegistrationModeButton.Text = "Registration Mode";
            this.mRegistrationModeButton.UseVisualStyleBackColor = true;
            this.mRegistrationModeButton.CheckedChanged += new System.EventHandler(this.mRegistrationModeButton_CheckedChanged);
            // 
            // mRefreshButton
            // 
            this.mRefreshButton.Location = new System.Drawing.Point(665, 0);
            this.mRefreshButton.Name = "mRefreshButton";
            this.mRefreshButton.Size = new System.Drawing.Size(75, 23);
            this.mRefreshButton.TabIndex = 8;
            this.mRefreshButton.Text = "Refresh";
            this.mRefreshButton.UseVisualStyleBackColor = true;
            this.mRefreshButton.Click += new System.EventHandler(this.mRefreshButton_Click);
            // 
            // mOverlayCheckbox
            // 
            this.mOverlayCheckbox.AutoSize = true;
            this.mOverlayCheckbox.Checked = true;
            this.mOverlayCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mOverlayCheckbox.Location = new System.Drawing.Point(746, 4);
            this.mOverlayCheckbox.Name = "mOverlayCheckbox";
            this.mOverlayCheckbox.Size = new System.Drawing.Size(97, 17);
            this.mOverlayCheckbox.TabIndex = 9;
            this.mOverlayCheckbox.Text = "Show Overlays";
            this.mOverlayCheckbox.UseVisualStyleBackColor = true;
            this.mOverlayCheckbox.CheckedChanged += new System.EventHandler(this.mOverlayCheckbox_CheckedChanged);
            // 
            // mColorCorrectionButton
            // 
            this.mColorCorrectionButton.AutoSize = true;
            this.mColorCorrectionButton.Checked = true;
            this.mColorCorrectionButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mColorCorrectionButton.Location = new System.Drawing.Point(840, 4);
            this.mColorCorrectionButton.Name = "mColorCorrectionButton";
            this.mColorCorrectionButton.Size = new System.Drawing.Size(135, 17);
            this.mColorCorrectionButton.TabIndex = 10;
            this.mColorCorrectionButton.Text = "Apply Color Corrections";
            this.mColorCorrectionButton.UseVisualStyleBackColor = true;
            this.mColorCorrectionButton.CheckedChanged += new System.EventHandler(this.mColorCorrectionButton_CheckedChanged);
            // 
            // PreviewPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.ControlBox = false;
            this.Controls.Add(this.mColorCorrectionButton);
            this.Controls.Add(this.mOverlayCheckbox);
            this.Controls.Add(this.mRefreshButton);
            this.Controls.Add(this.mRegistrationModeButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mCopyToTaggedButton);
            this.Controls.Add(this.mRegionTypeBox);
            this.Controls.Add(this.mGuidelinesCheckBox);
            this.Controls.Add(this.mToolStrip);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(320, 200);
            this.Name = "PreviewPanel";
            this.Text = "Preview";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PreviewPanel_KeyPress);
            this.mToolStrip.ResumeLayout(false);
            this.mToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mPicture)).EndInit();
            this.mContextMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mToolStrip;
        private System.Windows.Forms.PictureBox mPicture;
        private System.Windows.Forms.ToolStripLabel mZoomLabel;
        private System.Windows.Forms.ToolStripButton mMagnifyButton;
        private System.Windows.Forms.ToolStripButton mMinifyButton;
        private System.Windows.Forms.CheckBox mGuidelinesCheckBox;
        private System.Windows.Forms.ComboBox mRegionTypeBox;
        private System.Windows.Forms.ContextMenuStrip mContextMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteBlurRegionToolStripMenuItem;
        private System.Windows.Forms.Button mCopyToTaggedButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox mRegistrationModeButton;
        private System.Windows.Forms.Button mRefreshButton;
        private System.Windows.Forms.CheckBox mOverlayCheckbox;
        private System.Windows.Forms.CheckBox mColorCorrectionButton;
    }
}