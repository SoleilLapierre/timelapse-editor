﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeLapseEditor
{
    public partial class BatchDialog : Form
    {
        public delegate bool WorkCallback(int aIndex, bool aOption);
        public event WorkCallback ProcessFrameCallback;
        public delegate bool ProgressCallback(int aIndex);
        public event ProgressCallback FrameProgressCallback;
        private bool mProcessing = false;
        private BackgroundWorker mWorker;
        private Project mProject;

        public BatchDialog(Project aProject, string aMessage, int aStartIndex, int aEndIndex)
        {
            InitializeComponent();

            mProject = aProject;
            mFirstFrameBox.Value = (decimal)aStartIndex;
            mLastFrameBox.Value = (decimal)aEndIndex;
            mActionLabel.Text = aMessage;

            mWorker = new BackgroundWorker();
            mWorker.WorkerReportsProgress = true;
            mWorker.WorkerSupportsCancellation = true;
            mWorker.DoWork += new DoWorkEventHandler(mWorker_DoWork);
            mWorker.ProgressChanged += new ProgressChangedEventHandler(mWorker_ProgressChanged);
            mWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(mWorker_RunWorkerCompleted);
        }


        private void mCancelButton_Click(object sender, EventArgs e)
        {
            if (mProcessing)
            {
                mProcessing = false;
                mStartButton.Enabled = true;
                mEndButton.Enabled = true;
                mMinus100Button.Enabled = true;
                mPlus100Button.Enabled = true;
                mOptionCheckbox.Enabled = true;
                mWorker.CancelAsync();
            }

            Close();
        }

        public bool Autostart { get; set; }

        public int Start { get; set; }
        public int End { get; set; }

        public void SetOptionName(string aName)
        {
            mOptionCheckbox.Text = aName;
        }


        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            if (Autostart)
            {
                Go();
            }
        }


        private void Go()
        {
            if ((ProcessFrameCallback == null) || mProcessing)
            {
                return;
            }

            int numFrames = mProject.GetNumFrames();

            int start = (int)mFirstFrameBox.Value;
            start = Math.Max(0, Math.Min(start, numFrames - 1));

            int end = (int)mLastFrameBox.Value;
            end = Math.Max(0, Math.Min(end, numFrames - 1));

            mProgressBar.Minimum = start;
            mProgressBar.Maximum = end;

            Start = start;
            End = start;

            Tuple<int, int, bool> threadArgs = new Tuple<int, int, bool>(start, end, mOptionCheckbox.Checked);

            mProcessing = true;
            mStartButton.Enabled = false;
            mEndButton.Enabled = false;
            mMinus100Button.Enabled = false;
            mPlus100Button.Enabled = false;
            mOptionCheckbox.Enabled = false;
            mWorker.RunWorkerAsync(threadArgs);
        }


        private void mGoButton_Click(object sender, EventArgs aArgs)
        {
            Go();
        }


        private void mWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            Tuple<int, int, bool> args = (Tuple<int, int, bool>)e.Argument;

            int start = args.Item1;
            int end = args.Item2;

            for (int i = start; i <= end; ++i)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                try
                {
                    if (!ProcessFrameCallback(i, args.Item3))
                    {
                        MessageBox.Show(String.Format("Batch process self-aborted sy frame {0}.", i));
                        break;
                    }

                    worker.ReportProgress(i);
                    GC.Collect();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Batch process aborted at frame {0} by exception: {1}", i, ex.ToString()));
                    break;
                }
            }
        }


        private void mWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!mProcessing)
            {
                return;
            }

            int frameIndex = e.ProgressPercentage;
            mProgressBar.Value = frameIndex;
            mIndexLabel.Text = frameIndex.ToString();
            End = frameIndex;
            if (FrameProgressCallback != null)
            {
                if (!FrameProgressCallback(frameIndex))
                {
                    mWorker.CancelAsync();
                }
            }
        }



        private void mWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            mProcessing = false;
            mProgressBar.Minimum = 0;
            mProgressBar.Value = 0;
            mIndexLabel.Text = "";
            mStartButton.Enabled = true;
            mEndButton.Enabled = true;
            mMinus100Button.Enabled = true;
            mPlus100Button.Enabled = true;
            mOptionCheckbox.Enabled = true;
            Close();
        }


        private void mStartButton_Click(object sender, EventArgs e)
        {
            mFirstFrameBox.Value = 0;
        }


        private void mMinus100Button_Click(object sender, EventArgs e)
        {
            int start = 100 * (((int)mFirstFrameBox.Value - 1) / 100);
            start = Math.Max(0, start);
            mFirstFrameBox.Value = (decimal)start;
        }


        private void mPlus100Button_Click(object sender, EventArgs e)
        {
            int end = 100 * (((int)mLastFrameBox.Value / 100) + 1);
            end = Math.Min(end, mProject.GetNumFrames() - 1);
            mLastFrameBox.Value = (decimal)end;
        }


        private void mEndButton_Click(object sender, EventArgs e)
        {
            mLastFrameBox.Value = (decimal)(mProject.GetNumFrames() - 1);
        }
    }
}
