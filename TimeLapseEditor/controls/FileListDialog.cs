﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace TimeLapseEditor
{
    public partial class FileListDialog : Form
    {
        #region ----- Internal types -----

        public class Session
        {
            private List<int> mFrames = new List<int>();

            public Session()
            {
                Name = "Default";
            }

            public string Name { get; set; }
            public List<int> Frames { get { return mFrames; } }

            public int GetMaxIndex()
            {
                int m = -1;
                foreach (int i in mFrames)
                {
                    m = Math.Max(m, i);
                }

                return m;
            }

            public override string ToString()
            {
                return Name;
            }
        }

        #endregion
        #region ----- Member data -----

        private Project mProject;
        private FrameStore mFrameStore;
        private DialogResult mDialogResult = DialogResult.Cancel;
        private List<Session> mSessions = new List<Session>();
        private List<Frame> mClipboard = null;
        private bool mEnableEvents = true;

        #endregion
        #region ----- Construction -----

        public FileListDialog(Project aProject)
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(FileListDialog_FormClosing);

            mProject = aProject;
            mFrameStore = mProject.FrameStore;
            mDialogResult = DialogResult.Cancel;

            Repopulate();
        }

        #endregion
        #region ----- UI updates -----

        private void Repopulate()
        {
            mEnableEvents = false;
            mSessionListBox.SuspendLayout();

            string lastSelectedSessionName = null;
            Session lastSession = mSessionListBox.SelectedItem as Session;
            if (lastSession != null)
            {
                lastSelectedSessionName = lastSession.Name;
            }

            mSessions = new List<Session>();
            string lastSessionName = null;
            lastSession = null;

            int count = mFrameStore.Count;
            mProgressBar.Value = 0;
            mProgressBar.Maximum = count;
            int selectedIndex = -1;

            for (int i = 0; i < count; ++i)
            {
                mProgressBar.Value = i;
                Frame f = mFrameStore[i];
                string sessionName = "Unnamed Session";
                if (!string.IsNullOrEmpty(f.ImportSession))
                {
                    sessionName = f.ImportSession;
                }

                if ((lastSession == null) || !sessionName.Equals(lastSessionName))
                {
                    lastSession = new Session();
                    lastSession.Name = sessionName;
                    lastSessionName = sessionName;
                    mSessions.Add(lastSession);
                }

                lastSession.Frames.Add(i);

                if (lastSessionName.Equals(lastSelectedSessionName))
                {
                    selectedIndex = mSessions.Count - 1;
                }
            }

            mSessionListBox.Items.Clear();
            foreach (Session s in mSessions)
            {
                mSessionListBox.Items.Add(s);
            }

            mSessionListBox.SelectedIndex = selectedIndex;

            mSessionListBox.ResumeLayout();
            mProgressBar.Value = 0;
            mEnableEvents = true;

            PopulateSelectedSession();
        }


        private void PopulateSelectedSession()
        {
            mEnableEvents = false;
            mListView.SuspendLayout();
            mListView.Items.Clear();

            Session selectedSession = SelectedSession;
            if (selectedSession != null)
            {
                foreach (int i in selectedSession.Frames)
                {
                    Frame f = mFrameStore[i];
                    ListViewItem row = mListView.Items.Add(Path.GetFileName(f.SourcePath));
                    row.Tag = f;
                    row.SubItems.Add(f.Time.ToString());
                    row.SubItems.Add(f.Longitude.ToString());
                    row.SubItems.Add(f.Latitude.ToString());
                    row.SubItems.Add(f.Altitude.ToString());

                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    foreach (uint t in f.Sequences)
                    {
                        string seqName = Project.Instance.GetSequenceName(t);
                        if (seqName != null)
                        {
                            if (!first)
                            {
                                sb.Append(", ");
                            }

                            first = false;
                            sb.Append(seqName);
                        }
                    }
                    row.SubItems.Add(sb.ToString());

                    row.SubItems.Add(Path.GetDirectoryName(f.SourcePath));
                    row.SubItems.Add(f.MapImageFilename ?? "");
                    row.SubItems.Add(i.ToString());
                }
            }

            mListView.ResumeLayout();
            mEnableEvents = true;
        }

        #endregion
        #region ----- UI events -----
        #region ----- + File data grid

        private void mAddFilesButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents || (SelectedSession == null))
            {
                return;
            }

            OpenFileDialog fd = new OpenFileDialog();
            fd.CheckFileExists = true;
            fd.Multiselect = true;
            fd.ValidateNames = true;
            fd.Filter = "JPEG files (*.jpg)|*.jpg|PNG files (*.png)|*.png|Bitmap files (*.bmp)|*.bmp|GIF files (*.gif)|*.gif|TIFF files (*.tiff)|*.tiff|All files (*.*)|*.*";

            int index = GetFrameInsertPoint();

            if ((index >= 0) && (index < mFrameStore.Count))
            {
                fd.InitialDirectory = Path.GetDirectoryName(mFrameStore[index].SourcePath);
            }

            if (fd.ShowDialog() == DialogResult.OK)
            {
                SortedList<string, Frame> newItems = new SortedList<string, Frame>();
                mProgressBar.Value = 0;
                mProgressBar.Maximum = fd.FileNames.Length;
                foreach (string path in fd.FileNames)
                {
                    Frame f = new Frame(path);
                    f.ImportSession = SelectedSession.Name;
                    f.Project = mProject;
                    string sortKey = Path.GetFileNameWithoutExtension(f.SourcePath);
                    newItems.Add(sortKey, f);
                    mProgressBar.Increment(1);
                }

                if (index < 0)
                {
                    index = 0;
                }

                if (newItems.Count > 0)
                {
                    mFrameStore.InsertFrames(newItems.Values, index + 1);
                    mDialogResult = DialogResult.OK;
                    Repopulate();
                }
            }
        }


        private void mCutButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents || (SelectedSession == null))
            {
                return;
            }

            List<Frame> selected = new List<Frame>();

            foreach (ListViewItem row in mListView.SelectedItems)
            {
                Frame f = (Frame)row.Tag;
                selected.Add(f);
            }

            mFrameStore.Remove(selected);

            mClipboard = selected;
            mPasteButton.Enabled = ((mClipboard != null) && (mClipboard.Count > 0));

            Repopulate();
        }


        private void mCopyButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents || (SelectedSession == null))
            {
                return;
            }

            List<Frame> selected = new List<Frame>();

            foreach (ListViewItem row in mListView.SelectedItems)
            {
                Frame f = (Frame)row.Tag;
                selected.Add(f);
            }

            mClipboard = selected;
            mPasteButton.Enabled = ((mClipboard != null) && (mClipboard.Count > 0));
        }


        private void mPasteButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents || (SelectedSession == null))
            {
                return;
            }

            if (mClipboard != null)
            {
                List<Frame> selected = new List<Frame>();
                foreach (Frame f in mClipboard)
                {
                    Frame clone = new Frame(f);
                    clone.ImportSession = SelectedSession.Name;
                    selected.Add(clone);
                }

                int insertPoint = GetFrameInsertPoint();
                mFrameStore.InsertFrames(selected, insertPoint);

                Repopulate();
            }
        }


        private void mListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                bool selection = (mListView.SelectedIndices.Count > 0);
                mCopyButton.Enabled = selection;
                mCutButton.Enabled = selection;
                mPasteButton.Enabled = ((mClipboard != null) && (mClipboard.Count > 0));
                if (selection)
                {
                    mFindMapsButton.Text = "Find Maps for Selected";
                }
                else
                {
                    mFindMapsButton.Text = "Find Maps for All Images";
                }

                mPreviewBox.Image = null;
                if (mListView.SelectedItems.Count == 1)
                {
                    Frame f = mListView.SelectedItems[0].Tag as Frame;
                    if ((f != null) && File.Exists(f.SourcePath))
                    {
                        Image src = Image.FromFile(f.SourcePath);
                        Bitmap dest = new Bitmap(mPreviewBox.Width, mPreviewBox.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                        Graphics g = Graphics.FromImage(dest);
                        g.DrawImage(src, 0, 0, mPreviewBox.Width, mPreviewBox.Height);
                        mPreviewBox.Image = dest;
                    }
                }
            }
        }

        #endregion
        #region ----- + Session list box

        private void mAddSessionButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            SequenceDialog dlg = new SequenceDialog();
            dlg.Value = "New Session";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Session sess = new Session();
                sess.Name = dlg.Value;
                mSessions.Add(sess);
                mSessionListBox.Items.Add(sess);
                mSessionListBox.SelectedItem = sess;
                PopulateSelectedSession();
            }
        }


        private void mInsertSessionButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            SequenceDialog dlg = new SequenceDialog();
            dlg.Value = "New Session";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int insertPoint = mSessionListBox.SelectedIndex;
                Session sess = new Session();
                sess.Name = dlg.Value;
                if (insertPoint < 0)
                {
                    mSessions.Add(sess);
                    mSessionListBox.Items.Add(sess);
                }
                else
                {
                    mSessions.Insert(insertPoint, sess);
                    mSessionListBox.Items.Insert(insertPoint, sess);
                }

                mSessionListBox.SelectedItem = sess;
                PopulateSelectedSession();
            }
        }


        private void mSessionRenameButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents || (SelectedSession == null))
            {
                return;
            }

            SequenceDialog dlg = new SequenceDialog();
            dlg.Value = SelectedSession.Name;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SelectedSession.Name = dlg.Value;
                mSessionListBox.Refresh();
            }
        }


        private void mDeleteSessionButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents || (SelectedSession == null))
            {
                return;
            }

            List<Frame> frames = new List<Frame>();
            foreach (int i in SelectedSession.Frames)
            {
                frames.Add(mFrameStore[i]);
            }

            mFrameStore.Remove(frames);

            Repopulate();
        }


        private void mSessionListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mEnableEvents)
            {
                bool selection = (mSessionListBox.SelectedItem != null);
                mSessionRenameButton.Enabled = selection;
                mDeleteSessionButton.Enabled = selection;
                mFileGroupBox.Enabled = selection;
                PopulateSelectedSession();
                mListView_SelectedIndexChanged(sender, e);
            }
        }

        #endregion
        #region ----- + Misc. events

        private void mCloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void mFindMapsButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            Dictionary<string, string> mapTable = FindMapImages(Path.Combine(mProject.ProjectRoot, "maps"));

            mProgressBar.Value = 0;
            if (mListView.SelectedIndices.Count > 0)
            {
                mProgressBar.Maximum = mListView.SelectedIndices.Count;
                foreach (ListViewItem row in mListView.SelectedItems)
                {
                    Frame f = (Frame)row.Tag;
                    FindMap(f, mapTable);
                    mProgressBar.Increment(1);
                }
            }
            else
            {
                mProgressBar.Maximum = mFrameStore.Count;
                for (int i = 0; i < mFrameStore.Count; ++i)
                {
                    FindMap(mFrameStore[i], mapTable);
                    mProgressBar.Value = i;
                }
            }

            mProgressBar.Value = 0;
            mProject.IsSaved = false;
            PopulateSelectedSession();
        }


        private void FileListDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = mDialogResult;
        }


        private void mSmoothAltitudeButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            mProgressBar.Maximum = mFrameStore.Count;
            bool initialized = false;
            double average = 0.0;
            float prevAlt = 0.0f;
            int runLength = 0;
            int prevSign = 0;
            for (int i = 0; i < mFrameStore.Count; ++i)
            {
                Frame f = mFrameStore[i];
                if (f.Altitude > 0.0f)
                {
                    if (initialized)
                    {
                        int sign = Math.Sign(f.Altitude - prevAlt);
                        runLength = (sign == prevSign) ? runLength + 1 : 0;

                        double confidence = 0.05 + ((double)Math.Min(runLength, 7) / 10.0);
                        average = (1.0 - confidence) * average + confidence * f.Altitude;

                        prevAlt = f.Altitude;
                        prevSign = sign;

                        f.AltitudeSmoothed = (float)average;
                    }

                    if (!initialized)
                    {
                        initialized = true;
                        average = f.Altitude;
                        prevAlt = f.Altitude;
                    }
                }

                mProgressBar.Value = i;
            }

            mProgressBar.Value = 0;
            mProject.IsSaved = false;
        }


        private void mUpdateCompassButton_Click(object sender, EventArgs e)
        {
            if (!mEnableEvents)
            {
                return;
            }

            mProgressBar.Maximum = mFrameStore.Count;
            bool init;
            bool update;
            Compass compass = new Compass();

            for (int i = 0; i < mFrameStore.Count; ++i)
            {
                Frame f = mFrameStore[i];

                init = false;
                update = false;
                switch (f.CompassMode)
                {
                    case Frame.CompassUpdateMode.Initialize:
                        init = true;
                        update = false;
                        break;
                    case Frame.CompassUpdateMode.Update:
                        init = false;
                        update = true;
                        break;
                    case Frame.CompassUpdateMode.Freeze:
                        break;
                    default:
                        break;
                }

                if (init)
                {
                    compass.Heading = Project.Instance.HeadingOffsetTimeline[(double)i];
                    if (f.CoordinatesValid)
                    {
                        compass.Latitude = f.Latitude;
                        compass.Longitude = f.Longitude;
                    }
                }
                else if (update && f.CoordinatesValid)
                {
                    compass.Update(f.Latitude, f.Longitude);
                }

                f.Heading = (float)compass.Heading;

                mProgressBar.Value = i;
            }

            mProgressBar.Value = 0;
            mProject.IsSaved = false;
        }

        #endregion
        #endregion
        #region ----- Helpers -----

        private Session SelectedSession
        {
            get
            {
                return mSessionListBox.SelectedItem as Session;
            }
        }


        private int GetFrameInsertPoint()
        {
            int index = mFrameStore.Count - 1; // Default - add to end of project.
            if (mListView.SelectedIndices.Count > 0) // If there are selected frames, insert after them.
            {
                index = 0;
                foreach (int i in mListView.SelectedIndices)
                {
                    index = Math.Max(index, i + 1);
                }
            }
            else // No selected frames.
            {
                int m = -1;
                int i = mSessions.IndexOf(SelectedSession); // Use max of current selected session, working backward if necessary.
                while ((i >= 0) && (m < 0))
                {
                    m = mSessions[i].GetMaxIndex();
                    --i;
                }

                if (m >= 0)
                {
                    index = m + 1;
                }
            }

            return index;
        }


        private Dictionary<string, string> FindMapImages(string aSearchPath)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            Dictionary<string, KeyValuePair<DateTime, string>> files = new Dictionary<string, KeyValuePair<DateTime, string>>();
            if (!Directory.Exists(aSearchPath))
            {
                return result;
            }

            string[] fileArray = Directory.GetFiles(aSearchPath, "*_map.png", SearchOption.AllDirectories);
            mProgressBar.Value = 0;
            mProgressBar.Maximum = fileArray.Length;

            foreach (string fileName in fileArray)
            {
                string baseName = Path.GetFileNameWithoutExtension(fileName);
                baseName = baseName.Remove(baseName.Length - "_map".Length);
                DateTime timestamp = File.GetLastWriteTime(fileName);
                if (!files.ContainsKey(baseName))
                {
                    KeyValuePair<DateTime, string> kvp = new KeyValuePair<DateTime, string>(timestamp, fileName);
                    files.Add(baseName, kvp);
                }
                else
                {
                    KeyValuePair<DateTime, string> kvp = files[baseName];
                    if (timestamp > kvp.Key)
                    {
                        kvp = new KeyValuePair<DateTime, string>(timestamp, fileName);
                        files[baseName] = kvp;
                    }
                }

                mProgressBar.Increment(1);
            }

            foreach (string baseName in files.Keys)
            {
                result.Add(baseName, files[baseName].Value);
            }

            return result;
        }


        private void FindMap(Frame aFrame, Dictionary<string, string> aMapTable)
        {
            string baseName = Path.GetFileNameWithoutExtension(aFrame.SourcePath);
            if (aMapTable.ContainsKey(baseName))
            {
                aFrame.MapImageFilename = aMapTable[baseName];
            }
        }

        #endregion
    }
}
