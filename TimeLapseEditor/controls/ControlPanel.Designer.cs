﻿namespace TimeLapseEditor
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mMainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.mProjectMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.mNewProjectButton = new System.Windows.Forms.ToolStripMenuItem();
            this.mOpenProjectButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mProjectPropertiesButton = new System.Windows.Forms.ToolStripMenuItem();
            this.mInputFilesButton = new System.Windows.Forms.ToolStripMenuItem();
            this.renderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateMapScriptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportSequenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoregisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeBlackspaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mContrastKeyBox = new System.Windows.Forms.CheckBox();
            this.mGammaKeyBox = new System.Windows.Forms.CheckBox();
            this.mValueKeyBox = new System.Windows.Forms.CheckBox();
            this.mSaturationKeyBox = new System.Windows.Forms.CheckBox();
            this.mHueKeyBox = new System.Windows.Forms.CheckBox();
            this.mHueBox = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mScaleBox = new System.Windows.Forms.NumericUpDown();
            this.mRotationBox = new System.Windows.Forms.NumericUpDown();
            this.mXTranslationBox = new System.Windows.Forms.NumericUpDown();
            this.mYTranslationBox = new System.Windows.Forms.NumericUpDown();
            this.mScaleKeyButton = new System.Windows.Forms.CheckBox();
            this.mRotationKeyBotton = new System.Windows.Forms.CheckBox();
            this.mTranslationKeyButton = new System.Windows.Forms.CheckBox();
            this.mCompassOffsetBox = new System.Windows.Forms.NumericUpDown();
            this.mCompassKeyButton = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mLatitudeOffsetBox = new System.Windows.Forms.NumericUpDown();
            this.mLongitudeOffsetBox = new System.Windows.Forms.NumericUpDown();
            this.mLatitudeKeyButton = new System.Windows.Forms.CheckBox();
            this.mLongitudeKeyButton = new System.Windows.Forms.CheckBox();
            this.mMapZoomKeyBox = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.mSaturationBox = new System.Windows.Forms.NumericUpDown();
            this.mValueBox = new System.Windows.Forms.NumericUpDown();
            this.mGammaBox = new System.Windows.Forms.NumericUpDown();
            this.mContrastBox = new System.Windows.Forms.NumericUpDown();
            this.mMapZoomBox = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mSequenceListBox = new System.Windows.Forms.ListBox();
            this.mShowOverlayCheckBox = new System.Windows.Forms.CheckBox();
            this.mMapListBox = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mLabelCarryoverButton = new System.Windows.Forms.CheckBox();
            this.mTextOverlayListBox = new System.Windows.Forms.CheckedListBox();
            this.mDeleteTextOverlayButton = new System.Windows.Forms.Button();
            this.mAddTextOverlayButton = new System.Windows.Forms.Button();
            this.mColorButton = new System.Windows.Forms.Button();
            this.mTextOverlayYBox = new System.Windows.Forms.NumericUpDown();
            this.mTextOverlayXBox = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.mFontButton = new System.Windows.Forms.Button();
            this.mTextOverlayBox = new System.Windows.Forms.TextBox();
            this.mForceCarryoverButton = new System.Windows.Forms.CheckBox();
            this.mFrameMultipleBox = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.mAccumulatedJiggleLabel = new System.Windows.Forms.Label();
            this.mJiggleAngleBox = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.mJiggleYBox = new System.Windows.Forms.NumericUpDown();
            this.mJiggleXBox = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.mCompassModeBox = new System.Windows.Forms.ComboBox();
            this.mNextKeyButton = new System.Windows.Forms.Button();
            this.mPrevKeyButton = new System.Windows.Forms.Button();
            this.mMapCarryoverButton = new System.Windows.Forms.CheckBox();
            this.mFileIndexWidget = new TimeLapseEditor.Controls.VariableStepNumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.mMainMenuStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mHueBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mScaleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mRotationBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mXTranslationBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mYTranslationBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCompassOffsetBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLatitudeOffsetBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLongitudeOffsetBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mSaturationBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mValueBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGammaBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mContrastBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mMapZoomBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mTextOverlayYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mTextOverlayXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFrameMultipleBox)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mJiggleAngleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mJiggleYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mJiggleXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFileIndexWidget)).BeginInit();
            this.SuspendLayout();
            // 
            // mMainMenuStrip
            // 
            this.mMainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mProjectMenu,
            this.renderToolStripMenuItem});
            this.mMainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mMainMenuStrip.Name = "mMainMenuStrip";
            this.mMainMenuStrip.Size = new System.Drawing.Size(728, 24);
            this.mMainMenuStrip.TabIndex = 0;
            this.mMainMenuStrip.Text = "menuStrip1";
            // 
            // mProjectMenu
            // 
            this.mProjectMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mNewProjectButton,
            this.mOpenProjectButton,
            this.saveToolStripMenuItem,
            this.mProjectPropertiesButton,
            this.mInputFilesButton});
            this.mProjectMenu.Name = "mProjectMenu";
            this.mProjectMenu.Size = new System.Drawing.Size(56, 20);
            this.mProjectMenu.Text = "Project";
            // 
            // mNewProjectButton
            // 
            this.mNewProjectButton.Name = "mNewProjectButton";
            this.mNewProjectButton.Size = new System.Drawing.Size(137, 22);
            this.mNewProjectButton.Text = "New...";
            this.mNewProjectButton.Click += new System.EventHandler(this.mNewProjectMenuItem_Click);
            // 
            // mOpenProjectButton
            // 
            this.mOpenProjectButton.Name = "mOpenProjectButton";
            this.mOpenProjectButton.Size = new System.Drawing.Size(137, 22);
            this.mOpenProjectButton.Text = "Open...";
            this.mOpenProjectButton.Click += new System.EventHandler(this.mOpenProjectMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.mSaveProjectMenuItem_Click);
            // 
            // mProjectPropertiesButton
            // 
            this.mProjectPropertiesButton.Name = "mProjectPropertiesButton";
            this.mProjectPropertiesButton.Size = new System.Drawing.Size(137, 22);
            this.mProjectPropertiesButton.Text = "Properties...";
            this.mProjectPropertiesButton.Click += new System.EventHandler(this.mProjectPropertiesMenuItem_Click);
            // 
            // mInputFilesButton
            // 
            this.mInputFilesButton.Name = "mInputFilesButton";
            this.mInputFilesButton.Size = new System.Drawing.Size(137, 22);
            this.mInputFilesButton.Text = "Input Files...";
            this.mInputFilesButton.Click += new System.EventHandler(this.mInputFilesMenuItem_Click);
            // 
            // renderToolStripMenuItem
            // 
            this.renderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateMapScriptsToolStripMenuItem,
            this.exportSequenceToolStripMenuItem,
            this.autoregisterToolStripMenuItem,
            this.removeBlackspaceToolStripMenuItem});
            this.renderToolStripMenuItem.Name = "renderToolStripMenuItem";
            this.renderToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.renderToolStripMenuItem.Text = "Render";
            // 
            // generateMapScriptsToolStripMenuItem
            // 
            this.generateMapScriptsToolStripMenuItem.Name = "generateMapScriptsToolStripMenuItem";
            this.generateMapScriptsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.generateMapScriptsToolStripMenuItem.Text = "Generate Map Scripts...";
            this.generateMapScriptsToolStripMenuItem.Click += new System.EventHandler(this.mGenerateMapScriptsMenuItem_Click);
            // 
            // exportSequenceToolStripMenuItem
            // 
            this.exportSequenceToolStripMenuItem.Name = "exportSequenceToolStripMenuItem";
            this.exportSequenceToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.exportSequenceToolStripMenuItem.Text = "Export Sequence...";
            this.exportSequenceToolStripMenuItem.Click += new System.EventHandler(this.mExportSequenceMenuItem_Click);
            // 
            // autoregisterToolStripMenuItem
            // 
            this.autoregisterToolStripMenuItem.Name = "autoregisterToolStripMenuItem";
            this.autoregisterToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.autoregisterToolStripMenuItem.Text = "Autoregister...";
            this.autoregisterToolStripMenuItem.Click += new System.EventHandler(this.mAutoregisterMenuItem_Click);
            // 
            // removeBlackspaceToolStripMenuItem
            // 
            this.removeBlackspaceToolStripMenuItem.Name = "removeBlackspaceToolStripMenuItem";
            this.removeBlackspaceToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.removeBlackspaceToolStripMenuItem.Text = "Remove Blackspace";
            this.removeBlackspaceToolStripMenuItem.Click += new System.EventHandler(this.mRemoveBlackspaceMenuItem_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(550, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "File Index";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel1.Controls.Add(this.mContrastKeyBox, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.mGammaKeyBox, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.mValueKeyBox, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.mSaturationKeyBox, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.mHueKeyBox, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.mHueBox, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.mScaleBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.mRotationBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.mXTranslationBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.mYTranslationBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.mScaleKeyButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.mRotationKeyBotton, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.mTranslationKeyButton, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.mCompassOffsetBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.mCompassKeyButton, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.mLatitudeOffsetBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.mLongitudeOffsetBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.mLatitudeKeyButton, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.mLongitudeKeyButton, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.mMapZoomKeyBox, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.mSaturationBox, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.mValueBox, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.mGammaBox, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.mContrastBox, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.mMapZoomBox, 1, 7);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(362, 53);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(236, 329);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // mContrastKeyBox
            // 
            this.mContrastKeyBox.AutoSize = true;
            this.mContrastKeyBox.Location = new System.Drawing.Point(188, 306);
            this.mContrastKeyBox.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mContrastKeyBox.Name = "mContrastKeyBox";
            this.mContrastKeyBox.Size = new System.Drawing.Size(44, 17);
            this.mContrastKeyBox.TabIndex = 37;
            this.mContrastKeyBox.Text = "Key";
            this.mContrastKeyBox.UseVisualStyleBackColor = true;
            this.mContrastKeyBox.CheckedChanged += new System.EventHandler(this.mContrastKeyBox_CheckedChanged);
            // 
            // mGammaKeyBox
            // 
            this.mGammaKeyBox.AutoSize = true;
            this.mGammaKeyBox.Location = new System.Drawing.Point(188, 281);
            this.mGammaKeyBox.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mGammaKeyBox.Name = "mGammaKeyBox";
            this.mGammaKeyBox.Size = new System.Drawing.Size(44, 16);
            this.mGammaKeyBox.TabIndex = 36;
            this.mGammaKeyBox.Text = "Key";
            this.mGammaKeyBox.UseVisualStyleBackColor = true;
            this.mGammaKeyBox.CheckedChanged += new System.EventHandler(this.mGammaKeyBox_CheckedChanged);
            // 
            // mValueKeyBox
            // 
            this.mValueKeyBox.AutoSize = true;
            this.mValueKeyBox.Location = new System.Drawing.Point(188, 256);
            this.mValueKeyBox.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mValueKeyBox.Name = "mValueKeyBox";
            this.mValueKeyBox.Size = new System.Drawing.Size(44, 16);
            this.mValueKeyBox.TabIndex = 35;
            this.mValueKeyBox.Text = "Key";
            this.mValueKeyBox.UseVisualStyleBackColor = true;
            this.mValueKeyBox.CheckedChanged += new System.EventHandler(this.mValueKeyBox_CheckedChanged);
            // 
            // mSaturationKeyBox
            // 
            this.mSaturationKeyBox.AutoSize = true;
            this.mSaturationKeyBox.Location = new System.Drawing.Point(188, 231);
            this.mSaturationKeyBox.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mSaturationKeyBox.Name = "mSaturationKeyBox";
            this.mSaturationKeyBox.Size = new System.Drawing.Size(44, 16);
            this.mSaturationKeyBox.TabIndex = 34;
            this.mSaturationKeyBox.Text = "Key";
            this.mSaturationKeyBox.UseVisualStyleBackColor = true;
            this.mSaturationKeyBox.CheckedChanged += new System.EventHandler(this.mSaturationKeyBox_CheckedChanged);
            // 
            // mHueKeyBox
            // 
            this.mHueKeyBox.AutoSize = true;
            this.mHueKeyBox.Location = new System.Drawing.Point(188, 206);
            this.mHueKeyBox.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mHueKeyBox.Name = "mHueKeyBox";
            this.mHueKeyBox.Size = new System.Drawing.Size(44, 16);
            this.mHueKeyBox.TabIndex = 33;
            this.mHueKeyBox.Text = "Key";
            this.mHueKeyBox.UseVisualStyleBackColor = true;
            this.mHueKeyBox.CheckedChanged += new System.EventHandler(this.mHueKeyBox_CheckedChanged);
            // 
            // mHueBox
            // 
            this.mHueBox.DecimalPlaces = 2;
            this.mHueBox.Increment = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.mHueBox.Location = new System.Drawing.Point(96, 203);
            this.mHueBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.mHueBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.mHueBox.Name = "mHueBox";
            this.mHueBox.Size = new System.Drawing.Size(86, 20);
            this.mHueBox.TabIndex = 28;
            this.mHueBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mHueBox.ValueChanged += new System.EventHandler(this.mHueBox_ValueChanged);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 180);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Map Zoom:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 155);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "LongitudeOffset:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 105);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Compass Offset:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Scale:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Rotation:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "X Trans:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 80);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Y Trans:";
            // 
            // mScaleBox
            // 
            this.mScaleBox.DecimalPlaces = 5;
            this.mScaleBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mScaleBox.Location = new System.Drawing.Point(96, 3);
            this.mScaleBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.mScaleBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.mScaleBox.Name = "mScaleBox";
            this.mScaleBox.Size = new System.Drawing.Size(86, 20);
            this.mScaleBox.TabIndex = 4;
            this.mScaleBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mScaleBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.mScaleBox.ValueChanged += new System.EventHandler(this.mScaleBox_ValueChanged);
            // 
            // mRotationBox
            // 
            this.mRotationBox.DecimalPlaces = 2;
            this.mRotationBox.Location = new System.Drawing.Point(96, 28);
            this.mRotationBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.mRotationBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.mRotationBox.Name = "mRotationBox";
            this.mRotationBox.Size = new System.Drawing.Size(86, 20);
            this.mRotationBox.TabIndex = 5;
            this.mRotationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mRotationBox.ValueChanged += new System.EventHandler(this.mRotationBox_ValueChanged);
            // 
            // mXTranslationBox
            // 
            this.mXTranslationBox.Location = new System.Drawing.Point(96, 53);
            this.mXTranslationBox.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.mXTranslationBox.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.mXTranslationBox.Name = "mXTranslationBox";
            this.mXTranslationBox.Size = new System.Drawing.Size(86, 20);
            this.mXTranslationBox.TabIndex = 6;
            this.mXTranslationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mXTranslationBox.ValueChanged += new System.EventHandler(this.mXTranslationBox_ValueChanged);
            // 
            // mYTranslationBox
            // 
            this.mYTranslationBox.Location = new System.Drawing.Point(96, 78);
            this.mYTranslationBox.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.mYTranslationBox.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.mYTranslationBox.Name = "mYTranslationBox";
            this.mYTranslationBox.Size = new System.Drawing.Size(86, 20);
            this.mYTranslationBox.TabIndex = 7;
            this.mYTranslationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mYTranslationBox.ValueChanged += new System.EventHandler(this.mYTranslationBox_ValueChanged);
            // 
            // mScaleKeyButton
            // 
            this.mScaleKeyButton.AutoSize = true;
            this.mScaleKeyButton.Location = new System.Drawing.Point(188, 6);
            this.mScaleKeyButton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mScaleKeyButton.Name = "mScaleKeyButton";
            this.mScaleKeyButton.Size = new System.Drawing.Size(44, 16);
            this.mScaleKeyButton.TabIndex = 8;
            this.mScaleKeyButton.Text = "Key";
            this.mScaleKeyButton.UseVisualStyleBackColor = true;
            this.mScaleKeyButton.CheckedChanged += new System.EventHandler(this.mScaleKeyButton_CheckedChanged);
            // 
            // mRotationKeyBotton
            // 
            this.mRotationKeyBotton.AutoSize = true;
            this.mRotationKeyBotton.Location = new System.Drawing.Point(188, 31);
            this.mRotationKeyBotton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mRotationKeyBotton.Name = "mRotationKeyBotton";
            this.mRotationKeyBotton.Size = new System.Drawing.Size(44, 16);
            this.mRotationKeyBotton.TabIndex = 9;
            this.mRotationKeyBotton.Text = "Key";
            this.mRotationKeyBotton.UseVisualStyleBackColor = true;
            this.mRotationKeyBotton.CheckedChanged += new System.EventHandler(this.mRotationKeyBotton_CheckedChanged);
            // 
            // mTranslationKeyButton
            // 
            this.mTranslationKeyButton.AutoSize = true;
            this.mTranslationKeyButton.Location = new System.Drawing.Point(188, 68);
            this.mTranslationKeyButton.Margin = new System.Windows.Forms.Padding(3, 18, 3, 3);
            this.mTranslationKeyButton.Name = "mTranslationKeyButton";
            this.tableLayoutPanel1.SetRowSpan(this.mTranslationKeyButton, 2);
            this.mTranslationKeyButton.Size = new System.Drawing.Size(44, 17);
            this.mTranslationKeyButton.TabIndex = 10;
            this.mTranslationKeyButton.Text = "Key";
            this.mTranslationKeyButton.UseVisualStyleBackColor = true;
            this.mTranslationKeyButton.CheckedChanged += new System.EventHandler(this.mTranslationKeyButton_CheckedChanged);
            // 
            // mCompassOffsetBox
            // 
            this.mCompassOffsetBox.DecimalPlaces = 2;
            this.mCompassOffsetBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mCompassOffsetBox.Location = new System.Drawing.Point(96, 103);
            this.mCompassOffsetBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.mCompassOffsetBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.mCompassOffsetBox.Name = "mCompassOffsetBox";
            this.mCompassOffsetBox.Size = new System.Drawing.Size(86, 20);
            this.mCompassOffsetBox.TabIndex = 12;
            this.mCompassOffsetBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mCompassOffsetBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.mCompassOffsetBox.ValueChanged += new System.EventHandler(this.mCompassOffsetBox_ValueChanged);
            // 
            // mCompassKeyButton
            // 
            this.mCompassKeyButton.AutoSize = true;
            this.mCompassKeyButton.Location = new System.Drawing.Point(188, 106);
            this.mCompassKeyButton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mCompassKeyButton.Name = "mCompassKeyButton";
            this.mCompassKeyButton.Size = new System.Drawing.Size(44, 16);
            this.mCompassKeyButton.TabIndex = 13;
            this.mCompassKeyButton.Text = "Key";
            this.mCompassKeyButton.UseVisualStyleBackColor = true;
            this.mCompassKeyButton.CheckedChanged += new System.EventHandler(this.mCompassKeyButton_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 130);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Latitude Offset:";
            // 
            // mLatitudeOffsetBox
            // 
            this.mLatitudeOffsetBox.DecimalPlaces = 2;
            this.mLatitudeOffsetBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mLatitudeOffsetBox.Location = new System.Drawing.Point(96, 128);
            this.mLatitudeOffsetBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.mLatitudeOffsetBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.mLatitudeOffsetBox.Name = "mLatitudeOffsetBox";
            this.mLatitudeOffsetBox.Size = new System.Drawing.Size(86, 20);
            this.mLatitudeOffsetBox.TabIndex = 16;
            this.mLatitudeOffsetBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mLatitudeOffsetBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.mLatitudeOffsetBox.ValueChanged += new System.EventHandler(this.mLatitudeOffsetBox_ValueChanged);
            // 
            // mLongitudeOffsetBox
            // 
            this.mLongitudeOffsetBox.DecimalPlaces = 2;
            this.mLongitudeOffsetBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.mLongitudeOffsetBox.Location = new System.Drawing.Point(96, 153);
            this.mLongitudeOffsetBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.mLongitudeOffsetBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.mLongitudeOffsetBox.Name = "mLongitudeOffsetBox";
            this.mLongitudeOffsetBox.Size = new System.Drawing.Size(86, 20);
            this.mLongitudeOffsetBox.TabIndex = 17;
            this.mLongitudeOffsetBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mLongitudeOffsetBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.mLongitudeOffsetBox.ValueChanged += new System.EventHandler(this.mLongitudeOffsetBox_ValueChanged);
            // 
            // mLatitudeKeyButton
            // 
            this.mLatitudeKeyButton.AutoSize = true;
            this.mLatitudeKeyButton.Location = new System.Drawing.Point(188, 131);
            this.mLatitudeKeyButton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mLatitudeKeyButton.Name = "mLatitudeKeyButton";
            this.mLatitudeKeyButton.Size = new System.Drawing.Size(44, 16);
            this.mLatitudeKeyButton.TabIndex = 18;
            this.mLatitudeKeyButton.Text = "Key";
            this.mLatitudeKeyButton.UseVisualStyleBackColor = true;
            this.mLatitudeKeyButton.CheckedChanged += new System.EventHandler(this.mLatitudeKeyButton_CheckedChanged);
            // 
            // mLongitudeKeyButton
            // 
            this.mLongitudeKeyButton.AutoSize = true;
            this.mLongitudeKeyButton.Location = new System.Drawing.Point(188, 156);
            this.mLongitudeKeyButton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mLongitudeKeyButton.Name = "mLongitudeKeyButton";
            this.mLongitudeKeyButton.Size = new System.Drawing.Size(44, 16);
            this.mLongitudeKeyButton.TabIndex = 19;
            this.mLongitudeKeyButton.Text = "Key";
            this.mLongitudeKeyButton.UseVisualStyleBackColor = true;
            this.mLongitudeKeyButton.CheckedChanged += new System.EventHandler(this.mLongitudeKeyButton_CheckedChanged);
            // 
            // mMapZoomKeyBox
            // 
            this.mMapZoomKeyBox.AutoSize = true;
            this.mMapZoomKeyBox.Location = new System.Drawing.Point(188, 181);
            this.mMapZoomKeyBox.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.mMapZoomKeyBox.Name = "mMapZoomKeyBox";
            this.mMapZoomKeyBox.Size = new System.Drawing.Size(44, 16);
            this.mMapZoomKeyBox.TabIndex = 22;
            this.mMapZoomKeyBox.Text = "Key";
            this.mMapZoomKeyBox.UseVisualStyleBackColor = true;
            this.mMapZoomKeyBox.CheckedChanged += new System.EventHandler(this.mMapZoomKeyBox_CheckedChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(60, 205);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Hue:";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 230);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Saturation:";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(53, 255);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "Value:";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(44, 280);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(46, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "Gamma:";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(41, 305);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 27;
            this.label20.Text = "Contrast:";
            // 
            // mSaturationBox
            // 
            this.mSaturationBox.DecimalPlaces = 2;
            this.mSaturationBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mSaturationBox.Location = new System.Drawing.Point(96, 228);
            this.mSaturationBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mSaturationBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.mSaturationBox.Name = "mSaturationBox";
            this.mSaturationBox.Size = new System.Drawing.Size(86, 20);
            this.mSaturationBox.TabIndex = 29;
            this.mSaturationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mSaturationBox.ValueChanged += new System.EventHandler(this.mSaturationBox_ValueChanged);
            // 
            // mValueBox
            // 
            this.mValueBox.DecimalPlaces = 2;
            this.mValueBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mValueBox.Location = new System.Drawing.Point(96, 253);
            this.mValueBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mValueBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.mValueBox.Name = "mValueBox";
            this.mValueBox.Size = new System.Drawing.Size(86, 20);
            this.mValueBox.TabIndex = 30;
            this.mValueBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mValueBox.ValueChanged += new System.EventHandler(this.mValueBox_ValueChanged);
            // 
            // mGammaBox
            // 
            this.mGammaBox.DecimalPlaces = 2;
            this.mGammaBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.mGammaBox.Location = new System.Drawing.Point(96, 278);
            this.mGammaBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mGammaBox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.mGammaBox.Name = "mGammaBox";
            this.mGammaBox.Size = new System.Drawing.Size(86, 20);
            this.mGammaBox.TabIndex = 31;
            this.mGammaBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mGammaBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mGammaBox.ValueChanged += new System.EventHandler(this.mGammaBox_ValueChanged);
            // 
            // mContrastBox
            // 
            this.mContrastBox.DecimalPlaces = 2;
            this.mContrastBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mContrastBox.Location = new System.Drawing.Point(96, 303);
            this.mContrastBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mContrastBox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.mContrastBox.Name = "mContrastBox";
            this.mContrastBox.Size = new System.Drawing.Size(86, 20);
            this.mContrastBox.TabIndex = 32;
            this.mContrastBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mContrastBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mContrastBox.ValueChanged += new System.EventHandler(this.mContrastBox_ValueChanged);
            // 
            // mMapZoomBox
            // 
            this.mMapZoomBox.DecimalPlaces = 2;
            this.mMapZoomBox.Location = new System.Drawing.Point(96, 178);
            this.mMapZoomBox.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.mMapZoomBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mMapZoomBox.Name = "mMapZoomBox";
            this.mMapZoomBox.Size = new System.Drawing.Size(86, 20);
            this.mMapZoomBox.TabIndex = 21;
            this.mMapZoomBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mMapZoomBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mMapZoomBox.ValueChanged += new System.EventHandler(this.mMapZoomBox_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mSequenceListBox);
            this.groupBox1.Location = new System.Drawing.Point(0, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(179, 346);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sequences";
            // 
            // mSequenceListBox
            // 
            this.mSequenceListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mSequenceListBox.FormattingEnabled = true;
            this.mSequenceListBox.Location = new System.Drawing.Point(3, 16);
            this.mSequenceListBox.Name = "mSequenceListBox";
            this.mSequenceListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.mSequenceListBox.Size = new System.Drawing.Size(173, 327);
            this.mSequenceListBox.TabIndex = 0;
            this.mSequenceListBox.SelectedIndexChanged += new System.EventHandler(this.mSequenceListBox_SelectedIndexChanged);
            // 
            // mShowOverlayCheckBox
            // 
            this.mShowOverlayCheckBox.AutoSize = true;
            this.mShowOverlayCheckBox.Location = new System.Drawing.Point(611, 230);
            this.mShowOverlayCheckBox.Name = "mShowOverlayCheckBox";
            this.mShowOverlayCheckBox.Size = new System.Drawing.Size(97, 17);
            this.mShowOverlayCheckBox.TabIndex = 5;
            this.mShowOverlayCheckBox.Text = "Show Overlays";
            this.mShowOverlayCheckBox.UseVisualStyleBackColor = true;
            this.mShowOverlayCheckBox.CheckedChanged += new System.EventHandler(this.mShowOverlayCheckBox_CheckedChanged);
            // 
            // mMapListBox
            // 
            this.mMapListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mMapListBox.FormattingEnabled = true;
            this.mMapListBox.Location = new System.Drawing.Point(6, 20);
            this.mMapListBox.Name = "mMapListBox";
            this.mMapListBox.Size = new System.Drawing.Size(160, 229);
            this.mMapListBox.TabIndex = 7;
            this.mMapListBox.SelectedIndexChanged += new System.EventHandler(this.mMapListBox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mMapListBox);
            this.groupBox2.Location = new System.Drawing.Point(185, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 257);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Maps";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.mLabelCarryoverButton);
            this.groupBox3.Controls.Add(this.mTextOverlayListBox);
            this.groupBox3.Controls.Add(this.mDeleteTextOverlayButton);
            this.groupBox3.Controls.Add(this.mAddTextOverlayButton);
            this.groupBox3.Controls.Add(this.mColorButton);
            this.groupBox3.Controls.Add(this.mTextOverlayYBox);
            this.groupBox3.Controls.Add(this.mTextOverlayXBox);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.mFontButton);
            this.groupBox3.Controls.Add(this.mTextOverlayBox);
            this.groupBox3.Location = new System.Drawing.Point(3, 379);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(720, 174);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Labels";
            // 
            // mLabelCarryoverButton
            // 
            this.mLabelCarryoverButton.AutoSize = true;
            this.mLabelCarryoverButton.Location = new System.Drawing.Point(360, 146);
            this.mLabelCarryoverButton.Name = "mLabelCarryoverButton";
            this.mLabelCarryoverButton.Size = new System.Drawing.Size(100, 17);
            this.mLabelCarryoverButton.TabIndex = 21;
            this.mLabelCarryoverButton.Text = "Label Carryover";
            this.mLabelCarryoverButton.UseVisualStyleBackColor = true;
            // 
            // mTextOverlayListBox
            // 
            this.mTextOverlayListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mTextOverlayListBox.FormattingEnabled = true;
            this.mTextOverlayListBox.Location = new System.Drawing.Point(9, 19);
            this.mTextOverlayListBox.Name = "mTextOverlayListBox";
            this.mTextOverlayListBox.Size = new System.Drawing.Size(157, 109);
            this.mTextOverlayListBox.TabIndex = 17;
            this.mTextOverlayListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.mTextOverlayListBox_ItemCheck);
            this.mTextOverlayListBox.SelectedIndexChanged += new System.EventHandler(this.mTextOverlayListBox_SelectedIndexChanged);
            // 
            // mDeleteTextOverlayButton
            // 
            this.mDeleteTextOverlayButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mDeleteTextOverlayButton.Enabled = false;
            this.mDeleteTextOverlayButton.Location = new System.Drawing.Point(90, 142);
            this.mDeleteTextOverlayButton.Name = "mDeleteTextOverlayButton";
            this.mDeleteTextOverlayButton.Size = new System.Drawing.Size(75, 23);
            this.mDeleteTextOverlayButton.TabIndex = 13;
            this.mDeleteTextOverlayButton.Text = "Delete";
            this.mDeleteTextOverlayButton.UseVisualStyleBackColor = true;
            this.mDeleteTextOverlayButton.Click += new System.EventHandler(this.mDeleteTextOverlayButton_Click);
            // 
            // mAddTextOverlayButton
            // 
            this.mAddTextOverlayButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mAddTextOverlayButton.Location = new System.Drawing.Point(9, 142);
            this.mAddTextOverlayButton.Name = "mAddTextOverlayButton";
            this.mAddTextOverlayButton.Size = new System.Drawing.Size(75, 23);
            this.mAddTextOverlayButton.TabIndex = 11;
            this.mAddTextOverlayButton.Text = "Add New";
            this.mAddTextOverlayButton.UseVisualStyleBackColor = true;
            this.mAddTextOverlayButton.Click += new System.EventHandler(this.mAddTextOverlayButton_Click);
            // 
            // mColorButton
            // 
            this.mColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mColorButton.Enabled = false;
            this.mColorButton.Location = new System.Drawing.Point(252, 142);
            this.mColorButton.Name = "mColorButton";
            this.mColorButton.Size = new System.Drawing.Size(75, 23);
            this.mColorButton.TabIndex = 20;
            this.mColorButton.Text = "Color...";
            this.mColorButton.UseVisualStyleBackColor = true;
            this.mColorButton.Click += new System.EventHandler(this.mColorButton_Click);
            // 
            // mTextOverlayYBox
            // 
            this.mTextOverlayYBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mTextOverlayYBox.Enabled = false;
            this.mTextOverlayYBox.Location = new System.Drawing.Point(643, 145);
            this.mTextOverlayYBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mTextOverlayYBox.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.mTextOverlayYBox.Name = "mTextOverlayYBox";
            this.mTextOverlayYBox.Size = new System.Drawing.Size(70, 20);
            this.mTextOverlayYBox.TabIndex = 19;
            this.mTextOverlayYBox.ValueChanged += new System.EventHandler(this.mTextOverlayYBox_ValueChanged);
            // 
            // mTextOverlayXBox
            // 
            this.mTextOverlayXBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mTextOverlayXBox.Enabled = false;
            this.mTextOverlayXBox.Location = new System.Drawing.Point(541, 145);
            this.mTextOverlayXBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mTextOverlayXBox.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.mTextOverlayXBox.Name = "mTextOverlayXBox";
            this.mTextOverlayXBox.Size = new System.Drawing.Size(73, 20);
            this.mTextOverlayXBox.TabIndex = 18;
            this.mTextOverlayXBox.ValueChanged += new System.EventHandler(this.mTextOverlayXBox_ValueChanged);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(620, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Y:";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(518, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "X:";
            // 
            // mFontButton
            // 
            this.mFontButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mFontButton.Enabled = false;
            this.mFontButton.Location = new System.Drawing.Point(171, 142);
            this.mFontButton.Name = "mFontButton";
            this.mFontButton.Size = new System.Drawing.Size(75, 23);
            this.mFontButton.TabIndex = 15;
            this.mFontButton.Text = "Font...";
            this.mFontButton.UseVisualStyleBackColor = true;
            this.mFontButton.Click += new System.EventHandler(this.mFontButton_Click);
            // 
            // mTextOverlayBox
            // 
            this.mTextOverlayBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mTextOverlayBox.Enabled = false;
            this.mTextOverlayBox.Location = new System.Drawing.Point(172, 19);
            this.mTextOverlayBox.Multiline = true;
            this.mTextOverlayBox.Name = "mTextOverlayBox";
            this.mTextOverlayBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.mTextOverlayBox.Size = new System.Drawing.Size(542, 117);
            this.mTextOverlayBox.TabIndex = 14;
            this.mTextOverlayBox.WordWrap = false;
            this.mTextOverlayBox.TextChanged += new System.EventHandler(this.mTextOverlayBox_TextChanged);
            // 
            // mForceCarryoverButton
            // 
            this.mForceCarryoverButton.AutoSize = true;
            this.mForceCarryoverButton.Location = new System.Drawing.Point(611, 253);
            this.mForceCarryoverButton.Name = "mForceCarryoverButton";
            this.mForceCarryoverButton.Size = new System.Drawing.Size(101, 17);
            this.mForceCarryoverButton.TabIndex = 10;
            this.mForceCarryoverButton.Text = "Force Carryover";
            this.mForceCarryoverButton.UseVisualStyleBackColor = true;
            // 
            // mFrameMultipleBox
            // 
            this.mFrameMultipleBox.Location = new System.Drawing.Point(648, 204);
            this.mFrameMultipleBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mFrameMultipleBox.Name = "mFrameMultipleBox";
            this.mFrameMultipleBox.Size = new System.Drawing.Size(77, 20);
            this.mFrameMultipleBox.TabIndex = 11;
            this.mFrameMultipleBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mFrameMultipleBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mFrameMultipleBox.ValueChanged += new System.EventHandler(this.mFrameMultipleBox_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(613, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Frame Multiple:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.mAccumulatedJiggleLabel);
            this.groupBox4.Controls.Add(this.mJiggleAngleBox);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.mJiggleYBox);
            this.groupBox4.Controls.Add(this.mJiggleXBox);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Location = new System.Drawing.Point(607, 54);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(118, 131);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Jiggle Offset";
            // 
            // mAccumulatedJiggleLabel
            // 
            this.mAccumulatedJiggleLabel.AutoSize = true;
            this.mAccumulatedJiggleLabel.Location = new System.Drawing.Point(1, 104);
            this.mAccumulatedJiggleLabel.Name = "mAccumulatedJiggleLabel";
            this.mAccumulatedJiggleLabel.Size = new System.Drawing.Size(48, 13);
            this.mAccumulatedJiggleLabel.TabIndex = 6;
            this.mAccumulatedJiggleLabel.Text = "Acc. JO:";
            // 
            // mJiggleAngleBox
            // 
            this.mJiggleAngleBox.DecimalPlaces = 2;
            this.mJiggleAngleBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.mJiggleAngleBox.Location = new System.Drawing.Point(43, 68);
            this.mJiggleAngleBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.mJiggleAngleBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.mJiggleAngleBox.Name = "mJiggleAngleBox";
            this.mJiggleAngleBox.Size = new System.Drawing.Size(55, 20);
            this.mJiggleAngleBox.TabIndex = 5;
            this.mJiggleAngleBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mJiggleAngleBox.ValueChanged += new System.EventHandler(this.mJiggleAngleBox_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 70);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Angle:";
            // 
            // mJiggleYBox
            // 
            this.mJiggleYBox.DecimalPlaces = 2;
            this.mJiggleYBox.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mJiggleYBox.Location = new System.Drawing.Point(43, 44);
            this.mJiggleYBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mJiggleYBox.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.mJiggleYBox.Name = "mJiggleYBox";
            this.mJiggleYBox.Size = new System.Drawing.Size(54, 20);
            this.mJiggleYBox.TabIndex = 3;
            this.mJiggleYBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mJiggleYBox.ValueChanged += new System.EventHandler(this.mJiggleYBox_ValueChanged);
            // 
            // mJiggleXBox
            // 
            this.mJiggleXBox.DecimalPlaces = 2;
            this.mJiggleXBox.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mJiggleXBox.Location = new System.Drawing.Point(43, 18);
            this.mJiggleXBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mJiggleXBox.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.mJiggleXBox.Name = "mJiggleXBox";
            this.mJiggleXBox.Size = new System.Drawing.Size(54, 20);
            this.mJiggleXBox.TabIndex = 2;
            this.mJiggleXBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mJiggleXBox.ValueChanged += new System.EventHandler(this.mJiggleXBox_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 46);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Y:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(20, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "X:";
            // 
            // mCompassModeBox
            // 
            this.mCompassModeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mCompassModeBox.Location = new System.Drawing.Point(608, 281);
            this.mCompassModeBox.Name = "mCompassModeBox";
            this.mCompassModeBox.Size = new System.Drawing.Size(101, 21);
            this.mCompassModeBox.TabIndex = 14;
            this.mCompassModeBox.SelectedIndexChanged += new System.EventHandler(this.mCompassModeBox_SelectedIndexChanged);
            // 
            // mNextKeyButton
            // 
            this.mNextKeyButton.Location = new System.Drawing.Point(458, 24);
            this.mNextKeyButton.Name = "mNextKeyButton";
            this.mNextKeyButton.Size = new System.Drawing.Size(87, 23);
            this.mNextKeyButton.TabIndex = 15;
            this.mNextKeyButton.Text = "Next Keyframe";
            this.mNextKeyButton.UseVisualStyleBackColor = true;
            this.mNextKeyButton.Click += new System.EventHandler(this.mNextKeyButton_Click);
            // 
            // mPrevKeyButton
            // 
            this.mPrevKeyButton.Location = new System.Drawing.Point(363, 24);
            this.mPrevKeyButton.Name = "mPrevKeyButton";
            this.mPrevKeyButton.Size = new System.Drawing.Size(89, 23);
            this.mPrevKeyButton.TabIndex = 16;
            this.mPrevKeyButton.Text = "Prev Keyframe";
            this.mPrevKeyButton.UseVisualStyleBackColor = true;
            this.mPrevKeyButton.Click += new System.EventHandler(this.mPrevKeyButton_Click);
            // 
            // mMapCarryoverButton
            // 
            this.mMapCarryoverButton.AutoSize = true;
            this.mMapCarryoverButton.Location = new System.Drawing.Point(191, 284);
            this.mMapCarryoverButton.Name = "mMapCarryoverButton";
            this.mMapCarryoverButton.Size = new System.Drawing.Size(95, 17);
            this.mMapCarryoverButton.TabIndex = 17;
            this.mMapCarryoverButton.Text = "Map Carryover";
            this.mMapCarryoverButton.UseVisualStyleBackColor = true;
            // 
            // mFileIndexWidget
            // 
            this.mFileIndexWidget.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mFileIndexWidget.Location = new System.Drawing.Point(608, 27);
            this.mFileIndexWidget.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.mFileIndexWidget.Name = "mFileIndexWidget";
            this.mFileIndexWidget.Size = new System.Drawing.Size(120, 20);
            this.mFileIndexWidget.TabIndex = 1;
            this.mFileIndexWidget.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mFileIndexWidget.ValueChanged += new System.EventHandler(this.mFileIndexWidget_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(608, 309);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "Delete Frame";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.DeleteFrameButton_Click);
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(728, 565);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.mMapCarryoverButton);
            this.Controls.Add(this.mPrevKeyButton);
            this.Controls.Add(this.mNextKeyButton);
            this.Controls.Add(this.mCompassModeBox);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.mFrameMultipleBox);
            this.Controls.Add(this.mForceCarryoverButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.mShowOverlayCheckBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mFileIndexWidget);
            this.Controls.Add(this.mMainMenuStrip);
            this.MainMenuStrip = this.mMainMenuStrip;
            this.Name = "ControlPanel";
            this.Text = "Timelapse Editor";
            this.mMainMenuStrip.ResumeLayout(false);
            this.mMainMenuStrip.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mHueBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mScaleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mRotationBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mXTranslationBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mYTranslationBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCompassOffsetBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLatitudeOffsetBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mLongitudeOffsetBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mSaturationBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mValueBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mGammaBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mContrastBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mMapZoomBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mTextOverlayYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mTextOverlayXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFrameMultipleBox)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mJiggleAngleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mJiggleYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mJiggleXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFileIndexWidget)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mMainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem mProjectMenu;
        private System.Windows.Forms.ToolStripMenuItem mNewProjectButton;
        private System.Windows.Forms.ToolStripMenuItem mOpenProjectButton;
        private System.Windows.Forms.ToolStripMenuItem mProjectPropertiesButton;
        private Controls.VariableStepNumericUpDown mFileIndexWidget;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem mInputFilesButton;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown mScaleBox;
        private System.Windows.Forms.NumericUpDown mRotationBox;
        private System.Windows.Forms.NumericUpDown mXTranslationBox;
        private System.Windows.Forms.NumericUpDown mYTranslationBox;
        private System.Windows.Forms.CheckBox mScaleKeyButton;
        private System.Windows.Forms.CheckBox mRotationKeyBotton;
        private System.Windows.Forms.CheckBox mTranslationKeyButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox mSequenceListBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown mCompassOffsetBox;
        private System.Windows.Forms.CheckBox mCompassKeyButton;
        private System.Windows.Forms.CheckBox mShowOverlayCheckBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown mLatitudeOffsetBox;
        private System.Windows.Forms.NumericUpDown mLongitudeOffsetBox;
        private System.Windows.Forms.CheckBox mLatitudeKeyButton;
        private System.Windows.Forms.CheckBox mLongitudeKeyButton;
        private System.Windows.Forms.NumericUpDown mMapZoomBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox mMapZoomKeyBox;
        private System.Windows.Forms.CheckedListBox mMapListBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button mDeleteTextOverlayButton;
        private System.Windows.Forms.Button mAddTextOverlayButton;
        private System.Windows.Forms.Button mColorButton;
        private System.Windows.Forms.NumericUpDown mTextOverlayYBox;
        private System.Windows.Forms.NumericUpDown mTextOverlayXBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button mFontButton;
        private System.Windows.Forms.TextBox mTextOverlayBox;
        private System.Windows.Forms.ToolStripMenuItem renderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateMapScriptsToolStripMenuItem;
        private System.Windows.Forms.CheckBox mForceCarryoverButton;
        private System.Windows.Forms.NumericUpDown mFrameMultipleBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripMenuItem exportSequenceToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown mJiggleYBox;
        private System.Windows.Forms.NumericUpDown mJiggleXBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown mJiggleAngleBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox mCompassModeBox;
        private System.Windows.Forms.NumericUpDown mHueBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown mSaturationBox;
        private System.Windows.Forms.NumericUpDown mValueBox;
        private System.Windows.Forms.NumericUpDown mGammaBox;
        private System.Windows.Forms.NumericUpDown mContrastBox;
        private System.Windows.Forms.CheckBox mContrastKeyBox;
        private System.Windows.Forms.CheckBox mGammaKeyBox;
        private System.Windows.Forms.CheckBox mValueKeyBox;
        private System.Windows.Forms.CheckBox mSaturationKeyBox;
        private System.Windows.Forms.CheckBox mHueKeyBox;
        private System.Windows.Forms.Button mNextKeyButton;
        private System.Windows.Forms.Button mPrevKeyButton;
        private System.Windows.Forms.CheckedListBox mTextOverlayListBox;
        private System.Windows.Forms.CheckBox mLabelCarryoverButton;
        private System.Windows.Forms.CheckBox mMapCarryoverButton;
        private System.Windows.Forms.ToolStripMenuItem autoregisterToolStripMenuItem;
        private System.Windows.Forms.Label mAccumulatedJiggleLabel;
        private System.Windows.Forms.ToolStripMenuItem removeBlackspaceToolStripMenuItem;
        private System.Windows.Forms.Button button1;
    }
}

