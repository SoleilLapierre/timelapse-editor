﻿// Copyright (c) 2015 by Soleil Lapierre.

using ManagedCowTools.Math;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;

namespace TimeLapseEditor
{
    public partial class ControlPanel : Form
    {
        #region ----- Data members -----

        private PreviewPanel mPreviewWindow;
        private bool mSuspendEvents = false;
        private Dictionary<uint, int> mSequenceIndices = null;
        private Frame mCurrentFrame = null;

        #endregion
        #region ---- Initialization and teardown -----

        public ControlPanel()
        {
            InitializeComponent();

            mPreviewWindow = new PreviewPanel(this);
            mPreviewWindow.Show();

            mCompassModeBox.Items.Clear();
            foreach (object o in Enum.GetValues(typeof(Frame.CompassUpdateMode)))
            {
                mCompassModeBox.Items.Add(o);
            }
        }

        
        protected override void OnClosing(CancelEventArgs e)
        {
            if (CheckCurrentProjectSaved())
            {
                base.OnClosing(e);
                Properties.Settings.Default.Save();
            }
            else
            {
                e.Cancel = true;
            }
        }


        private bool CheckCurrentProjectSaved()
        {
            bool proceed = true;
            if (!Project.IsCurrentProjectSaved())
            {
                DialogResult dr = MessageBox.Show("Save current project first?", "Project not saved", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    try
                    {
                        Project.SaveCurrentProject();
                    }
                    catch (System.IO.IOException e)
                    {
                        MessageBox.Show(string.Format("Could not save project. Error message:\n{0}", e.Message), "Error");
                        proceed = false;
                    }
                }

                if (dr == DialogResult.Cancel)
                {
                    proceed = false;
                }
            }

            return proceed;
        }

        #endregion
        #region ----- Public methods and properties -----

        public int CurrentFrame
        {
            get { return (int)mFileIndexWidget.Value; }
        }


        public void UpdateJiggleValues()
        {
            Frame f = Project.Instance.GetFrame(CurrentFrame);
            if (f == null)
            {
                return;
            }

            SuspendLayout();
            mSuspendEvents = true;

            mJiggleXBox.Value = (decimal)f.JiggleCorrection.X;
            mJiggleYBox.Value = (decimal)f.JiggleCorrection.Y;
            mJiggleAngleBox.Value = (decimal)f.AngleCorrection;
            mAccumulatedJiggleLabel.Text = String.Format("AJ: {0:N2}, {1:N2}", f.AccumulatedJiggleCorrection.X, f.AccumulatedJiggleCorrection.Y);

            mSuspendEvents = false;
            ResumeLayout();
        }


        public void ShowImage(int aIndex)
        {
            int oldIndex = Project.Instance.LastSelectedImage;
            int numFrames = Project.Instance.GetNumFrames();
            aIndex = Math.Max(0, Math.Min(aIndex, numFrames - 1));
            Project.Instance.LastSelectedImage = aIndex;

            bool enableControls = true;
            bool showOverlays = true;
            ManagedCowTools.Math.Vector2 translation = ManagedCowTools.Math.Vector2.Zero;
            bool transKeyed = false;
            int frameMultiple = 1;
            Vector2 jiggle = Vector2.Zero;
            float jiggleA = 0.0f;
            Vector2 accJiggle = Vector2.Zero;
            Frame.CompassUpdateMode compassMode = Frame.CompassUpdateMode.Update;

            List<uint> selectedSequences = new List<uint>();

            if ((aIndex >= 0) && (aIndex < numFrames))
            {
                Frame f = Project.Instance.GetFrame(aIndex);

                // Auto-clone some current control selections when moving to a new frame.
                if ((mCurrentFrame != null) && (oldIndex == (aIndex - 1)))
                {
                    f.CarryOverPropertiesFrom(mCurrentFrame, mForceCarryoverButton.Checked, mMapCarryoverButton.Checked, mLabelCarryoverButton.Checked);
                    Project.Instance.IsSaved = false;
                }

                mPreviewWindow.DisplayImage(aIndex, f);
                double indexTime = (double)aIndex;
                translation = Project.Instance.TranslationTimeline[indexTime];
                transKeyed = Project.Instance.TranslationTimeline.IsKeyAt(indexTime);
                frameMultiple = f.FrameMultiple;
                jiggle = f.JiggleCorrection;
                jiggleA = f.AngleCorrection;
                accJiggle = f.AccumulatedJiggleCorrection;
                compassMode = f.CompassMode;

                showOverlays = f.ShowOverlays;
                selectedSequences.AddRange(f.Sequences);
                mCurrentFrame = f;
            }
            else
            {
                mPreviewWindow.DisplayImage(aIndex, null);
                enableControls = false;
                mCurrentFrame = null;
            }

            SuspendLayout();
            mSuspendEvents = true;

            mShowOverlayCheckBox.Checked = showOverlays;
            mFileIndexWidget.Value = (decimal)aIndex;

            UpdateTimelineControls(aIndex, Project.Instance.ScaleTimeline, 1.0f, mScaleBox, mScaleKeyButton, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.RotationTimeline, 0.0f, mRotationBox, mRotationKeyBotton, enableControls);

            mXTranslationBox.Enabled = enableControls;
            mXTranslationBox.Value = (decimal)translation.X;
            mYTranslationBox.Enabled = enableControls;
            mYTranslationBox.Value = (decimal)translation.Y;
            mTranslationKeyButton.Enabled = enableControls;
            mTranslationKeyButton.Checked = transKeyed;

            UpdateTimelineControls(aIndex, Project.Instance.HeadingOffsetTimeline, 0.0f, mCompassOffsetBox, mCompassKeyButton, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.LatitudeOffsetTimeline, 0.0f, mLatitudeOffsetBox, mLatitudeKeyButton, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.LongitudeOffsetTimeline, 0.0f, mLongitudeOffsetBox, mLongitudeKeyButton, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.MapZoomTimeline, 15.0f, mMapZoomBox, mMapZoomKeyBox, enableControls);

            UpdateTimelineControls(aIndex, Project.Instance.HueTimeline, 0.0f, mHueBox, mHueKeyBox, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.SaturationTimeline, 0.0f, mSaturationBox, mSaturationKeyBox, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.BrightnessTimeline, 0.0f, mValueBox, mValueKeyBox, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.ContrastTimeline, 0.0f, mContrastBox, mContrastKeyBox, enableControls);
            UpdateTimelineControls(aIndex, Project.Instance.GammaTimeline, 1.0f, mGammaBox, mGammaKeyBox, enableControls);

            mFrameMultipleBox.Value = (decimal)frameMultiple;
            mJiggleXBox.Value = (decimal)jiggle.X;
            mJiggleYBox.Value = (decimal)jiggle.Y;
            mJiggleAngleBox.Value = (decimal)jiggleA;
            mAccumulatedJiggleLabel.Text = String.Format("AJ: {0:N2}, {1:N2}", accJiggle.X, accJiggle.Y);

            mCompassModeBox.SelectedItem = compassMode;

            mSequenceListBox.SelectedIndices.Clear();
            foreach (uint id in selectedSequences)
            {
                if (mSequenceIndices.ContainsKey(id))
                {
                    mSequenceListBox.SelectedIndices.Add(mSequenceIndices[id]);
                }
            }

            PopulateMapList();
            PopulateTextOverlays();

            mSuspendEvents = false;
            ResumeLayout();
        }


        public bool BatchRemoveBlackspace(int aIndex, bool aBackspread)
        {
            if (aIndex > 0)
            {
                Frame f = Project.Instance.GetFrame(aIndex);
                if (f.FrameMultiple < 1)
                {
                    return true;
                }

                Bitmap img = Bitmap.FromFile(f.SourcePath) as Bitmap;
                if (img == null)
                {
                    throw new FileLoadException(String.Format("Could not load {0}", f.SourcePath));
                }

                Vector2 correction = ConstrainEdges(aIndex, f, img);
                int numFrames = Project.Instance.GetNumFrames();

                // If we're not spreading the error backwards in time, just apply it to the current frame and be done.
                if (!aBackspread)
                {
                    f.JiggleCorrection += correction;
                    mPreviewWindow.InvalidateCachedImages(aIndex, numFrames - 1);
                    Project.Instance.UpdateAccumulatedProperties(aIndex);
                    return true;
                }

                // We are back-spreading the error; pick a target number of frames to spread if over (1 pixel per frame is ideal).
                float maxDelta = Math.Max(Math.Abs(correction.X), Math.Abs(correction.Y));
                int numFramesDesired = (int)Math.Ceiling(maxDelta); // 1 pixel per frame.
                int numFramesAvailable = 1;
                int i = aIndex;
                // Examine actual frame data to see how many are available.
                while (numFramesAvailable < numFramesDesired)
                {
                    if (i == 0) // Stop at start of time.
                    {
                        break;
                    }

                    --i;

                    Frame tempFrame = Project.Instance.GetFrame(i);
                    if (tempFrame.FrameMultiple < 1) // Start when we reach a jump cut.
                    {
                        break;
                    }

                    ++numFramesAvailable;
                }

                // Loop over all the frames between the run start and the current frame.
                while ((i <= aIndex) && (numFramesAvailable > 0))
                {
                    Frame frame = Project.Instance.GetFrame(i);

                    // Apply an even portion of the remaining error to this frame.
                    Vector2 desiredDelta = correction * 1.0f / (float)numFramesAvailable;
                    frame.JiggleCorrection += desiredDelta;
                    correction -= desiredDelta;

                    mPreviewWindow.InvalidateCachedImages(i, numFrames - 1);
                    Project.Instance.UpdateAccumulatedProperties(i);

                    // Constrain the result and add any new error to the remaining total.
                    img = Bitmap.FromFile(frame.SourcePath) as Bitmap;
                    if (img == null)
                    {
                        throw new FileLoadException(String.Format("Could not load {0}", frame.SourcePath));
                    }

                    Vector2 newConstraint = ConstrainEdges(i, frame, img);
                    frame.JiggleCorrection += newConstraint;
                    correction += newConstraint;

                    mPreviewWindow.InvalidateCachedImages(i, numFrames - 1);
                    Project.Instance.UpdateAccumulatedProperties(i);

                    ++i;
                    --numFramesAvailable;
                }

            }

            return true;
        }

        #endregion
        #region ----- UI callbacks -----
        #region -------- Menus ---------

        private void mNewProjectMenuItem_Click(object sender, EventArgs e)
        {
            if (CheckCurrentProjectSaved())
            {
                Project proj = new Project();
                if (ProjectSettingsForm.Show(proj) == DialogResult.OK)
                {
                    FolderBrowserDialog fd = new FolderBrowserDialog();
                    fd.ShowNewFolderButton = true;
                    if (fd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }

                    if (!Directory.Exists(fd.SelectedPath))
                    {
                        return;
                    }

                    proj.ProjectRoot = fd.SelectedPath;

                    Project.Instance = proj;

                    string toc = proj.IndexPath;
                    if (!Directory.Exists(proj.IndexPath))
                    {
                        Directory.CreateDirectory(proj.IndexPath);
                    }

                    proj.FrameStore.LoadIndex(proj.IndexPath);

                    Project.SaveCurrentProject();
                    ResetControls();
                }
            }
        }


        private void mOpenProjectMenuItem_Click(object sender, EventArgs args)
        {
            if (CheckCurrentProjectSaved())
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.CheckFileExists = true;
                fd.DefaultExt = "xml";
                fd.Multiselect = false;
                fd.Filter = "XML files (*.xml)|*.xml";
                fd.RestoreDirectory = false;
                if (!String.IsNullOrEmpty(Properties.Settings.Default.LastProjectPath))
                {
                    fd.InitialDirectory = Properties.Settings.Default.LastProjectPath;
                }

                if (fd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        if (Project.LoadCurrentProject(fd.FileName))
                        {
                            ResetControls();
                            string projPath = Path.GetDirectoryName(fd.FileName);
                            if (!projPath.Equals(Properties.Settings.Default.LastProjectPath))
                            {
                                Properties.Settings.Default.LastProjectPath = projPath;
                                Properties.Settings.Default.Save();
                            }
                        }
                    }
                    catch (System.IO.IOException e)
                    {
                        MessageBox.Show(string.Format("Could not load project. Error message:\n{0}", e.Message), "Error");
                    }
                }
            }
        }


        private void mProjectPropertiesMenuItem_Click(object sender, EventArgs e)
        {
            Project proj = new Project(Project.Instance);
            if (ProjectSettingsForm.Show(proj) == DialogResult.OK)
            {
                Project.Instance = proj;
                mPreviewWindow.InvalidateCachedImages();
            }
        }


        private void mInputFilesMenuItem_Click(object sender, EventArgs e)
        {
            FileListDialog dialog = new FileListDialog(Project.Instance);
            dialog.ShowDialog();
            dialog.Dispose();
        }


        private void mSaveProjectMenuItem_Click(object sender, EventArgs e)
        {
            Project.SaveCurrentProject();
        }


        private void mGenerateMapScriptsMenuItem_Click(object sender, EventArgs e)
        {
            (new GenerateMapScriptsDialog(Project.Instance)).ShowDialog();
        }


        private void mExportSequenceMenuItem_Click(object sender, EventArgs e)
        {
            (new ExportSequenceDialog(Project.Instance)).ShowDialog();
        }


        private void mAutoregisterMenuItem_Click(object sender, EventArgs e)
        {
            int currentFrame = (int)mFileIndexWidget.Value;
            bool regMode = mPreviewWindow.RegistrationMode;
            mPreviewWindow.RegistrationMode = true;

            using (BatchDialog dlg = new BatchDialog(Project.Instance, "Select range to autoregister", CurrentFrame, Math.Min(CurrentFrame + 100, Project.Instance.GetNumFrames() - 1)))
            {
                dlg.SetOptionName("Do blackspace removal");
                dlg.ProcessFrameCallback += new BatchDialog.WorkCallback(BatchAutoregister);
                dlg.FrameProgressCallback += new BatchDialog.ProgressCallback(BatchProgress);
                dlg.ShowDialog();
                dlg.ProcessFrameCallback -= new BatchDialog.WorkCallback(BatchAutoregister);
                dlg.FrameProgressCallback -= new BatchDialog.ProgressCallback(BatchProgress);
                mPreviewWindow.InvalidateCachedImages(dlg.Start, dlg.End);
            }

            mPreviewWindow.RegistrationMode = regMode;
            mFileIndexWidget.Value = (decimal)currentFrame;
            ShowImage(currentFrame);
        }


        private void mRemoveBlackspaceMenuItem_Click(object sender, EventArgs e)
        {
            int currentFrame = (int)mFileIndexWidget.Value;
            bool regMode = mPreviewWindow.RegistrationMode;
            mPreviewWindow.RegistrationMode = false;

            using (BatchDialog dlg = new BatchDialog(Project.Instance, "Select range to correct black space for", Math.Max(0, CurrentFrame - 100), CurrentFrame))
            {
                dlg.SetOptionName("Back-spread error");
                dlg.ProcessFrameCallback += new BatchDialog.WorkCallback(BatchRemoveBlackspace);
                dlg.ShowDialog();
                dlg.ProcessFrameCallback -= new BatchDialog.WorkCallback(BatchRemoveBlackspace);
                mPreviewWindow.InvalidateCachedImages(dlg.Start, dlg.End);
            }

            mPreviewWindow.RegistrationMode = regMode;
            mFileIndexWidget.Value = (decimal)currentFrame;
            ShowImage(currentFrame);
        }

        #endregion
        #region -------- Misc. controls -----

        private void mFileIndexWidget_ValueChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                ShowImage(CurrentFrame);
            }
        }


        private void mShowOverlayCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                if (mCurrentFrame != null)
                {
                    mCurrentFrame.ShowOverlays = mShowOverlayCheckBox.Checked;
                    Project.Instance.IsSaved = false;
                    ShowImage(CurrentFrame);
                }
            }
        }


        private void mAddTextOverlayButton_Click(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            TextOverlay ovl = new TextOverlay();

            TextOverlay selection = mTextOverlayListBox.SelectedItem as TextOverlay;
            int index = Project.Instance.TextOverlays.Count;
            if (selection != null)
            {
                ovl.Position = selection.Position;
                ovl.Color = selection.Color;
                ovl.Font = selection.Font;
                index = mTextOverlayListBox.SelectedIndex;
                Project.Instance.TextOverlays.Insert(index, ovl);
            }
            else
            {
                Project.Instance.TextOverlays.Add(ovl);
            }

            Project.Instance.IsSaved = false;

            mSuspendEvents = true;
            PopulateTextOverlays();
            mTextOverlayListBox.SelectedIndex = index;
            mSuspendEvents = false;

            ShowImage(CurrentFrame);
        }


        private void mDeleteTextOverlayButton_Click(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            int index = mTextOverlayListBox.SelectedIndex;
            if ((index >= 0) && (index < Project.Instance.TextOverlays.Count))
            {
                Project.Instance.TextOverlays.RemoveAt(index);
                Project.Instance.IsSaved = false;
                ShowImage(CurrentFrame);
            }
        }


        private void mTextOverlayListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            PopulateSelectedTextOverlay();
        }


        private void mFontButton_Click(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            TextOverlay ovl = mTextOverlayListBox.SelectedItem as TextOverlay;
            if (ovl == null)
            {
                return;
            }

            FontDialog fd = new FontDialog();
            fd.Font = ovl.Font;
            fd.FontMustExist = true;
            fd.ShowApply = false;
            fd.ShowColor = false;
            fd.ShowEffects = false;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ovl.Font = fd.Font;
                Project.Instance.IsSaved = false;
                ShowImage(CurrentFrame);
            }
        }


        private void mColorButton_Click(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            TextOverlay ovl = mTextOverlayListBox.SelectedItem as TextOverlay;
            if (ovl != null)
            {
                ColorDialog dlg = new ColorDialog();
                dlg.AllowFullOpen = true;
                dlg.AnyColor = true;
                dlg.FullOpen = true;
                dlg.SolidColorOnly = false;
                dlg.Color = ovl.Color;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    ovl.Color = dlg.Color;
                    Project.Instance.IsSaved = false;
                    ShowImage(CurrentFrame);
                }
            }
        }


        private void mTextOverlayXBox_ValueChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            TextOverlay ovl = mTextOverlayListBox.SelectedItem as TextOverlay;
            if (ovl != null)
            {
                ovl.SetX((float)mTextOverlayXBox.Value);
                Project.Instance.IsSaved = false;
                ShowImage(CurrentFrame);
            }
        }


        private void mTextOverlayYBox_ValueChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            TextOverlay ovl = mTextOverlayListBox.SelectedItem as TextOverlay;
            if (ovl != null)
            {
                ovl.SetY((float)mTextOverlayYBox.Value);
                Project.Instance.IsSaved = false;
                ShowImage(CurrentFrame);
            }
        }


        private void mTextOverlayBox_TextChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            TextOverlay ovl = mTextOverlayListBox.SelectedItem as TextOverlay;
            if (ovl != null)
            {
                ovl.Text = mTextOverlayBox.Text;
                Project.Instance.IsSaved = false;
                mTextOverlayListBox.Invalidate();
                ShowImage(CurrentFrame);
            }
        }


        private void mFrameMultipleBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                mCurrentFrame.FrameMultiple = (int)mFrameMultipleBox.Value;
                Project.Instance.IsSaved = false;
            }
        }


        private void mJiggleXBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                mCurrentFrame.JiggleCorrection = new Vector2((float)mJiggleXBox.Value, mCurrentFrame.JiggleCorrection.Y);
                Project.Instance.UpdateAccumulatedProperties(Math.Max(0, CurrentFrame - 1));
                Project.Instance.IsSaved = false;
                mPreviewWindow.InvalidateCachedImages(CurrentFrame, Project.Instance.GetNumFrames() - 1);
                ShowImage(CurrentFrame);
            }
        }


        private void mJiggleYBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                mCurrentFrame.JiggleCorrection = new Vector2(mCurrentFrame.JiggleCorrection.X, (float)mJiggleYBox.Value);
                Project.Instance.UpdateAccumulatedProperties(Math.Max(0, CurrentFrame - 1));
                Project.Instance.IsSaved = false;
                mPreviewWindow.InvalidateCachedImages(CurrentFrame, Project.Instance.GetNumFrames() - 1);
                ShowImage(CurrentFrame);
            }
        }


        private void mJiggleAngleBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                mCurrentFrame.AngleCorrection = (float)mJiggleAngleBox.Value;
                Project.Instance.IsSaved = false;
                mPreviewWindow.InvalidateCachedImage(CurrentFrame);
                ShowImage(CurrentFrame);
            }
        }


        private void mCompassModeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                mCurrentFrame.CompassMode = (Frame.CompassUpdateMode)mCompassModeBox.SelectedItem;
                Project.Instance.IsSaved = false;
            }
        }


        private void mNextKeyButton_Click(object sender, EventArgs e)
        {
            Project p = Project.Instance;
            if (p == null)
            {
                return;
            }

            List<int> keys = new List<int>();
            keys.Add((int)p.ContrastTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.GammaTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.HeadingOffsetTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.HueTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.LatitudeOffsetTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.LongitudeOffsetTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.MapZoomTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.RotationTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.SaturationTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.ScaleTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.TranslationTimeline.GetKeyAfter((double)CurrentFrame));
            keys.Add((int)p.BrightnessTimeline.GetKeyAfter((double)CurrentFrame));

            int frame = p.GetNumFrames() - 1;
            foreach (int k in keys)
            {
                if (k != CurrentFrame)
                {
                    frame = Math.Min(frame, k);
                }
            }

            frame = Math.Max(frame, CurrentFrame);

            ShowImage(frame);
        }


        private void mPrevKeyButton_Click(object sender, EventArgs e)
        {
            Project p = Project.Instance;
            if (p == null)
            {
                return;
            }

            int frame = 0;

            List<int> keys = new List<int>();
            keys.Add((int)p.ContrastTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.GammaTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.HeadingOffsetTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.HueTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.LatitudeOffsetTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.LongitudeOffsetTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.MapZoomTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.RotationTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.SaturationTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.ScaleTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.TranslationTimeline.GetKeyBefore((double)CurrentFrame));
            keys.Add((int)p.BrightnessTimeline.GetKeyBefore((double)CurrentFrame));

            foreach (int k in keys)
            {
                if (k != CurrentFrame)
                {
                    frame = Math.Max(frame, k);
                }
            }

            ShowImage(frame);
        }


        private void DeleteFrameButton_Click(object sender, EventArgs e)
        {
            int index = mCurrentFrame.Index;
            Project.Instance.FrameStore.Remove(new Frame[] { mCurrentFrame });
            mCurrentFrame = null;
            index = Math.Max(0, Math.Min(index, Project.Instance.FrameStore.Count - 1));
            mPreviewWindow.InvalidateCachedImage(index);
            ShowImage(index);
        }

        #endregion
        #region --------- Timeline controls ---------

        private void mScaleKeyButton_CheckedChanged(object sender, EventArgs e)
        {
            OnTimelineKeyBoxCheckedChanged(Project.Instance.ScaleTimeline, mScaleBox.Value, mScaleKeyButton.Checked);
        }


        private void mRotationKeyBotton_CheckedChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents)
            {
                return;
            }

            if (mRotationKeyBotton.Checked)
            {
                Project.Instance.RotationTimeline.Insert((double)CurrentFrame, (float)mRotationBox.Value);
            }
            else
            {
                Project.Instance.RotationTimeline.Remove((double)CurrentFrame);
            }

            Project.Instance.IsSaved = false;
            mPreviewWindow.InvalidateCachedImage(CurrentFrame);
            ShowImage(CurrentFrame);
        }


        private void mTranslationKeyButton_CheckedChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents)
            {
                return;
            }

            if (mTranslationKeyButton.Checked)
            {
                Project.Instance.TranslationTimeline.Insert((double)CurrentFrame, new ManagedCowTools.Math.Vector2((float)mXTranslationBox.Value, (float)mYTranslationBox.Value));
            }
            else
            {
                Project.Instance.TranslationTimeline.Remove((double)CurrentFrame);
            }

            Project.Instance.IsSaved = false;
            mPreviewWindow.InvalidateCachedImage(CurrentFrame);
            ShowImage(CurrentFrame);
        }


        private void mScaleBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.ScaleTimeline, mScaleBox.Value);
        }


        private void mRotationBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.RotationTimeline, mRotationBox.Value);
        }


        private void mXTranslationBox_ValueChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents)
            {
                return;
            }

            Project.Instance.TranslationTimeline.Insert((double)CurrentFrame, new ManagedCowTools.Math.Vector2((float)mXTranslationBox.Value, (float)mYTranslationBox.Value));
            Project.Instance.IsSaved = false;
            mPreviewWindow.InvalidateCachedImage(CurrentFrame);
            ShowImage(CurrentFrame);
        }


        private void mYTranslationBox_ValueChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents)
            {
                return;
            }

            Project.Instance.TranslationTimeline.Insert((double)CurrentFrame, new ManagedCowTools.Math.Vector2((float)mXTranslationBox.Value, (float)mYTranslationBox.Value));
            Project.Instance.IsSaved = false;
            mPreviewWindow.InvalidateCachedImage(CurrentFrame);
            ShowImage(CurrentFrame);
        }


        private void mCompassOffsetBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.HeadingOffsetTimeline, mCompassOffsetBox.Value);
        }


        private void mCompassKeyButton_CheckedChanged(object sender, EventArgs e)
        {
            OnTimelineKeyBoxCheckedChanged(Project.Instance.HeadingOffsetTimeline, mCompassOffsetBox.Value, mCompassKeyButton.Checked);
        }


        private void mLatitudeOffsetBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.LatitudeOffsetTimeline, mLatitudeOffsetBox.Value);
        }


        private void mLongitudeOffsetBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.LongitudeOffsetTimeline, mLongitudeOffsetBox.Value);
        }


        private void mLatitudeKeyButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                Project proj = Project.Instance;
                int before = (int)proj.LatitudeOffsetTimeline.GetKeyBefore((double)CurrentFrame);
                int after = (int)proj.LatitudeOffsetTimeline.GetKeyAfter((double)CurrentFrame);
                InvalidateMaps(Math.Min(proj.GetNumFrames() - 1, before), Math.Max(0, after));
            }
            OnTimelineKeyBoxCheckedChanged(Project.Instance.LatitudeOffsetTimeline, mLatitudeOffsetBox.Value, mLatitudeKeyButton.Checked);
        }


        private void mLongitudeKeyButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                Project proj = Project.Instance;
                int before = (int)proj.LongitudeOffsetTimeline.GetKeyBefore((double)CurrentFrame);
                int after = (int)proj.LongitudeOffsetTimeline.GetKeyAfter((double)CurrentFrame);
                InvalidateMaps(Math.Min(proj.GetNumFrames() - 1, before), Math.Max(0, after));
            }
            OnTimelineKeyBoxCheckedChanged(Project.Instance.LongitudeOffsetTimeline, mLongitudeOffsetBox.Value, mLongitudeKeyButton.Checked);
        }


        private void mMapZoomBox_ValueChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                Project proj = Project.Instance;
                int before = (int)proj.MapZoomTimeline.GetKeyBefore((double)CurrentFrame);
                int after = (int)proj.MapZoomTimeline.GetKeyAfter((double)CurrentFrame);
                InvalidateMaps(Math.Min(proj.GetNumFrames() - 1, before), Math.Max(0, after));
            }
            OnTimelineControlChanged(Project.Instance.MapZoomTimeline, mMapZoomBox.Value);
        }


        private void mMapZoomKeyBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!mSuspendEvents)
            {
                Project proj = Project.Instance;
                int before = (int)proj.MapZoomTimeline.GetKeyBefore((double)CurrentFrame);
                int after = (int)proj.MapZoomTimeline.GetKeyAfter((double)CurrentFrame);
                InvalidateMaps(Math.Min(proj.GetNumFrames() - 1, before), Math.Max(0, after));
            }
            OnTimelineKeyBoxCheckedChanged(Project.Instance.MapZoomTimeline, mMapZoomBox.Value, mMapZoomKeyBox.Checked);
        }


        private void mHueBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.HueTimeline, mHueBox.Value);
        }


        private void mSaturationBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.SaturationTimeline, mSaturationBox.Value);
        }


        private void mValueBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.BrightnessTimeline, mValueBox.Value);
        }


        private void mGammaBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.GammaTimeline, mGammaBox.Value);
        }


        private void mContrastBox_ValueChanged(object sender, EventArgs e)
        {
            OnTimelineControlChanged(Project.Instance.ContrastTimeline, mContrastBox.Value);
        }


        private void mHueKeyBox_CheckedChanged(object sender, EventArgs e)
        {
            OnTimelineKeyBoxCheckedChanged(Project.Instance.HueTimeline, mHueBox.Value, mHueKeyBox.Checked);
        }


        private void mSaturationKeyBox_CheckedChanged(object sender, EventArgs e)
        {
            OnTimelineKeyBoxCheckedChanged(Project.Instance.SaturationTimeline, mSaturationBox.Value, mSaturationKeyBox.Checked);
        }


        private void mValueKeyBox_CheckedChanged(object sender, EventArgs e)
        {
            OnTimelineKeyBoxCheckedChanged(Project.Instance.BrightnessTimeline, mValueBox.Value, mValueKeyBox.Checked);
        }


        private void mGammaKeyBox_CheckedChanged(object sender, EventArgs e)
        {
            OnTimelineKeyBoxCheckedChanged(Project.Instance.GammaTimeline, mGammaBox.Value, mGammaKeyBox.Checked);
        }


        private void mContrastKeyBox_CheckedChanged(object sender, EventArgs e)
        {
            OnTimelineKeyBoxCheckedChanged(Project.Instance.ContrastTimeline, mContrastBox.Value, mContrastKeyBox.Checked);
        }


        private void OnTimelineControlChanged(FloatParameter aTimeline, decimal aNewValue)
        {
            if (mSuspendEvents)
            {
                return;
            }

            aTimeline.Insert((double)CurrentFrame, (float)aNewValue);
            Project.Instance.IsSaved = false;
            mPreviewWindow.InvalidateCachedImages();
            ShowImage(CurrentFrame);
        }


        private void OnTimelineKeyBoxCheckedChanged(FloatParameter aTimeline, decimal aCurrentValue, bool aChecked)
        {
            if (mSuspendEvents)
            {
                return;
            }

            if (aChecked)
            {
                aTimeline.Insert((double)CurrentFrame, (float)aCurrentValue);
            }
            else
            {
                aTimeline.Remove((double)CurrentFrame);
            }

            Project.Instance.IsSaved = false;
            mPreviewWindow.InvalidateCachedImages();
            ShowImage(CurrentFrame);
        }

        #endregion
        #region -------- List boxes --------

        private void mSequenceListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            List<uint> selection = new List<uint>();
            selection.Add(0); // Sentinel to prevent selection cloning into throwaway frames.
            foreach (Project.Sequence s in mSequenceListBox.SelectedItems)
            {
                selection.Add(s.ID);
            }

            mCurrentFrame.Sequences = selection;
            Project.Instance.IsSaved = false;
        }


        private void mMapListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            List<string> list = new List<string>();
            foreach (string s in mMapListBox.CheckedItems)
            {
                list.Add(s);
            }

            mCurrentFrame.MapsToRender = list;
            Project.Instance.IsSaved = false;
        }


        private void mTextOverlayListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (mSuspendEvents || (mCurrentFrame == null))
            {
                return;
            }

            TextOverlay ovl = mTextOverlayListBox.Items[e.Index] as TextOverlay;
            if (e.NewValue == CheckState.Checked)
            {
                if (!mCurrentFrame.TextOverlayIDs.Contains(ovl.ID))
                {
                    mCurrentFrame.TextOverlayIDs.Add(ovl.ID);
                }
            }
            else if (e.NewValue == CheckState.Unchecked)
            {
                mCurrentFrame.TextOverlayIDs.Remove(ovl.ID);
            }

            Project.Instance.IsSaved = false;
            ShowImage(CurrentFrame);
        }

        #endregion
        #endregion
        #region -------- Private helpers --------

        private void ResetControls()
        {
            SuspendLayout();
            mSuspendEvents = true;

            mFileIndexWidget.Value = 0;
            mFileIndexWidget.Maximum = Project.Instance.GetNumFrames() - 1;

            mSequenceListBox.Items.Clear();
            mSequenceIndices = new Dictionary<uint, int>();
            foreach (Project.Sequence s in Project.Instance.Sequences)
            {
                int index = mSequenceListBox.Items.Add(s);
                mSequenceIndices[s.ID] = index;
            }

            // Populate maps list with found OSM files.
            mMapListBox.Items.Clear();
            SortedList<string, bool> mapFiles = new SortedList<string, bool>();
            if (Directory.Exists(Project.Instance.OsmMapsPath))
            {
                foreach (string fileName in Directory.GetFiles(Project.Instance.OsmMapsPath, "*.osm", SearchOption.AllDirectories))
                {
                    string name = Path.GetFileNameWithoutExtension(fileName);
                    if (!mapFiles.ContainsKey(name))
                    {
                        mapFiles.Add(name, false);
                    }
                }
            }

            foreach (string fileName in mapFiles.Keys)
            {
                mMapListBox.Items.Add(fileName);
            }


            mSuspendEvents = false;
            ResumeLayout();

            ShowImage(Project.Instance.LastSelectedImage);
        }


        private void InvalidateMaps(int aFrom, int aTo)
        {
            Project proj = Project.Instance;
            for (int i = aFrom; i <= aTo; ++i)
            {
                Frame f = proj.GetFrame(i);
                f.MapImageFilename = null;
            }

            proj.IsSaved = false;
        }


        private void UpdateTimelineControls(int aIndex, FloatParameter aTimeline, float aDefault, NumericUpDown aNumberBox, CheckBox aKeyedBox, bool aEnable)
        {
            float val = aDefault;
            bool check = false;

            if ((aIndex >= 0) && (aIndex < Project.Instance.GetNumFrames()))
            {
                val = aTimeline[(double)aIndex];
                check = aTimeline.IsKeyAt((double)aIndex);
            }

            aNumberBox.Value = Math.Max(aNumberBox.Minimum, Math.Min(aNumberBox.Maximum, (decimal)val));
            aKeyedBox.Checked = check;

            aNumberBox.Enabled = aEnable;
            aKeyedBox.Enabled = aEnable;
        }


        private void PopulateMapList()
        {
            if (!mSuspendEvents)
            {
                throw new InvalidOperationException("Should only be called when populating controls");
            }

            HashSet<string> czeched = null;
            if (mCurrentFrame != null)
            {
                czeched = new HashSet<string>(mCurrentFrame.MapsToRender);
            }
            else
            {
                czeched = new HashSet<string>();
            }

            List<object> items = new List<object>();
            foreach (object o in mMapListBox.Items)
            {
                items.Add(o);
            }

            foreach (object o in items)
            {
                mMapListBox.SetItemChecked(mMapListBox.Items.IndexOf(o), czeched.Contains((string)o));
            }
        }


        private void PopulateTextOverlays()
        {
            if (!mSuspendEvents)
            {
                throw new InvalidOperationException("Should only be called when populating controls");
            }

            int selected = mTextOverlayListBox.SelectedIndex;
            mTextOverlayListBox.Items.Clear();

            HashSet<int> enabled = new HashSet<int>();
            if (mCurrentFrame != null)
            {
                foreach (int id in mCurrentFrame.TextOverlayIDs)
                {
                    enabled.Add(id);
                }
            }

            foreach (TextOverlay ovl in Project.Instance.TextOverlays)
            {
                int index = mTextOverlayListBox.Items.Add(ovl);
                mTextOverlayListBox.SetItemChecked(index, enabled.Contains(ovl.ID));
            }

            if (Project.Instance.TextOverlays.Count > selected)
            {
                mTextOverlayListBox.SelectedIndex = selected;
            }

            PopulateSelectedTextOverlay();
        }


        private void PopulateSelectedTextOverlay()
        {
            TextOverlay ovl = mTextOverlayListBox.SelectedItem as TextOverlay;
            if (ovl != null)
            {
                mTextOverlayBox.Text = ovl.Text;
                mTextOverlayXBox.Value = (decimal)ovl.Position.X;
                mTextOverlayYBox.Value = (decimal)ovl.Position.Y;
                mTextOverlayBox.Enabled = true;
                mTextOverlayXBox.Enabled = true;
                mTextOverlayYBox.Enabled = true;
                mColorButton.Enabled = true;
                mFontButton.Enabled = true;
                mDeleteTextOverlayButton.Enabled = true;
            }
            else
            {
                mTextOverlayXBox.Value = 0;
                mTextOverlayYBox.Value = 0;
                mTextOverlayBox.Text = "";
                mTextOverlayBox.Enabled = false;
                mTextOverlayXBox.Enabled = false;
                mTextOverlayYBox.Enabled = false;
                mColorButton.Enabled = false;
                mFontButton.Enabled = false;
                mDeleteTextOverlayButton.Enabled = false;
            }
        }


        private bool BatchAutoregister(int aIndex, bool aDoBlackspace)
        {
            if (aIndex > 0)
            {
                Frame cur = Project.Instance.GetFrame(aIndex);
                Frame prev = Project.Instance.GetFrame(aIndex - 1);
                if ((prev.FrameMultiple > 0) && (cur.FrameMultiple > 0))
                {
                    Vector2 delta = PreviewPanel.AutoRegister(aIndex, aIndex - 1);
                    cur.JiggleCorrection += delta;
                    Project.Instance.UpdateAccumulatedProperties(aIndex - 1);
                    if (aDoBlackspace)
                    {
                        BatchRemoveBlackspace(aIndex, false);
                    }

                    mPreviewWindow.InvalidateCachedImages(aIndex, Project.Instance.GetNumFrames() - 1);
                }
            }

            return true;
        }


        private bool BatchProgress(int aIndex)
        {
            mPreviewWindow.DisplayImage(aIndex, Project.Instance.GetFrame(aIndex));
            return true;
        }


        private Vector2 ConstrainEdges(int aIndex, Frame f, Bitmap aBitmap)
        {
            ImageHelper ih = new ImageHelper();
            ih.OutputSize = new Size(Project.Instance.OutputWidth, Project.Instance.OutputHeight);
            ih.Scale = Project.Instance.ScaleTimeline[(double)aIndex];
            ih.Rotation = f.AngleCorrection + Project.Instance.RotationTimeline[(double)aIndex];
            ih.Translation = f.AccumulatedJiggleCorrection + Project.Instance.TranslationTimeline[(double)aIndex];
            Matrix m = ih.GetTransform(aBitmap.Width, aBitmap.Height);
            Matrix identity = new Matrix();
            identity.Reset();

            Vector2 result;
            RectangleF destRect = new RectangleF(0.0f, 0.0f, (float)ih.OutputSize.Width, (float)ih.OutputSize.Height);
            RectangleF sourceRect = new RectangleF(0.0f, 0.0f, (float)aBitmap.Width, (float)aBitmap.Height);
            if (ManagedCowTools.Geometry.GeometryHelper.MoveRectInside(sourceRect, m, destRect, identity, out result) == ManagedCowTools.Geometry.GeometryHelper.GeometryResult.Impossible)
            {
                return Vector2.Zero;
            }

            result *= -1.0f;

            // For this program's usage, the original result will usually be a diagonal with
            // a small vertical component. To avoid vertical drift, we'll try flattening it
            // out and see if that works.  NOTE this is not quite correct since the
            // ideal horizontal translation is actually slightly larger than the magnitude
            // of the vector, but it will at least reduce the typical vertical drift to
            // tiny fractions of a pixel.

            float mag = result.Magnitude;
            if (mag < 0.1f) // Ignore fractions of a pixel.
            {
                return result;
            }

            if (result.X < 0.0f)
            {
                mag = -mag;
            }

            Vector2 temp = new Vector2(mag, 0.0f);

            // Check and see if the flattened vector works.
            Vector2 newResult;
            ih.Translation += temp;
            m = ih.GetTransform(aBitmap.Width, aBitmap.Height);
            if (ManagedCowTools.Geometry.GeometryHelper.MoveRectInside(sourceRect, m, destRect, identity, out newResult) == ManagedCowTools.Geometry.GeometryHelper.GeometryResult.OK)
            {
                result = temp + newResult;  // Add in vertical correction if it was still needed.
            }

            return result;
        }

        #endregion
    }
}
