﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace TimeLapseEditor
{
    public partial class GenerateMapScriptsDialog : Form
    {
        Project mProject;

        public GenerateMapScriptsDialog(Project aProject)
        {
            mProject = aProject;

            InitializeComponent();

            mRangeMaxBox.Value = (decimal)Properties.Settings.Default.MapRangeMax;
            mRangeMinBox.Value = (decimal)Properties.Settings.Default.MapRangeMin;
            mRangeAllButton.Checked = Properties.Settings.Default.MapRangeAll;
            mBaseNameBox.Text = Properties.Settings.Default.MapScriptBaseName;
            mBatchSizeBox.Value = (decimal)Properties.Settings.Default.MapBatchSize;
            mWidthBox.Value = (decimal)mProject.MapImageDimensions.X;
            mHeightBox.Value = (decimal)mProject.MapImageDimensions.Y;
        }


        private void mRangeSpecifyButton_CheckedChanged(object sender, System.EventArgs e)
        {
            mRangeMinBox.Enabled = mRangeSpecifyButton.Checked;
            mRangeMaxBox.Enabled = mRangeSpecifyButton.Checked;
        }


        private void mRangeMinBox_ValueChanged(object sender, System.EventArgs e)
        {
            if (mRangeMinBox.Value > mRangeMaxBox.Value)
            {
                mRangeMaxBox.Value = mRangeMinBox.Value;
            }

            mRangeSpecifyButton.Checked = true;
        }


        private void mRangeMaxBox_ValueChanged(object sender, System.EventArgs e)
        {
            if (mRangeMinBox.Value > mRangeMaxBox.Value)
            {
                mRangeMinBox.Value = mRangeMaxBox.Value;
            }

            mRangeSpecifyButton.Checked = true;
        }


        private void mGenerateButton_Click(object sender, System.EventArgs e)
        {
            GenerateScripts();
        }


        private List<string> GetMapList(Frame aFrame)
        {
            SortedList<string, bool> list = new SortedList<string, bool>();
            foreach (string name in aFrame.MapsToRender)
            {
                list.Add(name, false);
            }

            return new List<string>(list.Keys);
        }


        private bool ListsSame(IList aList1, IList aList2)
        {
            if ((aList1 == null) && (aList2 == null))
            {
                return true;
            }

            if ((aList1 == null) || (aList2 == null))
            {
                return false;
            }

            if (aList1.Count != aList2.Count)
            {
                return false;
            }

            int count = aList1.Count;
            for (int i = 0; i < count; ++i)
            {
                if (!aList1[i].Equals(aList2[i]))
                {
                    return false;
                }
            }

            return true;
        }


        private string GetOutputPath(int aIndex)
        {
            string batchDir = string.Format("{0:0000}", (int)(aIndex / 1000));
            string path = Path.Combine(mProject.ProjectRoot, "maps", batchDir);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }


        private void GenerateScripts()
        {
            UseWaitCursor = true;

            int numFrames = mProject.GetNumFrames();
            int startFrame = 0;
            int endFrame = numFrames - 1;
            if (mRangeSpecifyButton.Checked)
            {
                startFrame = Math.Max((int)mRangeMinBox.Value - 1, 0);
                endFrame = Math.Min((int)mRangeMaxBox.Value - 1, numFrames - 1);
            }

            mProgressBar.Minimum = startFrame;
            mProgressBar.Maximum = endFrame + 1;
            mProgressBar.Value = startFrame;

            Directory.CreateDirectory(Path.Combine(mProject.ProjectRoot, "scripts"));
            string basePath = Path.Combine(mProject.ProjectRoot, "scripts", mBaseNameBox.Text);

            int count = 0;
            int fileNo = 0;
            StreamWriter writer = null;
            Frame currentFrame = mProject.GetFrame(startFrame);
            List<string> prevMaps = GetMapList(currentFrame);
            List<string> curMaps = new List<string>(prevMaps);
            for (int i = startFrame; i <= endFrame;)
            {
                if (writer == null) // Starting a new script.
                {
                    string fileName = string.Format("{0}{1:0000}.txt", basePath, fileNo);
                    ++fileNo;
                    writer = new StreamWriter(fileName, false);
                    //writer.WriteLine(string.Format("change-dir {0}", mLocalOutputPathBox.Text));
                    writer.WriteLine("clear-map");
                    if (!string.IsNullOrEmpty(mProject.MapRulesName))
                    {
                        writer.WriteLine(String.Format("use-ruleset {0}", mProject.MapRulesName));
                    }
                    else
                    {
                        writer.WriteLine("use-ruleset alias=googlemaps");
                    }
                    writer.WriteLine("apply-ruleset");

                    foreach (string mapName in curMaps)
                    {
                        writer.WriteLine(string.Format("load-source {0}", Path.Combine(mProject.OsmMapsPath, mapName + ".osm")));
                    }
                }

                // Write script instructions for this frame.
                if (currentFrame.CoordinatesValid
                 && (curMaps.Count > 0)
                 && (currentFrame.FrameMultiple > 0)
                 && (!mOnlyMissingButton.Checked || String.IsNullOrEmpty(currentFrame.MapImageFilename)))
                {
                    double latitude = currentFrame.Latitude + (double)mProject.LatitudeOffsetTimeline[i];
                    double longitude = currentFrame.Longitude + (double)mProject.LongitudeOffsetTimeline[i];
                    writer.WriteLine(string.Format("move-pos x=-{0} y={1}", longitude, latitude));

                    writer.Write("export-bitmap");
                    float zoom = mProject.MapZoomTimeline[(double)i];
                    writer.Write(string.Format(" zoom={0}", zoom));
                    string outputBaseName = Path.GetFileNameWithoutExtension(currentFrame.SourcePath);
                    string outputPath = GetOutputPath(i);
                    writer.Write(string.Format(" file={0}", Path.Combine(outputPath, outputBaseName + "_map.png")));
                    writer.Write(string.Format(" width={0} height={1}", (int)mWidthBox.Value, (int)mHeightBox.Value));
                    writer.WriteLine(" scale=1");

                    ++count;
                }

                // Determine if it's time to split the output into a new script file.
                bool split = false;
                if (count >= (int)mBatchSizeBox.Value) // Split by batch size.
                {
                    split = true;
                }

                // Split when the map selections change.
                ++i;
                mProgressBar.Value = i;
                currentFrame = null;
                prevMaps = curMaps;
                curMaps = null;
                if (i <= endFrame)
                {
                    currentFrame = mProject.GetFrame(i);
                    curMaps = GetMapList(currentFrame);
                    if (!ListsSame(prevMaps, curMaps))
                    {
                        split = true;
                    }
                }

                // End file
                if (split)
                {
                    if (count == 0)
                    {
                        --fileNo; // No frames were emitted in the last file; overwrite it next pass.
                    }

                    count = 0;
                    writer.WriteLine("exit");
                    writer.Close();
                    writer = null;
                }
            }

            if (writer != null)
            {
                writer.WriteLine("exit");
                writer.Close();
            }

            UseWaitCursor = false;

            Properties.Settings.Default.MapRangeMax = (int)mRangeMaxBox.Value;
            Properties.Settings.Default.MapRangeMin = (int)mRangeMinBox.Value;
            Properties.Settings.Default.MapRangeAll = mRangeAllButton.Checked;
            Properties.Settings.Default.MapBatchSize = (int)mBatchSizeBox.Value;
            Properties.Settings.Default.MapScriptBaseName = mBaseNameBox.Text;
            Properties.Settings.Default.ScriptImageWidth = (int)mWidthBox.Value;
            Properties.Settings.Default.ScriptImageHeight= (int)mHeightBox.Value;
        }
    }
}
