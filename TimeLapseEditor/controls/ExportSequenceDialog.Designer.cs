﻿namespace TimeLapseEditor
{
    partial class ExportSequenceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mOutputPathBox = new ManagedCowTools.WinForms.PathWidget();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mProgressBar = new System.Windows.Forms.ProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mFirstOutputIndexBox = new System.Windows.Forms.NumericUpDown();
            this.mFrameRangeEndBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.mFrameRangeStartBox = new System.Windows.Forms.NumericUpDown();
            this.mOverlayButton = new System.Windows.Forms.CheckBox();
            this.mGoButton = new System.Windows.Forms.Button();
            this.mProgressLabel = new System.Windows.Forms.Label();
            this.mBaseFilenameBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mSequenceListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mNumCopiesBox = new System.Windows.Forms.NumericUpDown();
            this.mShowFrameButton = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mFirstOutputIndexBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFrameRangeEndBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFrameRangeStartBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mNumCopiesBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mOutputPathBox
            // 
            this.mOutputPathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mOutputPathBox.BrowseMode = ManagedCowTools.WinForms.PathWidget.BrowseModeType.Directory;
            this.mOutputPathBox.FileFilter = "All files (*.*)|*.*";
            this.mOutputPathBox.Location = new System.Drawing.Point(6, 19);
            this.mOutputPathBox.Name = "mOutputPathBox";
            this.mOutputPathBox.PathMustExist = true;
            this.mOutputPathBox.SelectedPath = "";
            this.mOutputPathBox.Size = new System.Drawing.Size(399, 26);
            this.mOutputPathBox.TabIndex = 0;
            this.mOutputPathBox.PathChanged += new ManagedCowTools.WinForms.PathWidget.PathChangedEvent(this.mOutputPathBox_PathChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.mOutputPathBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(411, 55);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Output path";
            // 
            // mProgressBar
            // 
            this.mProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mProgressBar.Location = new System.Drawing.Point(12, 279);
            this.mProgressBar.Name = "mProgressBar";
            this.mProgressBar.Size = new System.Drawing.Size(411, 23);
            this.mProgressBar.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.mFirstOutputIndexBox);
            this.groupBox2.Controls.Add(this.mFrameRangeEndBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.mFrameRangeStartBox);
            this.groupBox2.Location = new System.Drawing.Point(18, 137);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(208, 75);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Frame Range";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "First output index:";
            // 
            // mFirstOutputIndexBox
            // 
            this.mFirstOutputIndexBox.Location = new System.Drawing.Point(119, 47);
            this.mFirstOutputIndexBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.mFirstOutputIndexBox.Name = "mFirstOutputIndexBox";
            this.mFirstOutputIndexBox.Size = new System.Drawing.Size(73, 20);
            this.mFirstOutputIndexBox.TabIndex = 3;
            this.mFirstOutputIndexBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mFrameRangeEndBox
            // 
            this.mFrameRangeEndBox.Location = new System.Drawing.Point(119, 20);
            this.mFrameRangeEndBox.Name = "mFrameRangeEndBox";
            this.mFrameRangeEndBox.Size = new System.Drawing.Size(73, 20);
            this.mFrameRangeEndBox.TabIndex = 2;
            this.mFrameRangeEndBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mFrameRangeEndBox.ValueChanged += new System.EventHandler(this.mFrameRangeEndBox_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(97, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "to";
            // 
            // mFrameRangeStartBox
            // 
            this.mFrameRangeStartBox.Location = new System.Drawing.Point(7, 20);
            this.mFrameRangeStartBox.Name = "mFrameRangeStartBox";
            this.mFrameRangeStartBox.Size = new System.Drawing.Size(84, 20);
            this.mFrameRangeStartBox.TabIndex = 0;
            this.mFrameRangeStartBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mFrameRangeStartBox.ValueChanged += new System.EventHandler(this.mFrameRangeStartBox_ValueChanged);
            // 
            // mOverlayButton
            // 
            this.mOverlayButton.AutoSize = true;
            this.mOverlayButton.Location = new System.Drawing.Point(18, 74);
            this.mOverlayButton.Name = "mOverlayButton";
            this.mOverlayButton.Size = new System.Drawing.Size(105, 17);
            this.mOverlayButton.TabIndex = 4;
            this.mOverlayButton.Text = "Render Overlays";
            this.mOverlayButton.UseVisualStyleBackColor = true;
            // 
            // mGoButton
            // 
            this.mGoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mGoButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.mGoButton.Location = new System.Drawing.Point(12, 250);
            this.mGoButton.Name = "mGoButton";
            this.mGoButton.Size = new System.Drawing.Size(214, 23);
            this.mGoButton.TabIndex = 5;
            this.mGoButton.Text = "Start";
            this.mGoButton.UseVisualStyleBackColor = false;
            this.mGoButton.Click += new System.EventHandler(this.mGoButton_Click);
            // 
            // mProgressLabel
            // 
            this.mProgressLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mProgressLabel.AutoSize = true;
            this.mProgressLabel.BackColor = System.Drawing.Color.Transparent;
            this.mProgressLabel.Location = new System.Drawing.Point(21, 284);
            this.mProgressLabel.Name = "mProgressLabel";
            this.mProgressLabel.Size = new System.Drawing.Size(134, 13);
            this.mProgressLabel.TabIndex = 6;
            this.mProgressLabel.Text = "Wrote frame i as foobarbaz";
            // 
            // mBaseFilenameBox
            // 
            this.mBaseFilenameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mBaseFilenameBox.Location = new System.Drawing.Point(97, 218);
            this.mBaseFilenameBox.Name = "mBaseFilenameBox";
            this.mBaseFilenameBox.Size = new System.Drawing.Size(129, 20);
            this.mBaseFilenameBox.TabIndex = 7;
            this.mBaseFilenameBox.TextChanged += new System.EventHandler(this.mBaseFilenameBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Base Filename:";
            // 
            // mSequenceListBox
            // 
            this.mSequenceListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mSequenceListBox.FormattingEnabled = true;
            this.mSequenceListBox.Location = new System.Drawing.Point(232, 73);
            this.mSequenceListBox.Name = "mSequenceListBox";
            this.mSequenceListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.mSequenceListBox.Size = new System.Drawing.Size(191, 199);
            this.mSequenceListBox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(162, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Sequences:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "copies of each frame";
            // 
            // mNumCopiesBox
            // 
            this.mNumCopiesBox.Location = new System.Drawing.Point(18, 98);
            this.mNumCopiesBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.mNumCopiesBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mNumCopiesBox.Name = "mNumCopiesBox";
            this.mNumCopiesBox.Size = new System.Drawing.Size(36, 20);
            this.mNumCopiesBox.TabIndex = 12;
            this.mNumCopiesBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mNumCopiesBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // mShowFrameButton
            // 
            this.mShowFrameButton.AutoSize = true;
            this.mShowFrameButton.Location = new System.Drawing.Point(137, 116);
            this.mShowFrameButton.Name = "mShowFrameButton";
            this.mShowFrameButton.Size = new System.Drawing.Size(95, 17);
            this.mShowFrameButton.TabIndex = 13;
            this.mShowFrameButton.Text = "Show Frame #";
            this.mShowFrameButton.UseVisualStyleBackColor = true;
            // 
            // ExportSequenceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 314);
            this.Controls.Add(this.mShowFrameButton);
            this.Controls.Add(this.mNumCopiesBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mSequenceListBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.mBaseFilenameBox);
            this.Controls.Add(this.mProgressLabel);
            this.Controls.Add(this.mGoButton);
            this.Controls.Add(this.mOverlayButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.mProgressBar);
            this.Controls.Add(this.groupBox1);
            this.Name = "ExportSequenceDialog";
            this.Text = "ExportSequenceDialog";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mFirstOutputIndexBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFrameRangeEndBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mFrameRangeStartBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mNumCopiesBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ManagedCowTools.WinForms.PathWidget mOutputPathBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar mProgressBar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown mFrameRangeEndBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown mFrameRangeStartBox;
        private System.Windows.Forms.CheckBox mOverlayButton;
        private System.Windows.Forms.Button mGoButton;
        private System.Windows.Forms.Label mProgressLabel;
        private System.Windows.Forms.TextBox mBaseFilenameBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox mSequenceListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown mFirstOutputIndexBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown mNumCopiesBox;
        private System.Windows.Forms.CheckBox mShowFrameButton;
    }
}