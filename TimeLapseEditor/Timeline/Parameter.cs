﻿// Copyright (c) 2012 by Soleil Lapierre.

using System.Collections.Generic;

namespace TimeLapseEditor
{
    // Represents a parameter that varies with time.
    // Time is represented by a double.
    // The type T must be interpolable - ie have the +, - and * operators defined on it.
    public class Parameter<T, KeyType> : ITimeline where KeyType : ParameterKeyBase<T>, new()
    {
        protected List<KeyType> mKeyframes = new List<KeyType>();

        public Parameter()
        {
            DefaultInterpolationMode = ParameterTypes.InterpolationMode.Linear;
        }


        public Parameter(ParameterTypes.InterpolationMode aMode)
        {
            DefaultInterpolationMode = aMode;
        }


        public Parameter(Parameter<T, KeyType> aOther)
        {
            foreach (KeyType key in aOther.mKeyframes)
            {
                KeyType k = key.Clone() as KeyType;
                if (k != null)
                {
                    mKeyframes.Add(k);
                }
            }

            DefaultInterpolationMode = aOther.DefaultInterpolationMode;
        }


        public List<KeyType> KeyFrames
        {
            get { return mKeyframes; }
            set { mKeyframes = value; }
        }


        public ParameterTypes.InterpolationMode DefaultInterpolationMode { get; set; }


        public KeyType Insert(double aTime, T aValue)
        {
            return Insert(aTime, aValue, DefaultInterpolationMode);
        }


        public KeyType Insert(double aTime, T aValue, ParameterTypes.InterpolationMode aMode)
        {
            KeyType k = new KeyType();
            k.Time = aTime;

            int index = mKeyframes.BinarySearch(k);
            if (index >= 0)
            {
                if (mKeyframes.Count > 0)
                {
                    k = mKeyframes[index];
                }
                else
                {
                    mKeyframes.Insert(0, k);
                }
            }
            else
            {
                mKeyframes.Insert(~index, k);
            }

            k.Value = aValue;
            k.Mode = aMode;

            return k;
        }


        public void Remove(double aTime)
        {
            KeyType sentinel = new KeyType();
            sentinel.Time = aTime;

            int index = mKeyframes.BinarySearch(sentinel);
            if ((index >= 0) && (mKeyframes.Count > 1))
            {
                mKeyframes.RemoveAt(index);
            }
        }


        public bool IsKeyAt(double aTime)
        {
            KeyType sentinel = new KeyType();
            sentinel.Time = aTime;

            int index = mKeyframes.BinarySearch(sentinel);
            if ((index >= 0) && (mKeyframes.Count > 0))
            {
                return true;
            }

            return false;
        }


        public double GetKeyBefore(double aTime)
        {
            if (mKeyframes.Count > 0)
            {
                KeyType sentinel = new KeyType();
                sentinel.Time = aTime;

                int index = mKeyframes.BinarySearch(sentinel);
                if (index == 0)
                {
                    return mKeyframes[0].Time;
                }
                else if (index > 0)
                {
                    return mKeyframes[index - 1].Time;
                }
                else
                {
                    index = ~index;
                    if (index > 0)
                    {
                        return mKeyframes[index - 1].Time;
                    }
                }
            }

            return aTime;
        }


        public double GetKeyAfter(double aTime)
        {
            if (mKeyframes.Count > 0)
            {
                KeyType sentinel = new KeyType();
                sentinel.Time = aTime;

                int index = mKeyframes.BinarySearch(sentinel);
                if (index < 0)
                {
                    index = ~index;
                    if (index < mKeyframes.Count)
                    {
                        return mKeyframes[index].Time;
                    }
                }
                else if (index < mKeyframes.Count - 1)
                {
                    return mKeyframes[index + 1].Time;
                }
            }

            return aTime;
        }


        [System.Xml.Serialization.XmlIgnore]
        public T this[double aTime]
        {
            get
            {
                T result = default(T);

                KeyType sentinel = new KeyType();
                sentinel.Time = aTime;

                int index = mKeyframes.BinarySearch(sentinel);
                if (index >= 0)
                {
                    if (mKeyframes.Count > 0)
                    {
                        result = mKeyframes[index].Value;
                    }
                }
                else
                {
                    index = ~index;

                    if (index == 0)
                    {
                        if (mKeyframes.Count > 0)
                        {
                            result = mKeyframes[0].Value;
                        }
                    }
                    else if (index > mKeyframes.Count - 1)
                    {
                        result = mKeyframes[mKeyframes.Count - 1].Value;
                    }
                    else
                    {
                        KeyType lower = mKeyframes[index - 1];
                        KeyType upper = mKeyframes[index];

                        result = upper.Interpolate(aTime, lower);
                    }
                }

                return result;
            }
        }
    }
}
