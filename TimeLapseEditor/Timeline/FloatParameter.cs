﻿// Copyright (c) 2012 by Soleil Lapierre.

namespace TimeLapseEditor
{
    public class FloatParameter : Parameter<float, FloatParameterKey>
    {
        public FloatParameter()
        : base()
        {
        }


        public FloatParameter(ParameterTypes.InterpolationMode aMode)
        : base(aMode)
        {
        }


        public FloatParameter(FloatParameter aOther)
        : base(aOther)
        {
        }
    }
}
