﻿// Copyright (c) 2012 by Soleil Lapierre.

using ManagedCowTools.Math;
using System;

namespace TimeLapseEditor
{
    public class Vector2ParameterKey : ParameterKeyBase<Vector2>
    {
        public override ParameterKeyBase<Vector2> Clone()
        {
            Vector2ParameterKey key = new Vector2ParameterKey();
            key.Time = Time;
            key.Mode = Mode;
            key.Value = Value;
            return key;
        }


        public override Vector2 Interpolate(double aTime, ParameterKeyBase<Vector2> aFrom)
        {
            double interval = Time - aFrom.Time;
            double portion = aTime - aFrom.Time;
            Vector2 result = aFrom.Value;

            switch (Mode)
            {
                case ParameterTypes.InterpolationMode.Step:
                    if (interval >= (0.5 * interval))
                    {
                        result = Value;
                    }
                    break;
                case ParameterTypes.InterpolationMode.Sinus:
                    {
                        double angle = ((portion / interval) * Math.PI) - 0.5 * Math.PI;
                        double scale = 0.5 * (Math.Sin(angle) + 1.0);
                        Vector2 delta = Value - aFrom.Value;
                        result += delta * (float)scale;
                    }
                    break;
                case ParameterTypes.InterpolationMode.Linear:
                default:
                    {
                        Vector2 delta = Value - aFrom.Value;
                        result += delta * (float)(portion / interval);
                    }
                    break;
            }

            return result;
        }
    }
}
