﻿// Copyright (c) 2012 by Soleil Lapierre.

using ManagedCowTools.Math;

namespace TimeLapseEditor
{
    public class Vector2Parameter : Parameter<Vector2, Vector2ParameterKey>
    {
        public Vector2Parameter()
        : base()
        {
        }


        public Vector2Parameter(ParameterTypes.InterpolationMode aMode)
        : base(aMode)
        {
        }


        public Vector2Parameter(Vector2Parameter aOther)
        : base(aOther)
        {
        }
    }
}
