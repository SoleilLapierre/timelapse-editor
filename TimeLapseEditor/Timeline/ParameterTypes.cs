﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeLapseEditor
{
    public class ParameterTypes
    {
        public enum InterpolationMode
        {
            Step = 0,
            Linear,
            Sinus
        };
    }
}
