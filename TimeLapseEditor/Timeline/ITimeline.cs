﻿// Copyright (c) 2013 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.Text;

namespace TimeLapseEditor
{
    public interface ITimeline
    {
        bool IsKeyAt(double aTime);
        double GetKeyBefore(double aTime);
        double GetKeyAfter(double aTime);
    }
}
