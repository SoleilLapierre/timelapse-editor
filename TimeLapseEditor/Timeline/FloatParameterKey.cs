﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
namespace TimeLapseEditor
{
    public class FloatParameterKey : ParameterKeyBase<float>
    {
        public override ParameterKeyBase<float> Clone()
        {
            FloatParameterKey key = new FloatParameterKey();
            key.Time = Time;
            key.Mode = Mode;
            key.Value = Value;
            return key;
        }


        public override float Interpolate(double aTime, ParameterKeyBase<float> aFrom)
        {
            double interval = Time - aFrom.Time;
            double portion = aTime - aFrom.Time;
            float result = aFrom.Value;

            switch (Mode)
            {
                case ParameterTypes.InterpolationMode.Step:
                    if (portion >= (0.5 * interval))
                    {
                        result = Value;
                    }
                    break;
                case ParameterTypes.InterpolationMode.Sinus:
                    {
                        double angle = ((portion / interval) * Math.PI) - 0.5 * Math.PI;
                        double scale = 0.5 * (Math.Sin(angle) + 1.0);
                        float delta = Value - aFrom.Value;
                        result += (float)(scale * (double)delta);
                    }
                    break;
                case ParameterTypes.InterpolationMode.Linear:
                default:
                    {
                        float delta = Value - aFrom.Value;
                        result += (float)((portion / interval) * (double)delta);
                    }
                    break;
            }

            return result;
        }
    }
}
