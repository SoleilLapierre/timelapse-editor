﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;

namespace TimeLapseEditor
{
    public abstract class ParameterKeyBase<T> : IComparable<ParameterKeyBase<T>>
    {
        public double Time { get; set; }
        public T Value { get; set; }

        public ParameterTypes.InterpolationMode Mode { get; set; }

        public ParameterKeyBase()
        {
            Mode = ParameterTypes.InterpolationMode.Linear;
        }

        public abstract ParameterKeyBase<T> Clone();

        public abstract T Interpolate(double aTime, ParameterKeyBase<T> aFrom);

        public int CompareTo(ParameterKeyBase<T> other)
        {
            return Time.CompareTo(other.Time);
        }
    }
}
