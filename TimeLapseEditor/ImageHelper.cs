﻿// Copyright (c) 2012 by Soleil Lapierre.

using ManagedCowTools.Math;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace TimeLapseEditor
{
    public class ImageHelper
    {
        public float Rotation { get; set; }
        public Vector2 Translation { get; set; }
        public float Scale { get; set; }
        public Size OutputSize { get; set; }


        public ImageHelper()
        {
            Rotation = 0.0f;
            Translation = Vector2.Zero;
            Scale = 1.0f;
            OutputSize = new Size(1024, 768);
        }


        public Bitmap Transform(Bitmap aImage)
        {
            int w = OutputSize.Width;
            int h = OutputSize.Height;
            Bitmap b = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            b.SetResolution(aImage.HorizontalResolution, aImage.VerticalResolution);
            Graphics g = Graphics.FromImage(b);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.ResetTransform();
            g.Transform = GetTransform(aImage.Width, aImage.Height);

            g.DrawImage(aImage, new Point(0, 0));

            return b;
        }


        public Matrix GetTransform(int aSourceWidth, int aSourceHeight)
        {
            Matrix m = new Matrix();

            m.Translate(Translation.X, -Translation.Y);
            m.Translate(0.5f * OutputSize.Width, 0.5f * OutputSize.Height);
            m.Scale(Scale, Scale);
            m.Rotate(-Rotation);
            m.Translate(-0.5f * aSourceWidth, -0.5f * aSourceHeight);

            return m;
        }


        public Matrix GetInverseTransform(int aSourceWidth, int aSourceHeight)
        {
            Matrix m = GetTransform(aSourceWidth, aSourceHeight);
            m.Invert();
            return m;
        }
    }
}
