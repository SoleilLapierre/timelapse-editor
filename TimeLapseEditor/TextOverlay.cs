﻿// Copyright (c) 2012 by Soleil Lapierre.

using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using ManagedCowTools.Math;

namespace TimeLapseEditor
{
    public class TextOverlay
    {
        public TextOverlay()
        {
            Text = "Text";
            Position = new ManagedCowTools.Math.Vector2(10.0f, 20.0f);
            Color = Color.Red;
            Font = new Font(System.Drawing.FontFamily.GenericSansSerif, 16.0f);
            ID = System.Guid.NewGuid().GetHashCode();
        }

        public TextOverlay(TextOverlay aOther)
        {
            Text = aOther.Text;
            Position = aOther.Position;
            Color = aOther.Color;
            Font = aOther.Font;
            ID = aOther.ID;
        }

        public string Text { get; set; }

        public int ID { get; set; }

        public Vector2 Position { get; set; }

        [XmlIgnore]
        public System.Drawing.Font Font { get; set; }

        [XmlIgnore]
        public System.Drawing.Color Color { get; set; }
        
        public override string ToString()
        {
            return Text;
        }

        public string SerlializableFont
        {
            get
            {
                try
                {
                    if (Font != null)
                    {
                        TypeConverter converter = TypeDescriptor.GetConverter(typeof(System.Drawing.Font));
                        return converter.ConvertToString(Font);
                    }
                }
                catch { }

                return null;
            }
            set
            {
                Font = null;

                try
                {
                    TypeConverter converter = TypeDescriptor.GetConverter(typeof(System.Drawing.Font));
                    Font = (System.Drawing.Font)converter.ConvertFromString(value);
                }
                catch { }
            }
        }


        public int SerializableColor
        {
            get
            {
                return Color.ToArgb();
            }
            set
            {
                Color = System.Drawing.Color.FromArgb(value);
            }
        }


        public void SetX(float aX)
        {
            Vector2 temp = Position;
            temp.X = aX;
            Position = temp;
        }


        public void SetY(float aY)
        {
            Vector2 temp = Position;
            temp.Y = aY;
            Position = temp;
        }
    }
}
