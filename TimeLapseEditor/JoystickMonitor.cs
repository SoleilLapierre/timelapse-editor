﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using SlimDX.DirectInput;
using System.Windows.Forms;

namespace TimeLapseEditor
{
    public class JoystickMonitor : IDisposable
    {
        private Joystick mJoystick;
        private JoystickState mStickState = new JoystickState();
        private bool mActive = false;
        private Timer mTimer;


        public JoystickMonitor()
        {
            DirectInput directInput = new DirectInput();

            foreach (DeviceInstance d in directInput.GetDevices(DeviceClass.GameController, DeviceEnumerationFlags.AttachedOnly))
            {
                try
                {
                    mJoystick = new Joystick(directInput, d.InstanceGuid);
                    //mJoystick.SetCooperativeLevel(this, CooperativeLevel.Exclusive | CooperativeLevel.Foreground);
                    break;
                }
                catch (DirectInputException)
                {
                }
            }

            if (mJoystick == null)
            {
                return;
            }

            foreach (DeviceObjectInstance dobj in mJoystick.GetObjects())
            {
                if ((dobj.ObjectType & ObjectDeviceType.Axis) != 0)
                {
                    mJoystick.GetObjectPropertiesById((int)dobj.ObjectType).SetRange(-1000, 1000);
                }
            }

            if (!mJoystick.Acquire().IsFailure)
            {
                mActive = true;
            
                mTimer = new Timer();
                mTimer.Tick += new EventHandler(mTimer_Tick);
                mTimer.Interval = 1000 / 30;
                mTimer.Start();
            }
        }


        public void Dispose()
        {
            mActive = false;
            mTimer.Stop();
            mTimer = null;

            if (mJoystick != null)
            {
                mJoystick.Unacquire();
                mJoystick.Dispose();
            }

            mJoystick = null;
        }


        private void mTimer_Tick(object sender, EventArgs e)
        {
            if (!mActive)
            {
                return;
            }

            if (mJoystick.Poll().IsFailure)
            {
                return;
            }

            mStickState = mJoystick.GetCurrentState();
            if (Result.Last.IsFailure)
            {
                return;
            }
        }


        public ManagedCowTools.Math.Vector2 LeftStick
        {
            get
            {
                return new ManagedCowTools.Math.Vector2((float)mStickState.X / 1000.0f, -(float)mStickState.Y / 1000.0f);
            }
        }


        public ManagedCowTools.Math.Vector2 RightStick
        {
            get
            {
                return new ManagedCowTools.Math.Vector2((float)mStickState.RotationX / 1000.0f, -(float)mStickState.RotationY / 1000.0f);
            }
        }


        public float TriggerSum
        {
            get
            {
                return (float)mStickState.Z / 1000.0f;
            }
        }


        public bool AButton
        {
            get
            {
                return mStickState.GetButtons()[0];
            }
        }


        public bool BButton
        {
            get
            {
                return mStickState.GetButtons()[1];
            }
        }


        public bool XButton
        {
            get
            {
                return mStickState.GetButtons()[2];
            }
        }


        public bool YButton
        {
            get
            {
                return mStickState.GetButtons()[3];
            }
        }


        public bool LeftShoulderButton
        {
            get
            {
                return mStickState.GetButtons()[4];
            }
        }


        public bool RightShoulderButton
        {
            get
            {
                return mStickState.GetButtons()[5];
            }
        }


        public bool LeftStickButton
        {
            get
            {
                return mStickState.GetButtons()[8];
            }
        }


        public bool RightStickButton
        {
            get
            {
                return mStickState.GetButtons()[9];
            }
        }
    }
}
