﻿// Copyright (c) 2012 by Soleil Lapierre.

using System.Collections.Generic;

namespace TimeLapseEditor
{
    public class ObjectMRUCache<IDType, ObjectType> where ObjectType : class
    {
        private class Item
        {
            public int LastAccess { get; set; }
            public IDType ID { get; set; }
            public ObjectType Value { get; set; }
        }

        private Dictionary<IDType, Item> mTable = new Dictionary<IDType, Item>();
        private List<Item> mList = new List<Item>();
        private int mCapacity;

        public ObjectMRUCache(int aCapacity)
        {
            mCapacity = aCapacity;
        }


        public void Clear()
        {
            mTable.Clear();
            mList.Clear();
        }


        public void Insert(IDType aId, ObjectType aValue)
        {
            Remove(aId);
            BumpAges();
            Item item = new Item();
            item.ID = aId;
            item.Value = aValue;
            item.LastAccess = 0;
            mTable[aId] = item;
            mList.Insert(0, item);
            TrimCapacity();
        }


        public ObjectType Get(IDType aId)
        {
            ObjectType result = null;

            if (mTable.ContainsKey(aId))
            {
                Item item = mTable[aId];
                mList.Remove(item);
                BumpAges();
                item.LastAccess = 0;
                mList.Insert(0, item);
                result = item.Value;
            }

            return result;
        }


        public void Remove(IDType aId)
        {
            if (mTable.ContainsKey(aId))
            {
                Item item = mTable[aId];
                mTable.Remove(aId);
                mList.Remove(item);
            }
        }


        private void BumpAges()
        {
            foreach (Item item in mList)
            {
                item.LastAccess++;
            }
        }


        private void TrimCapacity()
        {
            while (mList.Count > mCapacity)
            {
                Item item = mList[mList.Count - 1];
                mList.Remove(item);
                mTable.Remove(item.ID);
            }
        }
    }
}
