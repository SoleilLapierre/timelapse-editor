﻿// Copyright (c) 2012 by Soleil Lapierre.

using System;

namespace TimeLapseEditor
{
    public class Compass
    {
        private double mLatitude;
        private double mLongitude;
        private double mHeading;
        private int mCall;

        public Compass()
        {
            mLatitude = 0.0f;
            mLongitude = 0.0f;
            mHeading = 0.0f;
            mCall = 0;
        }


        public Compass(double aLatitude, double aLongitude, double aHeading)
        {
            mLatitude = aLatitude;
            mLongitude = aLongitude;
            mHeading = aHeading;
            mCall = 3;
        }

        
        public double Heading
        {
            get { return mHeading; }
            set
            {
                mHeading = value;

                while (mHeading < 0.0f)
                {
                    mHeading += 360.0f;
                }

                while (mHeading > 360.0f)
                {
                    mHeading -= 360.0f;
                }

                mCall = 2;
            }
        }


        public double Latitude
        {
            get { return mLatitude; }
            set { mLatitude = value; }
        }


        public double Longitude
        {
            get { return mLongitude; }
            set { mLongitude = value; }
        }


        public void Update(double aLatitude, double aLongitude)
        {
            if (mCall > 0)
            {
                double dy = aLatitude - mLatitude;
                double dx = aLongitude - mLongitude;
                double angle = 180.0 * Math.Atan2(dy, dx) / Math.PI - 90.0;
                while (angle < 0.0)
                {
                    angle += 360.0;
                }

                if (mCall == 1)
                {
                    mHeading = angle;
                }
                else
                {
                    if ((angle - mHeading) < -180.0)
                    {
                        angle += 360.0;
                    }
                    else if ((angle - mHeading) > 180.0)
                    {
                        angle -= 360.0;
                    }

                    mHeading = 0.7f * mHeading + 0.3f * angle;
                }
            }

            if (mCall < 3)
            {
                mCall++;
            }

            while (mHeading < 0.0f)
            {
                mHeading += 360.0f;
            }

            while (mHeading > 360.0f)
            {
                mHeading -= 360.0f;
            }

            mLatitude = aLatitude;
            mLongitude = aLongitude;
        }
    }
}
